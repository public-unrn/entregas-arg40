package com.unrn.inscripcion.controller;

import com.unrn.inscripcion.dto.DTOEstudiante;
import com.unrn.inscripcion.service.EstudianteImpl;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/estudiante")
@Slf4j
@AllArgsConstructor
public class EstudianteController {
    @Autowired
    private EstudianteImpl estudianteImpl;

    @PostMapping("/crear")
    public DTOEstudiante saveEstudiante(@RequestBody DTOEstudiante dto) throws Exception {
        return estudianteImpl.saveEstudiante(dto);
    }

    @GetMapping("/listar")
    public List<DTOEstudiante> listarEstudiantes() throws Exception {
        return estudianteImpl.findAll();
    }

    @GetMapping("/listar/{dni}/{apellido}")
    public List<DTOEstudiante> listarCursoDespuesDeFecha(@PathVariable int dni, @PathVariable String apellido) throws Exception {
        return estudianteImpl.listarEstudianteDniM20AndApellido(dni, apellido);
    }

    @GetMapping("/listarPage/{pagina}/{cantidad}")
    public List<DTOEstudiante> listarEstudiantePag(@PathVariable int pagina, @PathVariable int cantidad) throws Exception {
        return estudianteImpl.listarEstudiantePag(pagina,cantidad);
    }

    @GetMapping("/getById/{estudiante_id}")
    public DTOEstudiante listarEstudianteById(@PathVariable Long estudiante_id) throws Exception {
        return estudianteImpl.listarEstudianteById(estudiante_id);
    }


    @PutMapping("/actualizar/{estudiante_id}")
    public DTOEstudiante updateEstudiante(@PathVariable Long estudiante_id , @RequestBody @Valid DTOEstudiante estudiante) throws Exception {
        return estudianteImpl.updateEstudiante(estudiante_id,estudiante);
    }

    @DeleteMapping("/eliminar/{estudiante_id}")
    @ApiResponse(responseCode = "204")
    public void deleteEstudiante(@PathVariable Long estudiante_id) throws Exception {
          estudianteImpl.deleteEstudiante(estudiante_id);

    }
}
