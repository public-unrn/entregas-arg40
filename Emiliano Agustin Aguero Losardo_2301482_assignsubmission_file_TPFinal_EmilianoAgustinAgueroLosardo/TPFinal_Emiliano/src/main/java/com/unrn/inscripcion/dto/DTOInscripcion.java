package com.unrn.inscripcion.dto;

import com.unrn.inscripcion.domain.Curso;
import com.unrn.inscripcion.domain.EstadoInscripcion;
import com.unrn.inscripcion.domain.Estudiante;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@Slf4j
public class DTOInscripcion {

    private LocalDate inscripcionFecha;
    private EstadoInscripcion estado;
    private Long estudiante_id;
    private Long curso_id;
}