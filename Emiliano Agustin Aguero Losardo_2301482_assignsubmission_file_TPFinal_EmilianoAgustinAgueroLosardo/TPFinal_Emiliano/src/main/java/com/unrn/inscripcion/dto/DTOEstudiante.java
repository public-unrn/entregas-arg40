package com.unrn.inscripcion.dto;

import jakarta.persistence.Column;
import jakarta.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
public class DTOEstudiante {

    private String nombre;
    private String apellido;
    private String email;
    private int dni;
    private LocalDate fechaNacimiento;
    private Long edad;
}
