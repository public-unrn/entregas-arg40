package com.unrn.inscripcion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InscripcionAplicacion {

    public static void main(String[] args) {
        SpringApplication.run(InscripcionAplicacion.class, args);
    }

}