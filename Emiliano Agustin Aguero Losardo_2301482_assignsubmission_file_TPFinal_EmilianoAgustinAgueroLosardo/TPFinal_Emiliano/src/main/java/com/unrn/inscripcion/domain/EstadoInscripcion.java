package com.unrn.inscripcion.domain;

public enum EstadoInscripcion {
    ACEPTADA, PENDIENTE, RECHAZADO
}
