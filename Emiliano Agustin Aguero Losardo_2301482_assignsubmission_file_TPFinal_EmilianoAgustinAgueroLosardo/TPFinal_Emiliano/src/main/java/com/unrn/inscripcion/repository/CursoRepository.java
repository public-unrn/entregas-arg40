package com.unrn.inscripcion.repository;

import com.unrn.inscripcion.domain.Curso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.awt.*;
import java.time.LocalDate;
import java.util.List;
@Repository
public interface CursoRepository extends JpaRepository<Curso,Long> {

    List<Curso> findByFechaInicioAfter(LocalDate fechaInicio);

}
