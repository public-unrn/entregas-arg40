package com.unrn.inscripcion.repository;


import com.unrn.inscripcion.domain.Estudiante;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EstudianteRepository extends JpaRepository<Estudiante,Long> {

    List<Estudiante> findByDniGreaterThanAndApellidoIs(int dni, String apellido);

    List<Estudiante> findByDniGreaterThan(int dni);

    List<Estudiante> findByApellidoLike(String apellido);

}
