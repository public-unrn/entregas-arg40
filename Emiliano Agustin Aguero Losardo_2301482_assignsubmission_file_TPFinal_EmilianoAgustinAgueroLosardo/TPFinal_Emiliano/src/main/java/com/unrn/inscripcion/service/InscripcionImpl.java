package com.unrn.inscripcion.service;

import com.unrn.inscripcion.domain.Curso;
import com.unrn.inscripcion.domain.EstadoInscripcion;
import com.unrn.inscripcion.domain.Estudiante;
import com.unrn.inscripcion.domain.Inscripcion;
import com.unrn.inscripcion.dto.DTOCurso;
import com.unrn.inscripcion.dto.DTOEstudiante;
import com.unrn.inscripcion.dto.DTOInscripcion;
import com.unrn.inscripcion.repository.CursoRepository;
import com.unrn.inscripcion.repository.EstudianteRepository;
import com.unrn.inscripcion.repository.InscripcionRepository;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class InscripcionImpl   {

    @Autowired
    private CursoRepository cursoRepository;
    @Autowired
    private EstudianteRepository estudianteRepository;
    @Autowired
    private InscripcionRepository inscripcionRepository;

    @Transactional
    public Long saveInscripcion(DTOInscripcion dto) {

        Curso curso = cursoRepository.findById(dto.getCurso_id()).orElseThrow(() -> new RuntimeException("Error al obtener Curso"));
        Estudiante estudiante = estudianteRepository.findById(dto.getEstudiante_id()).orElseThrow(() -> new RuntimeException("Error al obtener Estudiante"));


        Inscripcion inscripcion = new Inscripcion(
                null,
                dto.getInscripcionFecha(),
                dto.getEstado(),
                estudiante,
                curso
        );
        Inscripcion inscripcion1 = inscripcionRepository.save(inscripcion);
        return inscripcion1.getId();
    }


    public List<DTOInscripcion> getListByEstado(EstadoInscripcion estado) throws Exception {

        List<DTOInscripcion> inscripcionList = inscripcionRepository.findByEstadoIs(estado)
                .stream().map(inscripcion -> new DTOInscripcion(inscripcion.getInscripcionFecha(),inscripcion.getEstado(),inscripcion.getEstudiante().getId(),inscripcion.getCurso().getId()))
                .collect(Collectors.toList());

        if(inscripcionList.isEmpty()){
            throw new Exception("Error al obtener las Inscripciones");
        }

        return inscripcionList;
    }

    public List<DTOInscripcion> getListByEstadoNativo(EstadoInscripcion estado) throws Exception {

        List<DTOInscripcion> inscripcionList = inscripcionRepository.findByEstado(estado)
                .stream().map(inscripcion -> new DTOInscripcion(inscripcion.getInscripcionFecha(),inscripcion.getEstado(),inscripcion.getEstudiante().getId(),inscripcion.getCurso().getId()))
                .collect(Collectors.toList());

        if(inscripcionList.isEmpty()){
            throw new Exception("Error al obtener las Inscripciones");
        }

        return inscripcionList;
    }

// TODO Metodo para obtener todas las inscripciones por id de estudiante y eliminarlas
 /*   public  void deleteInscripcionByEstudianteId( Long id)
    {
        List<Long> inscripcionesIds = inscripcionRepository.findByEstudianteId(id)
                .stream().map(inscripcion -> inscripcion.getId())
                .collect(Collectors.toList());
        inscripcionRepository.deleteAllById(inscripcionesIds);
    }
*/
}

