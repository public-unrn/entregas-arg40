package com.unrn.inscripcion.service;

import com.unrn.inscripcion.domain.Estudiante;
import com.unrn.inscripcion.dto.DTOEstudiante;
import com.unrn.inscripcion.repository.EstudianteRepository;
import com.unrn.inscripcion.repository.InscripcionRepository;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;


import javax.net.ssl.SSLException;
import java.util.List;
import java.util.stream.Collectors;
@Slf4j
@Service
public class EstudianteImpl  {

    @Autowired
    private EstudianteRepository estudianteRepository;

    @Transactional
    public DTOEstudiante saveEstudiante(DTOEstudiante dto) throws Exception {
        try{

            Estudiante estudiante = new Estudiante(
                    null,
                    dto.getNombre(),
                    dto.getApellido(),
                    dto.getEmail(),
                    dto.getDni(),
                    dto.getFechaNacimiento(),
                    dto.getEdad());

            if(estudiante.esMayordeEdad()) {
                estudianteRepository.save(estudiante);
            }
            else
            {
                throw new RuntimeException("El estudiante debe ser mayor de edad");
            }
        }catch (Exception e){
            throw new SSLException(e.getMessage());
        }

        return dto;
    }

    public List<DTOEstudiante> findAll() throws Exception {

        List<DTOEstudiante> estudianteList = estudianteRepository.findAll()
                .stream().map(estudiante -> new DTOEstudiante(estudiante.getNombre(),estudiante.getApellido(),estudiante.getEmail(),estudiante.getDni(), estudiante.getFechaNacimiento(),null))
                .collect(Collectors.toList());
        if(estudianteList.isEmpty()){
            throw new Exception("Error al obtener Estudiantes");
        }
        return estudianteList;
    }


    public List<DTOEstudiante> listarEstudianteDniM20AndApellido(int dni, String apellido) throws Exception {

        List<DTOEstudiante> estudianteList = estudianteRepository.findByDniGreaterThanAndApellidoIs(dni, apellido)
                .stream().map(estudiante -> new DTOEstudiante(estudiante.getNombre(),estudiante.getApellido(),estudiante.getEmail(),estudiante.getDni(), estudiante.getFechaNacimiento(),null))
                //.filter(e -> e.getDni() > dni )
                .collect(Collectors.toList());

        if(estudianteList.isEmpty()){
            throw new Exception("Error al obtener Estudiantes");
        }

        return estudianteList;
    }


    public List<DTOEstudiante> listarEstudiantePag(@NotNull @Positive int pagina,@NotNull @Positive int cantidad) throws Exception {

        List<DTOEstudiante> estudianteList = estudianteRepository.findAll(PageRequest.of(pagina,cantidad, Sort.by("dni").ascending()))
                .stream().map(estudiante -> new DTOEstudiante(estudiante.getNombre(), estudiante.getApellido(),  estudiante.getEmail(),estudiante.getDni(), estudiante.getFechaNacimiento(),null))
                .collect(Collectors.toList());

        return estudianteList;
    }


    public DTOEstudiante listarEstudianteById(Long estudiante_id) throws Exception {
        DTOEstudiante estudianteDto = estudianteRepository.findById(estudiante_id).map(estudiante -> mapToDto(estudiante, new DTOEstudiante())).orElseThrow(ChangeSetPersister.NotFoundException::new);
        return  estudianteDto;
    }

    @Transactional
    public void deleteEstudiante(Long estudiante_id) throws  Exception{


        DTOEstudiante estudianteDto = estudianteRepository.findById(estudiante_id).map(estudiante -> mapToDto(estudiante, new DTOEstudiante())).orElseThrow(ChangeSetPersister.NotFoundException::new);

        //TODO Debido a que PostreSQL con Elephant no soporta el delete on casacade se creo un metodo para eliminar todas las incripciones asociadas al estudiante
        // inscripcionImpl.deleteInscripcionByEstudianteId(estudiante_id);


        estudianteRepository.deleteById(estudiante_id);
    }
    @Transactional
    public DTOEstudiante updateEstudiante(Long estudiante_id, DTOEstudiante dtoestudiante) throws  Exception{

        final Estudiante estudiante = estudianteRepository.findById(estudiante_id).orElseThrow(ChangeSetPersister.NotFoundException::new);
        maptoEntity(dtoestudiante, estudiante);
        estudianteRepository.save(estudiante);

        return dtoestudiante;

    }

    private DTOEstudiante mapToDto(final Estudiante estudiante, final DTOEstudiante dtoEstudiante)
    {
        dtoEstudiante.setNombre(estudiante.getNombre());
        dtoEstudiante.setApellido(estudiante.getApellido());
        dtoEstudiante.setEmail(estudiante.getEmail());
        dtoEstudiante.setFechaNacimiento(estudiante.getFechaNacimiento());
        dtoEstudiante.setDni(estudiante.getDni());

        return dtoEstudiante;
    }

    private Estudiante maptoEntity(final DTOEstudiante dtoEstudiante,final Estudiante estudianteent)
    {
        estudianteent.setNombre(dtoEstudiante.getNombre());
        estudianteent.setApellido(dtoEstudiante.getApellido());
        estudianteent.setEmail(dtoEstudiante.getEmail());
        estudianteent.setFechaNacimiento(dtoEstudiante.getFechaNacimiento());
        estudianteent.setDni(dtoEstudiante.getDni());
        return estudianteent;
    }


}