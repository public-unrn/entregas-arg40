package com.unrn.inscripcion.controller;

import com.unrn.inscripcion.domain.EstadoInscripcion;
import com.unrn.inscripcion.dto.DTOEstudiante;
import com.unrn.inscripcion.dto.DTOInscripcion;
import com.unrn.inscripcion.service.EstudianteImpl;
import com.unrn.inscripcion.service.InscripcionImpl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/inscripcion")
@Slf4j
@AllArgsConstructor
public class InscripcionController {

    @Autowired
    private InscripcionImpl inscripcionImpl;

    @PostMapping("/crear")
    public Long saveInscripcion(@RequestBody DTOInscripcion dto) throws Exception {
        return inscripcionImpl.saveInscripcion(dto);
    }


    @GetMapping("/listar")
    public List<DTOInscripcion> listarInscripcionesPorEstado(@RequestParam("estado") EstadoInscripcion estado) throws Exception {
        return inscripcionImpl.getListByEstado(estado);
    }
    @GetMapping("/listarnativo")
    public List<DTOInscripcion> listarInscripcionesNativo(@RequestParam("estado") EstadoInscripcion estado)throws Exception{
        return inscripcionImpl.getListByEstadoNativo(estado);
    }
}
