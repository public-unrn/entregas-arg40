package com.unrn.inscripcion.controller;

import com.unrn.inscripcion.domain.Curso;
import com.unrn.inscripcion.dto.DTOCurso;
import com.unrn.inscripcion.service.CursoImpl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/curso")
@Slf4j
@AllArgsConstructor
public class CursoController {

    @Autowired
    private CursoImpl cursoImpl;

    @PostMapping("/crear")
    public DTOCurso saveCurso(@RequestBody DTOCurso dto) throws Exception {
        return cursoImpl.saveCurso(dto);
    }

    @GetMapping("/listar")
    public List<DTOCurso> listarCursos() throws Exception {
        return cursoImpl.findAll();
    }

    @GetMapping("/listar/{fecha}")
    public List<DTOCurso> listarCursoDespuesDeFecha(@PathVariable LocalDate fecha) throws Exception {
        return cursoImpl.getListByFechaInicio(fecha);
    }

}
