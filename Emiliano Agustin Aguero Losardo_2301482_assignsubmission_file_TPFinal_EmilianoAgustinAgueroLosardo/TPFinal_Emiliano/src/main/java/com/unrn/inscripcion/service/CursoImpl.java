package com.unrn.inscripcion.service;

import com.unrn.inscripcion.domain.Curso;
import com.unrn.inscripcion.dto.DTOCurso;
import com.unrn.inscripcion.repository.CursoRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.net.ssl.SSLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CursoImpl  {

    @Autowired
    private CursoRepository cursoRepository;


    public List<DTOCurso> getListByFechaInicio( LocalDate fechaInicio) throws Exception {

        List<DTOCurso> cursoList = cursoRepository.findByFechaInicioAfter(fechaInicio)
                .stream().map(curso -> new DTOCurso(curso.getNombre(),curso.getDescripcion(),curso.getFechaInicio(),curso.getFechaFin()))
                .collect(Collectors.toList());

        if(cursoList.isEmpty()){
            throw new Exception("Error al obtener cursos");
        }

        return cursoList;
    }


    @Transactional
    public DTOCurso saveCurso(DTOCurso dto) throws Exception {
        try{
            Curso curso = new Curso(
                    null,
                    dto.getNombre(),
                    dto.getDescripcion(),
                    dto.getFechaInicio(),
                    dto.getFechaFin()
            );
            cursoRepository.save(curso);
        }catch (Exception e){
            throw new SSLException(e.getMessage());
        }

        return dto;
    }


    public List<DTOCurso> findAll() throws Exception {

        List<DTOCurso> cursoList = cursoRepository.findAll()
                .stream().map(c -> new DTOCurso(c.getNombre(),c.getDescripcion(),c.getFechaInicio(),c.getFechaFin()))
                .collect(Collectors.toList());

        if(cursoList.isEmpty()){
            throw new Exception("Error al obtener cursos");
        }

        return cursoList;
    }

}

