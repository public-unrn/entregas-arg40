package com.example.demo.repository;

import com.example.demo.domain.Estudiante;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EstudianteRepository extends JpaRepository<Estudiante, Long> {

    @Query("SELECT c FROM Estudiante c")
    List<Estudiante> findAllEstudiantes();

    @Query("SELECT c FROM Estudiante c WHERE c.dni > 20000000 AND c.apellido='Romero'")
    List<Estudiante> findAllDniyApellido();


    List<Estudiante> findAll();

    List<Estudiante> findByDniGreaterThanAndApellidoIs(int dni, String ape);



}
