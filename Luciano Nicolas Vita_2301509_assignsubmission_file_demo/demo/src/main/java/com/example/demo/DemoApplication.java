package com.example.demo;

import com.example.demo.domain.Curso;
import com.example.demo.domain.Estado;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;
import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.repository.InscripcionRepository;
import com.example.demo.service.InscripcionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Autowired
	CursoRepository cursoRepository;
	@Autowired
	EstudianteRepository estudianteRepository;
	@Autowired
	InscripcionRepository inscripcionRepository;
	@Autowired
	InscripcionService inscripcionService;


	private void saveEstudiantes(){
		estudianteRepository.saveAll(Arrays.asList(
				new Estudiante(null,39427858,"Luciano","Vita","Lucianonicolasvita13@gmail.com",LocalDate.of(1996,Month.MARCH, 18),27),
				new Estudiante(null, 33016244,"Lionel", "Messi","elcampeondelmundo_10@gmail.com",LocalDate.of(1987,Month.JUNE,24),36),
				new Estudiante(null, 35418290,"Emiliano","Martinez","miraquetecomohermano22@gmail.com",LocalDate.of(1992,Month.SEPTEMBER,2),30),
				new Estudiante(null, 33472815, "Angel","Di Maria","guardaquetelapico7@gmail.com",LocalDate.of(1988,Month.FEBRUARY,14),35)
		));
	}
	private void saveCursos(){
		cursoRepository.saveAll(Arrays.asList(
				new Curso(null, "Springboot",LocalDate.of(2023, Month.JUNE,13),LocalDate.of(2023, Month.JULY,10),"Desarrollo de Aplicaciones Autoconfigurables con SpringBoot"),
				new Curso(null,"Consultas",LocalDate.of(2023,Month.JUNE,11),LocalDate.of(2023,Month.JULY,6),"Framework"),
				new Curso(null, "Modelado",LocalDate.of(2023, Month.JUNE,13),LocalDate.of(2023, Month.JULY,8),"Spring Framework")

		));
	}


	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx){
		return args -> {
			saveEstudiantes();
			saveCursos();

			inscripcionService.incrip(1L,1L,LocalDate.of(2023,Month.MAY,25),Estado.PENDIENTE);


 			estudianteRepository.findAll();
		};
	}
}
