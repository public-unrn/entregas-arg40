package com.example.demo.repository;

import com.example.demo.domain.Estado;
import com.example.demo.domain.Inscripcion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InscripcionRepository extends JpaRepository<Inscripcion, Long> {
    @Query("SELECT c FROM Inscripcion c WHERE c.estado = 'RECHAZADA' OR c.estado = 'PENDIENTE'")
    List<Inscripcion> findAllRechazadasORPendientes();


    @Query("SELECT c FROM Inscripcion c WHERE estado= :estado")
    List<Inscripcion> finaAllInscripcionByEstado(@Param("estado") Estado estado);

    @Query(value = "SELECT * FROM Inscripcion c WHERE estado= :estado", nativeQuery = true)
    List<Inscripcion> findAllInscripByEstNat(@Param("estado") Estado estado);

    List<Inscripcion> findByEstadoIsOrEstadoIs(Estado rechazado, Estado pendiente);

    List<Inscripcion> findByEstadoIs(Estado estados);



}

