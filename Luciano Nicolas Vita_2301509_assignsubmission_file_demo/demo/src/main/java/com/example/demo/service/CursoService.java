package com.example.demo.service;

import com.example.demo.domain.Curso;
import com.example.demo.dto.CursoDTO;
import com.example.demo.repository.CursoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CursoService {

    @Autowired
    private CursoRepository cursoRepository;

    public CursoDTO saveCurso(CursoDTO cursoDTO) {
        Curso curso = new Curso(
                null,
                cursoDTO.getDescripcion(),
                cursoDTO.getFechaDeInicio(),
                cursoDTO.getFechaDeFin(),
                cursoDTO.getNombre()

        );

        cursoRepository.save(curso);

        return cursoDTO;
    }

    public List<CursoDTO> findAll(){
        return cursoRepository.findAll()
                .stream().map( c -> new CursoDTO(c.getDescripcion(),c.getFechaDeInicio(),c.getFechaDeFin(),c.getNombre()))
                .collect(Collectors.toList());
    }
}
