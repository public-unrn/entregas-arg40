package com.example.demo.service;

import com.example.demo.domain.Estudiante;
import com.example.demo.dto.EstudianteDTO;
import com.example.demo.repository.EstudianteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class EstudianteService {

    @Autowired
    private EstudianteRepository estudianteRepository;


    public EstudianteDTO saveEstudiante(EstudianteDTO estudianteDTO){
        Estudiante estudiante = new Estudiante(
                null,
                estudianteDTO.getDni(),
                estudianteDTO.getNombre(),
                estudianteDTO.getApellido(),
                estudianteDTO.getEmail(),
                estudianteDTO.getFechaDeNacimiento(),
                estudianteDTO.getEdad()
        );
        estudianteRepository.save(estudiante);

        return estudianteDTO;
    }


    public List<EstudianteDTO> findAll(){
        return estudianteRepository.findAll()
                .stream().map(c -> new EstudianteDTO(c.getDni(), c.getNombre(), c.getApellido(), c.getEmail(), c.getFechaDeNacimiento(), c.getEdad()))
                .collect(Collectors.toList());

    }

    public EstudianteDTO update(long id, EstudianteDTO estudianteDTO){
        Estudiante estudiante = new Estudiante(id, estudianteDTO.getDni(), estudianteDTO.getNombre(), estudianteDTO.getApellido(), estudianteDTO.getEmail(), estudianteDTO.getFechaDeNacimiento(), estudianteDTO.getEdad());

        estudianteRepository.save(estudiante);

        return estudianteDTO;

    }

    public EstudianteDTO find(Long id){
        Optional<Estudiante> estudianteOptional = estudianteRepository.findById(id);

        if (estudianteOptional.isEmpty()){
            throw new RuntimeException("Id no valido");
        }

        Estudiante estudiante = estudianteOptional.get();

        return new EstudianteDTO(estudiante.getDni(), estudiante.getNombre(), estudiante.getApellido(), estudiante.getEmail(), estudiante.getFechaDeNacimiento(), estudiante.getEdad());

    }

    public void delete(Long id){ estudianteRepository.deleteById(id);}
}
