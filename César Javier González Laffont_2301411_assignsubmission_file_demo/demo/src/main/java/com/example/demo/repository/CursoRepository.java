package com.example.demo.repository;

import com.example.demo.domain.Curso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface CursoRepository extends JpaRepository <Curso,String>{

    //Listar todos los cursos

    //consultas nativas usando JPQL
    @Query("SELECT c FROM Curso c")
    List<Curso> findAllCursos();

    //Listar todos los cursos que empezaron despues de una fecha
    @Query("SELECT c FROM Curso c WHERE c.fechaDeInicio > :fechaDeInicio")
    List<Curso> findCursosAfterDate(@Param("fechaDeInicio") LocalDate fechaDeInicio);

    //derivadas
    List<Curso> findAll();
    List<Curso> findCursoByFechaDeInicioAfter(LocalDate FechaDeInicio);

}
