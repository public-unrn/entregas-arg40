//Tp final UNRN Desarrollo de aplicaciones auto-configurables
//Alumno: Cesar Javier Gonzalez Laffont
//Fecha entrega: 18/7/2023

package com.example.demo;

import com.example.demo.domain.Curso;
import com.example.demo.domain.Estado;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;

import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.repository.InscripcionRepository;

import com.example.demo.service.InscripcionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.util.List;

@SpringBootApplication
public class UnrnProyectoApplication {
	public static void main(String[] args) {
		SpringApplication.run(UnrnProyectoApplication.class, args);
	}
	@Autowired
	EstudianteRepository estudianteRepository;
	@Autowired
	CursoRepository cursoRepository;
	@Autowired
	InscripcionRepository inscripcionRepository;
	@Autowired
	InscripcionService inscripcionService;
	@Bean
	public CommandLineRunner commandLineRunner(UnrnProyectoApplication ctx) {
		return args -> {
			System.out.println("La app se ha iniciado");

			// Genero datos

			cargarDatos();

			//Consignas TP CONSULTAS

			// Cada consulta del Tp Consultas tendrá su salida por consola

			// 1- Listar todos los cursos

			System.out.println("Resultado consulta 1 ESPECIALIZADA");
			mostrarCursos(cursoRepository.findAllCursos());

			// 2- Listar todos los estudiantes

			System.out.println("Resultado consulta 2 ESPECIALIZADA");
			mostrarEstudiantes(estudianteRepository.findAllEstudiantes());

			// 3- Listar todos los estudiantes que tengan un dni mayor a 20M y que su apellido sea “Romero”

			System.out.println("Resultado Consulta 3 ESPECIALIZADA");
			mostrarEstudiantes(estudianteRepository.findByDniAndApellido(20000000L,"Romero"));

			// 4- Listar todas las inscripciones rechazadas o pendiente

			System.out.println("Resultado Consulta 4 ESPECIALIZADA");
			System.out.println("Inscripciones Pendientes");
			mostrarInscripciones(inscripcionRepository.findByEstado(Estado.PENDIENTE));
			System.out.println("Inscripciones Rechazadas");
			mostrarInscripciones(inscripcionRepository.findByEstado(Estado.RECHAZADA));

			// 5- Listar todos los cursos que hayan empezado después de “01/02/2020”
			System.out.println("Resultado Consulta 5 ESPECIALIZADA");
			mostrarCursosConFechaInicio(cursoRepository.findCursosAfterDate(LocalDate.of(2020,2,1)));

			// 6- Listar todas las inscripciones en base a un parámetro de estado

			System.out.println("Resultado Consulta 6 ESPECIALIZADA");
			System.out.println("Inscripciones aceptadas");
			mostrarInscripciones(inscripcionRepository.findByEstado(Estado.ACEPTADA));

			// 7- Listar todas las inscripciones en base a un parámetro de estado utilizando consulta nativa

			System.out.println("Resultado Consulta 7 NATIVA");
			System.out.println("Inscripciones aceptadas");
			mostrarInscripciones(inscripcionRepository.findByEstadoNativa(Estado.ACEPTADA.name()));

			// 8- Implementar las primeras 6 consultas mediante consulta derivada

			//Con anotación @Query especializadas JPQL o nativas SQL
			//Sin anotación @Query derivadas
			//FindAll, findById, son operaciones que hereda de repository

			//8-1- Listar todos los cursos DERIVADA
			System.out.println("Resultado consulta 8-1 DERIVADA");
			mostrarCursos(cursoRepository.findAll());

			//8-2- Listar todos los estudiantes DERIVADA
			System.out.println("Resultado consulta 8-2 DERIVADA");
			mostrarEstudiantes(estudianteRepository.findAll());

			// 8-3- Listar todos los estudiantes que tengan un dni mayor a 20M y que su apellido sea “Romero” DERIVADA

			System.out.println("Resultado Consulta 8-3 DERIVADA");
			mostrarEstudiantes(estudianteRepository.findEstudiantesByDniGreaterThanAndApellido(20000000L,"Romero"));

			// 8-4- Listar todas las inscripciones rechazadas o pendiente DERIVADA

			System.out.println("Resultado Consulta 8-4 DERIVADA");
			System.out.println("Inscripciones Pendientes");
			mostrarInscripciones(inscripcionRepository.findInscripcionByEstado(Estado.PENDIENTE));
			System.out.println("Inscripciones Rechazadas");
			mostrarInscripciones(inscripcionRepository.findInscripcionByEstado(Estado.RECHAZADA));

			// 8-5- Listar todos los cursos que hayan empezado después de “01/02/2020” DERIVADA
			System.out.println("Resultado Consulta 8-5 DERIVADA");
			mostrarCursosConFechaInicio(cursoRepository.findCursoByFechaDeInicioAfter(LocalDate.of(2020,2,1)));

			// 8-6- Listar todas las inscripciones en base a un parámetro de estado DERIVADA

			System.out.println("Resultado Consulta 8-6 DERIVADA");
			System.out.println("Inscripciones aceptadas");
			mostrarInscripciones(inscripcionRepository.findInscripcionByEstado(Estado.ACEPTADA));

			// 9- Listar todos los estudiantes de forma paginada y ordenada ascendente por DNI
			// Realizar cualquier operación adicional con los resultados paginados
			System.out.println("Resultado Consulta 9 PAGINACION");
			PageRequest pageRequest = PageRequest.of(0, 10, Sort.by(Sort.Direction.ASC, "dni"));
			Page<Estudiante> page = estudianteRepository.findAll(pageRequest);
			mostrarEstudiantes(page.getContent());

			// 10- Probar las siguientes combinaciones:
			System.out.println("Resultado Consulta 10 PAGINACION: * pagina 1, tamaño 5");
			// * pagina 1, tamaño 5
			PageRequest pageRequest1 = PageRequest.of(1, 5, Sort.by(Sort.Direction.DESC, "dni"));
			Page page1 = estudianteRepository.findAll(pageRequest1);
			mostrarEstudiantes(page1.getContent());
			System.out.println("Resultado Consulta 10 PAGINACION: * pagina 0, tamaño 2");
			// * pagina 0, tamaño 2
			PageRequest pageRequest2 = PageRequest.of(0, 2, Sort.by(Sort.Direction.DESC, "dni"));
			Page page2 = estudianteRepository.findAll(pageRequest2);
			mostrarEstudiantes(page2.getContent());


			// 11- Servicio y Transacción
			// Queremos registrar una inscripción donde se tiene que indicar el estudiante y curso.
			// Reglas de negocio:
			// El estudiante debe ser mayor de edad
			// Nota: validar todos los parametros del metodo del servicio

			try {
				inscripcionService.createInscripcion("Databases",55555555L,Estado.ACEPTADA);
			}catch (Exception e){
				System.out.println(e.getMessage());
			}

		};
	}

	private static void mostrarCursosConFechaInicio(List<Curso> cursos5) {
		for (Curso curso : cursos5) {
			System.out.println("Curso: "+curso.getNombre() +"Fecha inicio: "+curso.getFechaDeInicio());
		}
	}

	private static void mostrarEstudiantes(List<Estudiante> estudiantes) {
		for (Estudiante estudiante : estudiantes)
		{
			System.out.println( "DNI: "+estudiante.getDni() +"  -   Apellido: "+ estudiante.getApellido());
		}
	}

	private static void mostrarCursos(List<Curso> cursos) {
		for (Curso curso : cursos)
		{
			System.out.println( "ID: "+curso.getNombre() +"  -   Curso: "+ curso.getDescripcion());
		}
	}

	private static void mostrarInscripciones(List<Inscripcion> inscripciones) {
		for (Inscripcion inscripcion : inscripciones) {
			System.out.println("Inscripcion ID:"+inscripcion.getId()+" Estudiante: "+inscripcion.getEstudiante().getApellido() +" Curso: "+ inscripcion.getCurso().getNombre());
		}
	}

	public void cargarDatos() {

		Estudiante estudiante1 = new Estudiante(11111111L,"Angel","Di Maria","angel@gmail.com", LocalDate.of(1986,6,11));
		Estudiante estudiante2 = new Estudiante(22222222L,"Lionel","Messi","messi@gmail.com",LocalDate.of(1986,6,24));
		Estudiante estudiante3 = new Estudiante(33333333L,"Emiliano","Martinez","dibu@gmail.com",LocalDate.of(1992,7,5));
		Estudiante estudiante4 = new Estudiante(44444444L,"Enzo","Fernandez","enzo@gmail.com",LocalDate.of(1999,9,6));
		Estudiante estudiante5 = new Estudiante(55555555L,"Cristian","Romero","cuti@gmail.com",LocalDate.of(1998,8,23));
		Curso curso1 = new Curso("Algebra II","Dept. Mathematics",LocalDate.of(2019,8,10),LocalDate.of(2019,12,15));
		Curso curso2 = new Curso("Statistic","Dept. Management",LocalDate.of(2020,8,10),LocalDate.of(2020,12,15));
		Curso curso3 = new Curso("Analysis Numeric","Dept. Informatics",LocalDate.of(2021,8,10),LocalDate.of(2021,12,15));
		Curso curso4 = new Curso("Databases","Dept. Informatics",LocalDate.of(2022,8,10),LocalDate.of(2022,12,15));

		persistirEstudiante(estudiante1);
		persistirEstudiante(estudiante2);
		persistirEstudiante(estudiante3);
		persistirEstudiante(estudiante4);
		persistirEstudiante(estudiante5);
		persistirCurso(curso1);
		persistirCurso(curso2);
		persistirCurso(curso3);
		persistirCurso(curso4);

		inscripcionService.createInscripcion(curso1.getNombre(),estudiante1.getDni(),null);
		inscripcionService.createInscripcion(curso1.getNombre(),estudiante2.getDni(),null);
		inscripcionService.createInscripcion(curso1.getNombre(),estudiante3.getDni(),null);
		inscripcionService.createInscripcion(curso1.getNombre(),estudiante4.getDni(),null);
		inscripcionService.createInscripcion(curso2.getNombre(),estudiante1.getDni(),null);
		inscripcionService.createInscripcion(curso2.getNombre(),estudiante2.getDni(),null);
		inscripcionService.createInscripcion(curso2.getNombre(),estudiante3.getDni(),null);
		inscripcionService.createInscripcion(curso2.getNombre(),estudiante4.getDni(),null);
		inscripcionService.createInscripcion(curso3.getNombre(),estudiante1.getDni(),null);
		inscripcionService.createInscripcion(curso3.getNombre(),estudiante2.getDni(),null);
		inscripcionService.createInscripcion(curso3.getNombre(),estudiante3.getDni(),null);
		inscripcionService.createInscripcion(curso3.getNombre(),estudiante4.getDni(),null);
	}

	private void persistirCurso(Curso curso) {
		if(!cursoRepository.existsById(curso.getNombre())){
			cursoRepository.save(curso);
		}
	}

	private void persistirEstudiante(Estudiante estudiante) {
		if(!estudianteRepository.existsById(estudiante.getDni())){
			estudianteRepository.save(estudiante);
		}
	}

}