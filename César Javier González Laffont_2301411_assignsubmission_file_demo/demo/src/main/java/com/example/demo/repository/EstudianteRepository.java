package com.example.demo.repository;

import com.example.demo.domain.Estudiante;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.awt.print.Pageable;
import java.util.List;

public interface EstudianteRepository extends JpaRepository<Estudiante,Long > {

    @Query("SELECT e FROM Estudiante e")
    List<Estudiante> findAllEstudiantes();
    @Query("SELECT e FROM Estudiante e WHERE e.dni > :dni AND e.apellido LIKE %:apellido%")
    List<Estudiante> findByDniAndApellido(@Param("dni") Long dni, @Param("apellido") String apellido);

    List<Estudiante> findAll();
    List<Estudiante> findAllByDni(Pageable pageable);
    List<Estudiante> findEstudiantesByDniGreaterThanAndApellido(Long dniMayorA, String apellido);

}
