package com.example.demo.domain;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.util.Date;
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table (name = "estudiante")
public class Estudiante {
    @Id
    private Long dni;
    private String nombre;
    private String apellido;
    private String email;
    @Column (name = "fecha_de_nacimiento")
    private LocalDate fechaDeNacimiento;

//    @Transient // no se persiste en la BD
//    private int edad;
    public int getEdad(){
        return LocalDate.now().getYear() - this.fechaDeNacimiento.getYear();
    }
}
