package com.example.demo.domain;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table (name = "inscripcion")
public class Inscripcion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "fecha_inscripcion")
    private LocalDate fechaInscripción;

    @ManyToOne
    @JoinColumn(name="dni")
    private Estudiante estudiante;
    @ManyToOne
    @JoinColumn(name = "nombre")
    private Curso curso;
    @Enumerated(EnumType.STRING)
    private Estado estado;

}
