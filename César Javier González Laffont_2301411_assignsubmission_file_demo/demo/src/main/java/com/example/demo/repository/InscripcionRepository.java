package com.example.demo.repository;

import com.example.demo.domain.Curso;
import com.example.demo.domain.Estado;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InscripcionRepository extends JpaRepository <Inscripcion,Long>{

    @Query("SELECT i FROM Inscripcion i WHERE i.estudiante=:estudiante AND i.curso = :curso ")
    List<Inscripcion> findByEstudianteAndCurso(@Param("estudiante") Estudiante estudiante, @Param("curso") Curso curso);
    @Query("SELECT i FROM Inscripcion i WHERE i.estado=:estado")
    List<Inscripcion> findByEstado(@Param("estado") Estado estado);
    @Query(value = "SELECT * FROM inscripcion i WHERE i.estado=:estado",nativeQuery = true)
    List<Inscripcion> findByEstadoNativa(@Param("estado") String estado);
    List<Inscripcion> findInscripcionByEstado(Estado estado);

}
