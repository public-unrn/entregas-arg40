package com.example.demo.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.*;

import java.time.LocalDate;
import java.util.Date;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table(name = "curso")
public class Curso {
    @Id
    private String nombre;
    private String descripcion;
    @Column(name = "fecha_de_inicio")
    private LocalDate fechaDeInicio;
    @Column(name="fecha_de_fin")
    private LocalDate fechaDeFin;

}

   /* Una inscripción pertenece a un único curso (relación Many-to-One
        entre "Inscripción" y "Curso").
        Una inscripción pertenece a un solo estudiante (relación Many-to-One
        entre "Inscripción" y "Estudiante").*/