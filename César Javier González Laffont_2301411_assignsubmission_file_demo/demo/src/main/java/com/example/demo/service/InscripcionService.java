package com.example.demo.service;

import com.example.demo.domain.Curso;
import com.example.demo.domain.Estado;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;
import com.example.demo.dto.InscripcionDTO;
import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.repository.InscripcionRepository;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

@Service
public class InscripcionService {

    @Autowired
    private CursoRepository cursoRepository;
    @Autowired
    private InscripcionRepository inscripcionRepository;
    @Autowired
    private EstudianteRepository estudianteRepository;

    @Transactional
    public Inscripcion createInscripcion(@NotNull String nombreCurso, @NotNull @Positive Long dniEstudiante, Estado estado) {
        Inscripcion resultado = null;
        Optional<Curso> cursoOptional = Optional.empty();
        Optional<Estudiante> estudianteOptional = Optional.empty();

        if (!nombreCurso.isEmpty()) {
            cursoOptional = cursoRepository.findById(nombreCurso);
        }
        else{
            throw new IllegalArgumentException("El nombre del curso no puede ser vacío");
        }
        if (dniEstudiante > 0) {
            estudianteOptional = estudianteRepository.findById(dniEstudiante);
        }else{
            throw new IllegalArgumentException("El dni del estudiante debe ser mayor que 0");
        }

        if(estado==null){
            estado = estadoRandom();
        }

        if (cursoOptional.isPresent() && estudianteOptional.isPresent()) {
            Curso curso = cursoOptional.get();
            Estudiante estudiante = estudianteOptional.get();
            if(estudiante.getEdad()>18) {
                resultado = new Inscripcion(null, LocalDate.now(), estudiante, curso, estado);
                inscripcionRepository.save(resultado);

            }
        }
        return resultado;
}

    private Estado estadoRandom(){
        Random random = new Random();
        Estado[] estados = Estado.values();
        int randomOrdinal = random.nextInt(estados.length);
        return estados[randomOrdinal];
    }

    public InscripcionDTO saveInscripcion(InscripcionDTO inscripcionDTO){
        Inscripcion inscripcion = new Inscripcion(
                null,
                inscripcionDTO.getFechaInscripción(),
                inscripcionDTO.getEstudiante(),
                inscripcionDTO.getCurso(),
                inscripcionDTO.getEstado()
        );
        inscripcionRepository.save(inscripcion);
        return inscripcionDTO;
    }
    public List<InscripcionDTO> findAll(){
        return inscripcionRepository.findAll().stream().map(i-> new InscripcionDTO(i.getId(),i.getFechaInscripción(),i.getEstudiante(),i.getCurso(),i.getEstado())).collect(Collectors.toList());
    }

    public InscripcionDTO update(Long id, InscripcionDTO inscripcionDTO){
        Inscripcion inscripcion = new Inscripcion(id,inscripcionDTO.getFechaInscripción(),inscripcionDTO.getEstudiante(),inscripcionDTO.getCurso(),inscripcionDTO.getEstado());
        inscripcionRepository.save(inscripcion);
        return inscripcionDTO;
    }

    public InscripcionDTO find(Long id){
        Optional<Inscripcion> optionalInscripcion = inscripcionRepository.findById(id);
        if(optionalInscripcion.isEmpty()){
            throw new RuntimeException("Id invalido");
        }
        Inscripcion inscripcion = optionalInscripcion.get();
        return new InscripcionDTO(inscripcion.getId(),inscripcion.getFechaInscripción(),inscripcion.getEstudiante(),inscripcion.getCurso(),inscripcion.getEstado());
    }
    public void delete(Long id){
        inscripcionRepository.deleteById(id);
    }

}
