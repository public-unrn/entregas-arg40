package com.example.demo.dto;

import jakarta.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class EstudianteDTO {
    private Long dni;
    private String nombre;
    private String apellido;
    private String email;
    private LocalDate fechaDeNacimiento;
}
