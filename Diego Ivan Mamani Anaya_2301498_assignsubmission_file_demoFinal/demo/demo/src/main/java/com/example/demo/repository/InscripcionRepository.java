package com.example.demo.repository;

import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InscripcionRepository extends JpaRepository <Inscripcion, Long> {
    @Query("SELECT c FROM Inscripcion c WHERE c.status = 'RECHAZADO' OR c.status = 'ACEPTADO'")
    List<Inscripcion> findByRechazadoOrAceptado();
    //List<Inscripcion> findByStatusIn(List<String> statusList);





    @Query("SELECT c FROM Inscripcion c WHERE c.status = :parametro")
    List<Inscripcion> findByEstadoParametrizado(@Param("parametro") String parametro);
    List<Inscripcion> findByStatus(String parametro);

}
