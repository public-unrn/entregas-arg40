package com.example.demo.repository;

import com.example.demo.domain.Curso;
import com.example.demo.domain.Estudiante;
import org.antlr.v4.runtime.atn.SemanticContext;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EstudianteRepository extends JpaRepository<Estudiante, Long> {
    @Query("SELECT c FROM Estudiante c")
    List<Estudiante> findAllEstudiante();
    List<Estudiante> findAll();


    @Query("SELECT c FROM Estudiante c WHERE c.dni > 20000000 AND c.apellido='Romero'")
    List<Estudiante> findAllEstudianteDniMayor20yApellidoRomero();
    List<Estudiante> findAllByDniGreaterThanAndApellidoEquals(int dni, String apellido);


}
