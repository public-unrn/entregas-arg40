package com.example.demo;

import com.example.demo.domain.Curso;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;
import com.example.demo.domain.Status;
import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.repository.InscripcionRepository;
import com.example.demo.service.InscripcionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Autowired
	EstudianteRepository estudianteRepository;

	@Autowired
	CursoRepository cursoRepository;
	@Autowired
	InscripcionRepository inscripcionRepository;

	@Autowired
	InscripcionService inscripcionService;

	@Bean
	public CommandLineRunner commandLineRunner (ApplicationContext ctx){
		return args -> {
			Estudiante estudiante = new Estudiante();
			estudiante.setApellido("Mamani");
			estudiante.setEdad(26);
			estudiante.setDni(40362686);
			estudiante.setEmail("diego.ivan@hotmail.com");
			estudiante.setFechaNacimiento(LocalDate.now());
			estudiante.setNombre("Diego");

			estudiante = estudianteRepository.save(estudiante);


			Curso curso =new Curso(null, "matematica","algebra y geometria",LocalDate.now(),LocalDate.now());

			curso = cursoRepository.saveAndFlush(curso);

			Inscripcion inscripcion = new Inscripcion(null,LocalDate.now(), Status.ACEPTADO,estudiante,curso);
			inscripcionRepository.saveAndFlush(inscripcion);

			cursoRepository.findAllCurso();
			estudianteRepository.findAllEstudiante();
			estudianteRepository.findAllEstudianteDniMayor20yApellidoRomero();
			inscripcionRepository.findByRechazadoOrAceptado();
			cursoRepository.findCursoFechaInicioMayor2020(LocalDate.now());
			inscripcionRepository.findByEstadoParametrizado("ACEPTADO");
			estudianteRepository.findAll(PageRequest.of(1,5, Sort.by(Sort.Direction.ASC,"dni")));
			estudianteRepository.findAll(PageRequest.of(0,2, Sort.by(Sort.Direction.ASC,"dni")));
			inscripcionService.inscripcion(1L,1L);
		};
	}
}
