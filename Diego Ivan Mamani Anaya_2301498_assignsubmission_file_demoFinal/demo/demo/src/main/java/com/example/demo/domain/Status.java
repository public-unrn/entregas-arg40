package com.example.demo.domain;

public enum Status {
    ACEPTADO, RECHAZADO, PENDIENTE
}
