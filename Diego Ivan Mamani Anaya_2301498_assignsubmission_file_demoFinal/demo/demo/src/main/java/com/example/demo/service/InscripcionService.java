package com.example.demo.service;

import com.example.demo.DTO.EstudianteDTO;
import com.example.demo.DTO.InscripcionDTO;
import com.example.demo.domain.Curso;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;
import com.example.demo.domain.Status;
import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.repository.InscripcionRepository;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.time.LocalDate;
import java.util.Optional;

@Service
@Validated
public class InscripcionService {
    @Autowired
    private CursoRepository cursoRepository;
    @Autowired
    private EstudianteRepository estudianteRepository;
    @Autowired
    private InscripcionRepository inscripcionRepository;



    @Transactional
    public void inscripcion(@NotNull @Positive Long estudianteId,@NotNull @Positive Long cursoId){

        Estudiante estudianteOptional = estudianteRepository.findById(estudianteId).orElseThrow(()->new RuntimeException("el id no es valido"));

        if (!estudianteOptional.esMayorEdad()){
            throw new RuntimeException("El estudiante es menor de edad...");
        }

        Curso cursoOptional = cursoRepository.findById(cursoId).orElseThrow(()->new RuntimeException("el id no es valido"));

        Inscripcion inscripcion = new Inscripcion(null, LocalDate.now(), Status.ACEPTADO, estudianteOptional, cursoOptional);

        inscripcionRepository.saveAndFlush(inscripcion);
    }


    public InscripcionDTO saveInscripcion (InscripcionDTO inscripcionDTO){
        Inscripcion inscripcion = new Inscripcion(null,inscripcionDTO.getFechaInscripcion(),inscripcionDTO.getStatus(),inscripcionDTO.getEstudiante(),inscripcionDTO.getCurso());
        inscripcionRepository.save(inscripcion);

        return inscripcionDTO;
    }

}
