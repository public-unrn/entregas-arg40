package com.example.demo.service;

import com.example.demo.DTO.CursoDTO;
import com.example.demo.DTO.EstudianteDTO;
import com.example.demo.domain.Curso;
import com.example.demo.domain.Estudiante;
import com.example.demo.repository.CursoRepository;
import org.springframework.beans.factory.annotation.Autowired;


public class CursoService {


    @Autowired
    private CursoRepository cursoRepository;
    public CursoDTO saveCurso (CursoDTO cursoDTO){
        Curso curso = new Curso(null,cursoDTO.getNombre(),cursoDTO.getDescripcion(),cursoDTO.getFechaInicio(),cursoDTO.getFechaFin());
        cursoRepository.save(curso);

        return cursoDTO;
    }
}
