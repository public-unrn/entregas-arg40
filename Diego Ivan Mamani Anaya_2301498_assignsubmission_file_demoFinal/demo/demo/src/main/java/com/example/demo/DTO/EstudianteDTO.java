package com.example.demo.DTO;


import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor

public class EstudianteDTO {


    private String nombre;
    private String apellido;
    private String email;
    private int dni;
    private LocalDate fechaNacimiento;
    private int edad;

}
