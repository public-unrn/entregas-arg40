package com.example.demo.repository;

import com.example.demo.domain.Curso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface CursoRepository extends JpaRepository<Curso, Long> {
    @Query ("SELECT c FROM Curso c")
    List<Curso> findAllCurso();
    List<Curso> findAll();



    @Query ("SELECT c FROM Curso c c.fechaInicio > :fecha")
    List <Curso> findCursoFechaInicioMayor2020(@Param("fecha") LocalDate fecha);

    List<Curso> findByFechaInicioGreaterThan(LocalDate fecha);

    @Query (value= "SELECT * FROM curso c c.fechaInicio > :fecha", nativeQuery = true)
    List <Curso> findCursoFechaInicioMayor2020Nativo(@Param("fecha") LocalDate fecha);


}
