package com.example.demo.service;

import com.example.demo.DTO.EstudianteDTO;
import com.example.demo.domain.Estudiante;
import com.example.demo.repository.EstudianteRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class EstudianteService {


    @Autowired
    private EstudianteRepository estudianteRepository;


    public EstudianteDTO saveEstudiante (EstudianteDTO estudianteDTO){
        Estudiante estudiante = new Estudiante (null,estudianteDTO.getNombre(),estudianteDTO.getApellido(),estudianteDTO.getEmail()
                ,estudianteDTO.getDni(),estudianteDTO.getFechaNacimiento(),estudianteDTO.getEdad());
        estudianteRepository.save(estudiante);

        return estudianteDTO;
    }

    public List <EstudianteDTO> findAll (){
        return estudianteRepository.findAll().stream().map(c -> new EstudianteDTO(c.getNombre(), c.getApellido(), c.getEmail(),c.getDni(),c.getFechaNacimiento(),c.getEdad())).collect(Collectors.toList());
    }
    public EstudianteDTO update (Long id, EstudianteDTO estudianteDTO){
        Estudiante estudiante = new Estudiante(id,estudianteDTO.getNombre(),estudianteDTO.getApellido(),estudianteDTO.getEmail(),estudianteDTO.getDni(),estudianteDTO.getFechaNacimiento(),estudianteDTO.getEdad());
        estudianteRepository.save(estudiante);
        return estudianteDTO;

    }

    public EstudianteDTO find(Long id){
        Optional<Estudiante> estudianteOptional = estudianteRepository.findById(id);
        if (estudianteOptional.isEmpty()){
        throw new RuntimeException("id Invalido");
    }
        Estudiante estudiante = estudianteOptional.get();
        return new EstudianteDTO(estudiante.getNombre(),estudiante.getApellido(),estudiante.getEmail(),estudiante.getDni(),estudiante.getFechaNacimiento(),estudiante.getEdad());
}
    public void delete (Long id){
        estudianteRepository.deleteById(id);
    }
}
