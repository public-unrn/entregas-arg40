package com.example.demo.dto;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EstudianteDto {
    //private Long id;
    private String nombre;
    private String apellido;
    private String dni;
    private String email;
    private LocalDate nacimiento;
}
