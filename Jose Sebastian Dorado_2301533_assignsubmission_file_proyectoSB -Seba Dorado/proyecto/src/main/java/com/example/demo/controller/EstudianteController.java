package com.example.demo.controller;

import com.example.demo.dto.EstudianteDTO;
import com.example.demo.service.EstudianteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/estudiante")
public class EstudianteController {

    @Autowired
    private EstudianteService estudianteService;

    @PostMapping
    public EstudianteDTO guardar(@RequestBody EstudianteDTO estudianteDTO){
        return estudianteService.agregarEstudiante(estudianteDTO);
    }

    @GetMapping
    public List<EstudianteDTO> listarTodos() {
        return estudianteService.listarEstudiantes();
    }

    @GetMapping("/{id}")
    public EstudianteDTO buscar(@PathVariable Long id) {
        return estudianteService.buscarEstudiante(id);
    }

    @PutMapping("/{id}")
    public EstudianteDTO actualizar(@PathVariable Long id, @RequestBody EstudianteDTO estudianteDTO) {
        return estudianteService.actualizarEstudiante(id, estudianteDTO);
    }

    @DeleteMapping("/{id}")
    public void eliminar(@PathVariable Long id){
        estudianteService.eliminarEstudiante(id);
    }
}
