package com.example.demo.controller;

import com.example.demo.dto.InscripcionDTO;
import com.example.demo.service.InscripcionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/inscripcion")
public class InscripcionController {

    @Autowired
    private InscripcionService inscripcionService;

    @PostMapping
    public InscripcionDTO guardar(@RequestBody InscripcionDTO inscripcionDTO){
         return inscripcionService.agregarInscripcion(inscripcionDTO);
    }

    @GetMapping
    public List<InscripcionDTO> listarTodos() {
        return inscripcionService.listarInscripciones();
    }

    @GetMapping("/{id}")
    public InscripcionDTO buscar(@PathVariable Long id) {
        return inscripcionService.buscarInscripcion(id);
    }

    @PutMapping("/{id}")
    public InscripcionDTO actualizar(@PathVariable Long id, @RequestBody InscripcionDTO inscripcionDTO) {
        return inscripcionService.actualizarInscripcion(id, inscripcionDTO);
    }

    @DeleteMapping("/{id}")
    public void eliminar(@PathVariable Long id){
        inscripcionService.eliminarInscripcion(id);
    }
}
