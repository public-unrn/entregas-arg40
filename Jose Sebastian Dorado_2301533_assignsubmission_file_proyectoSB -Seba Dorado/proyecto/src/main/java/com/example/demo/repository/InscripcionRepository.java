package com.example.demo.repository;

import com.example.demo.domain.Inscripcion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InscripcionRepository extends JpaRepository<Inscripcion, Long> {

    @Query("SELECT i FROM Inscripcion i")
    List<Inscripcion> findAllInscripciones();

    @Query("SELECT i FROM Inscripcion i WHERE i.estado = 'Rechazado' OR i.estado = 'Pendiente'")
    List<Inscripcion> findByStatusInscripciones();

    @Query("SELECT i FROM Inscripcion i WHERE i.estado = :estadoTipo")
    List<Inscripcion> findByInscripciones(@Param("estadoTipo") Enum estadoTipo);

    //PARA EVALUAR
    //inscripcionRepository.findByInscripciones(Estado.Pendiente)

    @Query(value = "SELECT * FROM inscripcion i WHERE i.id = :id", nativeQuery = true)
    List<Inscripcion> findByNativeInscripciones(@Param("id") int id);
}
