package com.example.demo.service;

import com.example.demo.domain.Curso;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;
import com.example.demo.dto.InscripcionDTO;
import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.repository.InscripcionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Validated
public class InscripcionService {

    @Autowired
    private EstudianteRepository estudianteRepository;

    @Autowired
    private CursoRepository cursoRepository;

    @Autowired
    private InscripcionRepository inscripcionRepository;

    @Transactional
    public InscripcionDTO agregarInscripcion(InscripcionDTO inscripcionDTO){

        if(inscripcionDTO.getFechaDeInscripcion().isBefore(LocalDate.now()) || inscripcionDTO.getFechaDeInscripcion().isAfter(LocalDate.now())){
                throw new RuntimeException("Las fecha ingresada no es valida");
        }

        if(inscripcionDTO.getEstado() == null){
             throw new RuntimeException("El estado no fue ingresado");
        }

        Estudiante estudiante = estudianteRepository
                .findById(inscripcionDTO.getEstudiante().getId())
                .orElseThrow(() -> new RuntimeException("El id del estudiante ingresado no se encontro"));

        if(estudiante.esMenorDeEdad()){
          throw new RuntimeException("El estudiante es menor de edad");
        }

        Curso curso = cursoRepository
                .findById(inscripcionDTO.getCurso().getId())
                .orElseThrow(() -> new RuntimeException("El id del curso ingresado no se encontro"));


        Inscripcion nuevaInscripcion = new Inscripcion(
                null,
                inscripcionDTO.getFechaDeInscripcion(),
                inscripcionDTO.getEstado(),
                curso,
                estudiante
        );

        inscripcionRepository.save(nuevaInscripcion);

        return  inscripcionDTO;
    }

    public List<InscripcionDTO> listarInscripciones() {
        return inscripcionRepository.findAll()
                .stream().map(i -> new InscripcionDTO(
                        i.getFechaDeInscripcion(),
                        i.getEstado(),
                        i.getCurso(),
                        i.getEstudiante()))
                .collect(Collectors.toList());
    }

    @Transactional
    public InscripcionDTO actualizarInscripcion(Long id, InscripcionDTO inscripcionDTO){

        Optional<Inscripcion> inscripcionOptional = inscripcionRepository.findById(id);

        if(inscripcionOptional.isEmpty()){
            throw  new RuntimeException("El id ingresado de la inscripcion  no se encontro");
        }

        Inscripcion actualizarInscripcion = new Inscripcion(
                id,
                inscripcionDTO.getFechaDeInscripcion(),
                inscripcionDTO.getEstado(),
                inscripcionDTO.getCurso(),
                inscripcionDTO.getEstudiante()
        );

        inscripcionRepository.save(actualizarInscripcion);

        return inscripcionDTO;
    }

    public InscripcionDTO buscarInscripcion(Long id){

        Optional<Inscripcion> inscripcionOptional = inscripcionRepository.findById(id);

        if(inscripcionOptional.isEmpty()){
            throw  new RuntimeException("El id ingresado de la inscripcion  no se encontro");
        }

        Inscripcion inscripcion = inscripcionOptional.get();

        return new InscripcionDTO(
                inscripcion.getFechaDeInscripcion(),
                inscripcion.getEstado(),
                inscripcion.getCurso(),
                inscripcion.getEstudiante());
    }

    @Transactional
    public void eliminarInscripcion(Long id){

        Optional<Inscripcion> inscripcionOptional = inscripcionRepository.findById(id);

        if(inscripcionOptional.isEmpty()){
            throw  new RuntimeException("El id ingresado de la inscripcion  no se encontro");
        }

        inscripcionRepository.deleteById(id);

    }
}
