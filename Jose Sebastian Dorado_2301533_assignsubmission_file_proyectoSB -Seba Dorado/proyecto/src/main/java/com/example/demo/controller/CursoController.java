package com.example.demo.controller;

import com.example.demo.dto.CursoDTO;
import com.example.demo.service.CursoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/curso")
public class CursoController {

    @Autowired
    private CursoService cursoService;

    @PostMapping
    public CursoDTO guardar(@RequestBody CursoDTO cursoDTO){
        return cursoService.agregarCurso(cursoDTO);
    }

    @GetMapping
    public List<CursoDTO> listarTodos() {
        return cursoService.listarCursos();
    }

    @GetMapping("/{id}")
    public CursoDTO buscar(@PathVariable Long id) {
        return cursoService.buscarCurso(id);
    }

    @PutMapping("/{id}")
    public CursoDTO actualizar(@PathVariable Long id, @RequestBody CursoDTO cursoDTO) {
        return cursoService.actualizarCurso(id, cursoDTO);
    }

    @DeleteMapping("/{id}")
    public void eliminar(@PathVariable Long id){
        cursoService.eliminarCurso(id);
    }
}
