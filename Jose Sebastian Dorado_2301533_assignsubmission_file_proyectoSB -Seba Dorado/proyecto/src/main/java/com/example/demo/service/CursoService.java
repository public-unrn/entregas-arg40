package com.example.demo.service;

import com.example.demo.domain.Curso;
import com.example.demo.dto.CursoDTO;
import com.example.demo.repository.CursoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CursoService {

    @Autowired
    private CursoRepository cursoRepository;

    @Transactional
    public CursoDTO agregarCurso(CursoDTO cursoDTO){

        Curso curso = new Curso(
             null,
             cursoDTO.getNombre(),
             cursoDTO.getDescripcion(),
             cursoDTO.getFechaDeInicio(),
             cursoDTO.getFechaDeFin()
        );

        cursoRepository.save(curso);

        return cursoDTO;
    }

    public List<CursoDTO> listarCursos() {
        return cursoRepository.findAll()
                .stream().map(c -> new CursoDTO(
                        c.getNombre(),
                        c.getDescripcion(),
                        c.getFechaDeInicio(),
                        c.getFechaDeFin()))
                .collect(Collectors.toList());
    }

    public CursoDTO buscarCurso(Long id){

        Optional<Curso> cursoOptional = cursoRepository.findById(id);

        if(cursoOptional.isEmpty()){
            throw  new RuntimeException("El id ingresado del curso  no se encontro");
        }

        Curso curso = cursoOptional.get();

        return new CursoDTO(
                curso.getNombre(),
                curso.getDescripcion(),
                curso.getFechaDeInicio(),
                curso.getFechaDeFin()
        );
    }

    @Transactional
    public CursoDTO actualizarCurso(Long id, CursoDTO cursoDTO){

        Optional<Curso> cursoOptional = cursoRepository.findById(id);

        if(cursoOptional.isEmpty()){
            throw  new RuntimeException("El id ingresado del curso  no se encontro");
        }

        Curso curso = new Curso(
                id,
                cursoDTO.getNombre(),
                cursoDTO.getDescripcion(),
                cursoDTO.getFechaDeInicio(),
                cursoDTO.getFechaDeFin()
        );

        cursoRepository.save(curso);

        return cursoDTO;
    }

    @Transactional
    public void eliminarCurso(Long id){

        Optional<Curso> cursoOptional = cursoRepository.findById(id);

        if(cursoOptional.isEmpty()){
            throw  new RuntimeException("El id ingresado del curso  no se encontro");
        }

        cursoRepository.deleteById(id);

    }
}
