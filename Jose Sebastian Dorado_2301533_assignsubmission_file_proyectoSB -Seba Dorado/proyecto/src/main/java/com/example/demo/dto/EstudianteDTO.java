package com.example.demo.dto;

import com.example.demo.domain.Estudiante;
import lombok.*;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collector;



@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class EstudianteDTO {
    private String nombre;
    private String apellido;
    private String email;
    private Integer dni;
    private LocalDate fechaDeNacimiento;
    private int edad;
}
