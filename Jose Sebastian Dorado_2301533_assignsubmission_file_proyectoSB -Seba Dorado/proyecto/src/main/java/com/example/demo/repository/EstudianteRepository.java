package com.example.demo.repository;

import com.example.demo.domain.Estudiante;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EstudianteRepository extends JpaRepository<Estudiante, Long> {

    @Query("SELECT e FROM Estudiante e")
    List<Estudiante> findAllEstudiantes();

    @Query("SELECT e FROM Estudiante e WHERE e.dni > 20000000 AND e.apellido = 'Romero'")
    List<Estudiante> findByDniAndName();

    @Query("SELECT e FROM Estudiante e ORDER BY e.dni ASC")
    List<Estudiante> findOrderAscEstudiantes();

    List<Estudiante> findByOrderByNombreAsc();

    //Lista los estudiantes por orde asc de dni y la pagina 0 con 4 elementos
    //estudianteRepository.findAll(PageRequest.of(0, 4, Sort.by(Sort.Direction.ASC, "dni")));

    List<Estudiante> findByNombre(String nombre);

    //Devuelve un valor booleano si existe el estudiante con el nombre ingresado
    boolean existsByNombre(String nombre);

    List<Estudiante> findByApellido(String apellido);

    //Busca los estudiantes de acuerdo a la primera letra
    List<Estudiante> findByApellidoStartingWith(String prefix);

    long countByNombre(String nombre);

    long countByApellido(String apellido);
}
