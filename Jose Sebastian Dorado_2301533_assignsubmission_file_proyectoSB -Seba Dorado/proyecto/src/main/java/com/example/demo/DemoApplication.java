package com.example.demo;

import com.example.demo.domain.Curso;
import com.example.demo.domain.Estado;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;
import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.repository.InscripcionRepository;
import com.example.demo.service.InscripcionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.util.Arrays;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Autowired
	EstudianteRepository estudianteRepository ;
	@Autowired
	CursoRepository cursoRepository;

	@Autowired
	InscripcionRepository inscripcionRepository;
	//InscripcionService inscripcionService;
	private void CrearEstudiantes() {
		estudianteRepository.saveAll(Arrays.asList(
				new Estudiante(null, "Carlos", "Martinez", "carlos@hotmail.com", 29875133, LocalDate.of(1983,11,20), 40),
				new Estudiante(null, "Luis", "Chavez", "luis@hotmail.com", 32542013, LocalDate.of(1987,01,9), 36),
				new Estudiante(null, "Maria", "Alia", "maria@hotmail.com", 12725649, LocalDate.of(1958,03,4), 65),
				new Estudiante(null, "Carla", "Romero", "carla@hotmail.com", 25000456, LocalDate.of(2005,04,25), 18),
				new Estudiante(null, "Martina", "Dorado", "martina@hotmail.com", 39254112, LocalDate.of(2008,07,29), 15),
				new Estudiante(null, "Lucas", "Sanchez", "lucas@hotmail.com", 25000456, LocalDate.of(2005,02,22), 18),
				new Estudiante(null, "Luz", "Garcia", "luz@hotmail.com", 45025327, LocalDate.of(2011,05,16), 12)
		));
	}

	private void CrearCursos() {
		cursoRepository.saveAll(Arrays.asList(
				new Curso(null, "Curso de Desarrolo de Aplicaciones Autoconfigurables", "Curso Basico de Desarrolo de Aplicaciones usando intelligent", LocalDate.of(2023,6,13), LocalDate.of(2023,07,9)),
				new Curso(null, "Curso de Desarrolo de Backend", "Curso Basico de Desarrolo donde veremos node, express, mongoDB", LocalDate.of(2019,01,20), LocalDate.of(2023,8,15)),
				new Curso(null, "Curso de Desarrollo Frontend", "Curso de Desarrolo donde veremos HTML, CSS, SASS", LocalDate.now(), LocalDate.now())
		));
	}

	private void CrearInscripciones() {
		inscripcionRepository.saveAll(Arrays.asList(
				new Inscripcion(null, LocalDate.now(), Estado.Aceptado, cursoRepository.getReferenceById(1L), estudianteRepository.getReferenceById(2L)),
				new Inscripcion(null, LocalDate.now(), Estado.Pendiente, cursoRepository.getReferenceById(2L), estudianteRepository.getReferenceById(1L)),
				new Inscripcion(null, LocalDate.now(), Estado.Rechazado, cursoRepository.getReferenceById(3L), estudianteRepository.getReferenceById(5L)),
				new Inscripcion(null, LocalDate.now(), Estado.Pendiente, cursoRepository.getReferenceById(3L), estudianteRepository.getReferenceById(4L)),
				new Inscripcion(null, LocalDate.now(), Estado.Aceptado, cursoRepository.getReferenceById(1L), estudianteRepository.getReferenceById(3L)),
				new Inscripcion(null, LocalDate.now(), Estado.Aceptado, cursoRepository.getReferenceById(1L), estudianteRepository.getReferenceById(1L))
		));
	}

	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		return args -> {
			CrearEstudiantes();
			CrearCursos();
			CrearInscripciones();

			estudianteRepository.findAll();

			//Listado de los estudiantes por orden asc dni y paginacion
			estudianteRepository.findAll(PageRequest.of(0, 2, Sort.by(Sort.Direction.ASC, "dni")));
			estudianteRepository.findAll(PageRequest.of(1, 5, Sort.by(Sort.Direction.ASC, "dni")));
		};
	}
}
