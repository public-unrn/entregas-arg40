package com.example.demo.service;

import com.example.demo.domain.Estudiante;
import com.example.demo.dto.EstudianteDTO;
import com.example.demo.repository.EstudianteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class EstudianteService {

    @Autowired
    private EstudianteRepository estudianteRepository;

    @Transactional
    public EstudianteDTO agregarEstudiante(EstudianteDTO estudianteDTO){
        Estudiante estudiante = new Estudiante(
                null,
                estudianteDTO.getNombre(),
                estudianteDTO.getApellido(),
                estudianteDTO.getEmail(),
                estudianteDTO.getDni(),
                estudianteDTO.getFechaDeNacimiento(),
                estudianteDTO.getEdad()

        );

        estudianteRepository.save(estudiante);

        return  estudianteDTO;
    }

    public List<EstudianteDTO> listarEstudiantes() {
        return estudianteRepository.findAll()
                        .stream().map(e -> new EstudianteDTO(
                                                e.getNombre(),
                                                e.getApellido(),
                                                e.getEmail(),
                                                e.getDni(),
                                                e.getFechaDeNacimiento(),
                                                e.getEdad()))
                        .collect(Collectors.toList());
    }

    @Transactional
    public EstudianteDTO actualizarEstudiante(Long id, EstudianteDTO estudianteDTO){

        Optional<Estudiante> estudianteOptional = estudianteRepository.findById(id);

        if(estudianteOptional.isEmpty()){
            throw  new RuntimeException("El id ingresado del estudiante  no se encontro");
        }

        Estudiante estudiante = new Estudiante(
                id,
                estudianteDTO.getNombre(),
                estudianteDTO.getApellido(),
                estudianteDTO.getEmail(),
                estudianteDTO.getDni(),
                estudianteDTO.getFechaDeNacimiento(),
                estudianteDTO.getEdad()

        );

        estudianteRepository.save(estudiante);

        return estudianteDTO;
    }

    public EstudianteDTO buscarEstudiante(Long id){

        Optional<Estudiante> estudianteOptional = estudianteRepository.findById(id);

        if(estudianteOptional.isEmpty()){
            throw  new RuntimeException("El id ingresado del estudiante  no se encontro");
        }

        Estudiante estudiante = estudianteOptional.get();

        return new EstudianteDTO(
                    estudiante.getNombre(),
                    estudiante.getApellido(),
                    estudiante.getEmail(),
                    estudiante.getDni(),
                    estudiante.getFechaDeNacimiento(),
                    estudiante.getEdad()
        );

    }

    @Transactional
    public void eliminarEstudiante(Long id){

        Optional<Estudiante> estudianteOptional = estudianteRepository.findById(id);

        if(estudianteOptional.isEmpty()){
            throw  new RuntimeException("El id ingresado del estudiante  no se encontro");
        }

        estudianteRepository.deleteById(id);

    }
}

