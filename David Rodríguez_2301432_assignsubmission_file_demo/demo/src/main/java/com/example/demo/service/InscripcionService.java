package com.example.demo.service;

import com.example.demo.domain.Estado;
import com.example.demo.dto.InscripcionDto;
import com.example.demo.repository.*;
import com.example.demo.domain.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Validated
public class InscripcionService {
    @Autowired
    private EstudianteRepository estudianteRepository;
    @Autowired
    private CursoRepository cursoRepository;
    @Autowired
    private  InscripcionRepository inscripcionRepository;

    @Transactional
    public void incripcion (@NotNull @Positive Long estudianteId, @NotNull @Positive Long cursoID,
                                      @NotNull LocalDate fechaInsc, @NotNull Estado estado) {
        Estudiante estudiante = estudianteRepository
                .findById(estudianteId)
                .orElseThrow(() -> new RuntimeException("El ID del Estudiante no es valido"));

        estudiante.calcularEdad(); //acá necesito calcular la edad porque la tengo como atributo.
        if (!estudiante.esMayor()) {
            throw new RuntimeException("El Estudiante es menor de edad");
        }

        Curso curso = cursoRepository
                .findById(cursoID)
                .orElseThrow(() -> new RuntimeException("El ID del Curso no es valido"));

        if (curso.getFechaInicio().isAfter(curso.getFechaFin())) {
            throw new RuntimeException("Las fechas son incorrectas");
        }

        Inscripcion inscripcion = new Inscripcion(
             null,
                fechaInsc,
                estado,
                curso,
                estudiante
        );

        inscripcionRepository.save(inscripcion);

    }

    public List<InscripcionDto> findAll() {
        return inscripcionRepository.findAll()
                .stream().map(i -> new InscripcionDto(i.getFechaDeInscripcion(),
                        i.getEstado(),
                        i.getCurso().getId(),
                        i.getEstudiante().getId()))
                .collect(Collectors.toList());
    }

    public InscripcionDto findById(Long id) {
        Optional<Inscripcion> inscripcionOptional = inscripcionRepository.findById(id);

        if (inscripcionOptional.isEmpty()) {
            throw new RuntimeException("El id no es valido");
        }

        Inscripcion inscripcion = inscripcionOptional.get();

        return new InscripcionDto(
                inscripcion.getFechaDeInscripcion(),
                inscripcion.getEstado(),
                inscripcion.getCurso().getId(),
                inscripcion.getEstudiante().getId()
        );
    }

    public InscripcionDto update(Long id, InscripcionDto inscripcionDto) {

        Inscripcion inscripcion = new Inscripcion(
                id,
                inscripcionDto.getFechaDeInscripcion(),
                inscripcionDto.getEstado(),
                cursoRepository.findById(inscripcionDto.getCurso()).get(),
                estudianteRepository.findById(inscripcionDto.getEstudiante()).get()
        );

        inscripcionRepository.save(inscripcion);

        return inscripcionDto;
    }

    public void delete(Long id) {
        inscripcionRepository.deleteById(id);
    }


}
