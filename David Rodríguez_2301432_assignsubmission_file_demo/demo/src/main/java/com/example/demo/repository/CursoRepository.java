package com.example.demo.repository;

import com.example.demo.domain.Curso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.time.LocalDate;
import java.util.List;

public interface CursoRepository extends JpaRepository<Curso, Long> {

    @Query("SELECT c FROM Curso c")
    List <Curso> findAllCursosQ();

    List<Curso> findAll();

    @Query("SELECT c FROM Curso c WHERE c.fechaInicio > :fecha")
    List<Curso> findByFechaInicioMayorQue(@Param("fecha")LocalDate fecha);

    List<Curso> findByFechaInicioAfter(LocalDate fecha);
}
