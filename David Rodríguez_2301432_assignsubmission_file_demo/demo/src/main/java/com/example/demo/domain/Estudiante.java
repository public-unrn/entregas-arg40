package com.example.demo.domain;

import jakarta.persistence.*;
import lombok.*;
import java.time.LocalDate;
import java.time.Period;

@Entity
@Table(name = "estudiante")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Estudiante {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "apellido")
    private String apellido;

    @Column(name = "email")
    private String email;

    @Column(name = "dni")
    private int dni;

    @Column(name = "fecha_de_nacimiento")
    private LocalDate fechaDeNacimiento;

    @Transient
    private int edad;

    public int getEdad() {   //redefino el getter de edad para que la calcule primero y tenga sentido
        this.calcularEdad();
        return edad;
    }

    public void calcularEdad() {

        edad = Period.between(fechaDeNacimiento,LocalDate.now()).getYears();
    }

    public boolean esMayor(){

        return edad >= 18;
    }
    /*
     teniendo en cuenta que edad es una variable debe de ser evaluada previo al calculo
     de si es o no mayor de edad, podria incluirlo directamente en el metodo pero no lo
     hago teniendo en cuenta las caracteristicas del modelo.
     lo que haria en todo caso seria no tener edad como variable y devolver:
     return Period.between(fechaDeNacimiento,LocalDate.now()).getYears() >= 18
    */
}
