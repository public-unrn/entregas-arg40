package com.example.demo.dto;

import com.example.demo.domain.Estado;
import lombok.AllArgsConstructor;
import lombok.Data;
import java.time.LocalDate;

@Data
@AllArgsConstructor
public class InscripcionDto {

    private LocalDate fechaDeInscripcion;
    private Estado estado;
    private Long curso;
    private Long estudiante;

}
