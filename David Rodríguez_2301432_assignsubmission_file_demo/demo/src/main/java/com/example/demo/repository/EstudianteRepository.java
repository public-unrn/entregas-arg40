package com.example.demo.repository;

import com.example.demo.domain.Estudiante;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;

public interface EstudianteRepository extends JpaRepository<Estudiante, Long> {

    @Query("SELECT e FROM Estudiante e")
    List<Estudiante> findAllEstudiantes();

    List<Estudiante> findAll(); // cambio los nombres de los mensajes para no sobreescribir,


    @Query("SELECT e FROM Estudiante e WHERE e.dni > 20000000 AND e.apellido = 'Romero'")
    List<Estudiante> findAllEstudiantes20MyRomero();

    List<Estudiante> findByDniGreaterThanAndApellido(int dni, String apellido);


}
