package com.example.demo.service;

import com.example.demo.domain.Estudiante;
import com.example.demo.dto.EstudianteDto;
import com.example.demo.repository.EstudianteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class EstudianteService {

    @Autowired
    EstudianteRepository estudianteRepository;

    public EstudianteDto saveEstudiante(EstudianteDto estudianteDto) {
        Estudiante estudiante = new Estudiante(
                null,
                estudianteDto.getNombre(),
                estudianteDto.getApellido(),
                estudianteDto.getEmail(),
                estudianteDto.getDni(),
                estudianteDto.getFechaDeNacimiento(),
                estudianteDto.getEdad()
        );

        estudianteRepository.save(estudiante);

        return estudianteDto;
    }

    public List<EstudianteDto> findAll() {
        return estudianteRepository.findAll()
                .stream().map(e -> new EstudianteDto(e.getNombre(),e.getApellido(),
                        e.getEmail(), e.getDni(), e.getFechaDeNacimiento(), e.getEdad()))
                            .collect(Collectors.toList());
    }

    public EstudianteDto findById(Long id) {
        Optional<Estudiante> estudianteOptional = estudianteRepository.findById(id);

        if (estudianteOptional.isEmpty()) {
            throw new RuntimeException("El id no es valido");
        }

        Estudiante estudiante = estudianteOptional.get();
        estudiante.calcularEdad();

        return new EstudianteDto(
                estudiante.getNombre(),
                estudiante.getApellido(),
                estudiante.getEmail(),
                estudiante.getDni(),
                estudiante.getFechaDeNacimiento(),
                estudiante.getEdad()
        );
    }

    public EstudianteDto update(Long id, EstudianteDto estudianteDto) {

        Estudiante estudiante = new Estudiante(
                id,
                estudianteDto.getNombre(),
                estudianteDto.getApellido(),
                estudianteDto.getEmail(),
                estudianteDto.getDni(),
                estudianteDto.getFechaDeNacimiento(),
                estudianteDto.getEdad() );

        estudianteRepository.save(estudiante);

        return estudianteDto;
    }

    public void delete(Long id) {
        estudianteRepository.deleteById(id);
    }
}
