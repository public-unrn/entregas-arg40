package com.example.demo.repository;

import com.example.demo.domain.Estado;
import com.example.demo.domain.Inscripcion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.List;

public interface InscripcionRepository extends JpaRepository<Inscripcion, Long> {

    @Query("SELECT i FROM Inscripcion i where i.estado = RECHAZADA OR i.estado = PENDIENTE")
    List<Inscripcion> findByRechazadaOPendiente();

    List<Inscripcion> findByEstadoOrEstado(Estado estado1, Estado estado2);

    @Query("SELECT i FROM Inscripcion i WHERE i.estado = :estad")
    List<Inscripcion> findByEstadoParam(@Param("estad") Estado estad);

    List<Inscripcion> findByEstado(Estado estado);

    @Query(value = "SELECT distinct i.* FROM Inscripcion as i WHERE i.estado = :estad", nativeQuery = true)
    List<Inscripcion> findByEstadoNativaParam(@Param("estad") int estad);  //hay que usar el Ordinal y no el ESTADO



}
