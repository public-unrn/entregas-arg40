package com.example.demo;

import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.repository.InscripcionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;


@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Autowired
	CursoRepository cursoRepository;
	@Autowired
	EstudianteRepository estudianteRepository;
	@Autowired
	InscripcionRepository inscripcionRepository;
	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		return args -> {

/*
//		Generacion de ESTUDIANTES

			Utilidades utilidades = new Utilidades();
			for (int i = 0; i < 20; i++) {
				Estudiante estudiante = new Estudiante();
				estudiante.setNombre(utilidades.randomName());
				estudiante.setApellido(utilidades.randomLastName());
				estudiante.setDni(utilidades.randomDNI());
				estudiante.setFechaDeNacimiento(utilidades.randomBirthDate());
				estudiante.setEmail((utilidades.randomMail()));
				estudiante.calcularEdad();

				estudianteRepository.save(estudiante);
			}

//		Generacion de Inscripciones

			for (int i = 1; i < 21; i++) {
				Inscripcion inscripcion = new Inscripcion();

				Optional<Estudiante> estudiante = estudianteRepository.findById((long) i);
				Optional<Curso> curso = cursoRepository.findById((long) (Math.random()*3+1));

				inscripcion.setCurso(curso.get());
				inscripcion.setEstudiante(estudiante.get());
				inscripcion.setFechaDeInscripcion(LocalDate.of(2023,05,01));
				int estadoRNG = ((int)(Math.random()*3+1));
				if (estadoRNG == 1) {
					inscripcion.setEstado(Estado.ACEPTADA);
				} else if (estadoRNG == 2) {
					inscripcion.setEstado(Estado.PENDIENTE);
				}else {
					inscripcion.setEstado(Estado.RECHAZADA);
				}

				inscripcionRepository.save(inscripcion);
			}
*/
			estudianteRepository.findAll(PageRequest.of(1, 5, Sort.by(Sort.Direction.ASC, "dni")));
			estudianteRepository.findAll(PageRequest.of(0, 2, Sort.by(Sort.Direction.ASC, "dni")));
			inscripcionRepository.findAll();


		};
	}
}
