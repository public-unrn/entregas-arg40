package com.example.demo.controller;

import com.example.demo.dto.CursoDto;
import com.example.demo.service.CursoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/curso")
public class CursoController {

    @Autowired
    private CursoService cursoService;

    @PostMapping                        // Post: Crear
    public CursoDto save (@RequestBody CursoDto cursoDto) {
        return cursoService.saveCurso(cursoDto);
    }

    @GetMapping
    public List<CursoDto> all (){

        return cursoService.findAll();
    }

    @GetMapping("/{id}")                // Get: Obtener
    public CursoDto find (@PathVariable Long id) { //para usar la variable que se le pasa y buscar por ese id

        return cursoService.findById(id);
    }

    @PutMapping("/{id}")                // Put: Editar
    public CursoDto update (@PathVariable Long id, @RequestBody CursoDto cursoDto) {
        return cursoService.update(id, cursoDto);
    }

    @DeleteMapping("/{id}")             // Delete: Borrrar
    public void delete (@PathVariable Long id) {
        cursoService.delete(id);
    }
}
