package com.example.demo.service;

import com.example.demo.domain.Curso;
import com.example.demo.dto.CursoDto;
import com.example.demo.repository.CursoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CursoService {

    @Autowired
    CursoRepository cursoRepository;

    public CursoDto saveCurso(CursoDto cursoDto) {
        Curso curso = new Curso(
                null,
                cursoDto.getNombre(),
                cursoDto.getDescripcion(),
                cursoDto.getFechaInicio(),
                cursoDto.getFechaFin()
        );

        if (cursoDto.getFechaInicio().isAfter(cursoDto.getFechaFin())) {
            throw new RuntimeException("Las fechas no son validas");
        }

        cursoRepository.save(curso);

        return cursoDto;
    }

    public List<CursoDto> findAll() {
        return cursoRepository.findAll()
                .stream().map(c -> new CursoDto(c.getNombre(),
                        c.getDescripcion(),
                        c.getFechaInicio(),
                        c.getFechaFin()))
                .collect(Collectors.toList());
    }

    public CursoDto findById(Long id) {
        Optional<Curso> cursoOptional = cursoRepository.findById(id);

        if (cursoOptional.isEmpty()) {
            throw new RuntimeException("El id no es valido");
        }

        Curso curso = cursoOptional.get();

        return new CursoDto(
                curso.getNombre(),
                curso.getDescripcion(),
                curso.getFechaInicio(),
                curso.getFechaFin()
        );
    }

    public CursoDto update(Long id, CursoDto cursoDto) {

        Curso curso = new Curso(
                id,
                cursoDto.getNombre(),
                cursoDto.getDescripcion(),
                cursoDto.getFechaInicio(),
                cursoDto.getFechaFin()
        );

        cursoRepository.save(curso);

        return cursoDto;
    }

    public void delete(Long id) {
        cursoRepository.deleteById(id);
    }
}
