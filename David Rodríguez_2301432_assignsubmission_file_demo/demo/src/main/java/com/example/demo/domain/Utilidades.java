package com.example.demo.domain;

import java.time.LocalDate;

public class Utilidades {

    public String randomName(){
        String[] nombres = {
                "Mateo", "Sofia", "Alejandro", "Isabela", "Santiago", "Valentina", "Mia", "Natalia", "pilar",
                "Dante", "Enzo", "Facundo", "Florencia", "Thiago", "Juan", "Ana"
        };
        return nombres[(int)(Math.random()*16)];
    }

    public String randomLastName(){
        String[] apellidos = {
                "Garcia", "Romero", "Fernandez", "Gonzalez", "Lopez", "Martinez", "Sanchez", "Perez",
                "Ricci", "Ferretti", "Cerroni", "Betti", "Santoni", "Martini", "Giuliani", "Mancini"
        };
        return apellidos[(int)(Math.random()*16)];
    }

    public int randomDNI () {
        return (int)(Math.random()*(40000000-10000000+1)+10000000);
    }

    public String randomMail () {
        String[] usuario = {
                "Lyon", "Fox", "Deer", "Cat", "Bear", "Monkey", "Goat", "Cow", "Chicken", "Duck",
                "Leopard", "Kangaroo",
        };
        String[] dominio = {
                "Gmail.com", "Yahoo.com", "Hotmail.com", "Outlook.com"
        };
        return usuario[((int)(Math.random()*12))]+"@"+dominio[((int)(Math.random()*4))];
    }

    public LocalDate randomBirthDate () {
        int dia, mes, anho;
        anho = (int) (Math.random()*(2004-1960)+1960);
        mes = (int) (Math.random()*12+1);
        if ((mes == 4)||(mes == 6)||(mes == 9)||(mes == 11)) {
            dia = (int) (Math.random()*30+1);
        } else if (mes == 2) {
            dia = (int) (Math.random()*28+1);
        } else {
            dia = (int) (Math.random()*31+1);
        }
        return LocalDate.of(anho, mes, dia);
    }
}
