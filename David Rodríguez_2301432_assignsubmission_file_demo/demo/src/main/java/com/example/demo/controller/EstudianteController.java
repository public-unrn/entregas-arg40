package com.example.demo.controller;

import com.example.demo.dto.EstudianteDto;
import com.example.demo.service.EstudianteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/estudiante")
public class EstudianteController {

    @Autowired
    private EstudianteService estudianteService;

    @PostMapping                        // Post: Crear
    public EstudianteDto save (@RequestBody EstudianteDto estudianteDto) {
        return estudianteService.saveEstudiante(estudianteDto);
    }

    @GetMapping
    public List<EstudianteDto> all (){
        return estudianteService.findAll();
    }

    @GetMapping("/{id}")                // Get: Obtener
    public EstudianteDto find (@PathVariable Long id) { //para usar la variable que se le pasa y buscar por ese id
        return estudianteService.findById(id);
    }

    @PutMapping("/{id}")                // Put: Editar
    public EstudianteDto update (@PathVariable Long id, @RequestBody EstudianteDto estudianteDto) {
        return estudianteService.update(id, estudianteDto);
    }

    @DeleteMapping("/{id}")             // Delete: Borrrar
    public void delete (@PathVariable Long id) {
        estudianteService.delete(id);
    }
}
