package com.example.demo.domain;

import jakarta.persistence.*;
import lombok.*;
import java.time.LocalDate;

@Entity
@Table(name = "inscripcion")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Inscripcion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "fecha_de_inscripcion")
    private LocalDate fechaDeInscripcion;

    @Column(name = "estado")
    private Estado estado;

    @ManyToOne (fetch = FetchType.LAZY)
    @JoinColumn(name = "curso")
    private Curso curso;

    @ManyToOne (fetch = FetchType.LAZY)
    @JoinColumn(name = "estudiante")
    private Estudiante estudiante;

}
