package com.example.demo.controller;

import com.example.demo.dto.InscripcionDto;
import com.example.demo.service.InscripcionService;
import jakarta.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/inscripcion")
public class InscripcionController {

    @Autowired
    InscripcionService inscripcionService;

    @PostMapping
    public void save (@RequestBody @NotNull InscripcionDto inscripcionDto){
        inscripcionService.incripcion(
                inscripcionDto.getEstudiante(),
                inscripcionDto.getCurso(),
                inscripcionDto.getFechaDeInscripcion(),
                inscripcionDto.getEstado()
        );
    }

    @GetMapping
    public List<InscripcionDto> all (){

        return inscripcionService.findAll();
    }

    @GetMapping("/{id}")                // Get: Obtener
    public InscripcionDto find (@PathVariable Long id) { //para usar la variable que se le pasa y buscar por ese id

        return inscripcionService.findById(id);
    }

    @PutMapping("/{id}")                // Put: Editar
    public InscripcionDto update (@PathVariable Long id, @RequestBody InscripcionDto inscripcionDto) {
        return inscripcionService.update(id, inscripcionDto);
    }

    @DeleteMapping("/{id}")             // Delete: Borrrar
    public void delete (@PathVariable Long id) {
        inscripcionService.delete(id);
    }
}
