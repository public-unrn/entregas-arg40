package com.example.demo.repository;

import com.example.demo.domain.Estudiante;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EstudianteRepository {
    import org.springframework.data.domain.Page;
    import org.springframework.data.domain.Pageable;
    import org.springframework.data.jpa.repository.JpaRepository;
    import java.util.List;

    public interface EstudianteRepository extends JpaRepository<Estudiante, Long> {
        List<Estudiante> findByDniGreaterThanAndApellidoEquals(String dni, String apellido);
        Page<Estudiante> findAllByOrderByDniAsc(Pageable pageable);
    }

}
