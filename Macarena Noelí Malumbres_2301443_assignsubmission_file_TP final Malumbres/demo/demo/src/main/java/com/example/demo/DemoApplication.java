package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.time.LocalDate;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Autowired
	CustomerRepository customerRepository;

	@Bean
	public CommandLineRunner commandLineRunner (ApplicationContext ctx) {
		return args -> {
			Customer customer = new Customer();

			customer.setFirstname("Ale");
			customer.setLastname("Huaiquilican");
			customer.setBirthDate(LocalDate.now());
			customer.setPhoneNumber("123456789");
			customer.setEmail("ale@email");

			customerRepository.findAll();

			customerRepository.save(customer);

			customerRepository.findAll();
		};
	}
}
