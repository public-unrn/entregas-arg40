package com.example.demo.repository;

import com.example.demo.domain.Curso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.w3c.dom.stylesheets.LinkStyle;

import java.time.LocalDate;

public interface CursoRepository {
    import org.springframework.data.jpa.repository.JpaRepository;
    import java.time.LocalDate;
    import java.util.List;

    public interface CursoRepository extends JpaRepository<Curso, Long> {
        LinkStyle<Curso> findByFechaInicioAfter(LocalDate fecha);
    }

}
