package com.example.demo.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Table(name = "estudiante")
@Getter
@Setter
public class Estudiante {
    import javax.persistence.*;
    import java.time.LocalDate;

    public class Estudiante {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long id;
        private String nombre;
        private String apellido;
        private String email;
        private String dni;
        private LocalDate fechaNacimiento;

    }

}
