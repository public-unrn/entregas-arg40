package com.example.demo.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

public class Curso {
    import javax.persistence.*;
    import java.time.LocalDate;

    @Entity
    @Table(name = "curso")
    @Getter
    @Setter
    public class Curso {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long id;
        private String nombre;
        private String descripcion;
        private LocalDate fechaInicio;
        private LocalDate fechaFin;

}
