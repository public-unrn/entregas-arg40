package com.example.demo.repository;

import com.example.demo.domain.Inscripcion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface InscripcionRepository {
    import org.springframework.data.jpa.repository.JpaRepository;
    import java.util.List;

    public interface InscripcionRepository extends JpaRepository<Inscripcion, Long> {
        List<Inscripcion> findByEstadoIn(String... estados);
        List<Inscripcion> findByEstado(String estado);
    }

}
