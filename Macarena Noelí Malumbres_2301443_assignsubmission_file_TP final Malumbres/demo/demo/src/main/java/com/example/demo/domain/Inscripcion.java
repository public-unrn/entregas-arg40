package com.example.demo.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

public class Inscripcion {
    import javax.persistence.*;
    import java.time.LocalDate;

    @Getter
    @Setter
    @Table(name = "inscripcion")
    @Entity
    public class Inscripcion {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long id;
        private LocalDate fechaInscripcion;
        private String estado;

        @ManyToOne
        @JoinColumn(name = "curso_id")
        private Curso curso;

        @ManyToOne
        @JoinColumn(name = "estudiante_id")
        private Estudiante estudiante;

    }

}
