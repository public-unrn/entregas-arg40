package com.example.tpspring.controllers;

import com.example.tpspring.DTO.InscripcionDTO;
import com.example.tpspring.model.Inscripcion;
import com.example.tpspring.services.InscripcionServiceImp;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/inscripcion")
public class InscripcionController {

    InscripcionServiceImp inscripcionServiceImp;

    public InscripcionController(InscripcionServiceImp inscripcionServiceImp) {
        this.inscripcionServiceImp = inscripcionServiceImp;
    }

    @PostMapping
    public void registrarInscripcion(@RequestBody InscripcionDTO inscripcionDTO) {
        Inscripcion inscripcion = inscripcionServiceImp.convertirInscripcionDTOaInscripcion(inscripcionDTO);
        inscripcionServiceImp.registrarInscripcion(inscripcion);
    }


}
