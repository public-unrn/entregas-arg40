package com.example.tpspring.services;

import com.example.tpspring.DTO.EstudianteDTO;
import com.example.tpspring.model.Estudiante;
import org.springframework.data.domain.Page;

import java.util.List;

public interface EstudianteService {
    void guardarEstudiante(EstudianteDTO estudiantedto);
    List<Estudiante> listarEstudiantes();
    List<Estudiante> buscarEstudiantePorDniYApellido(Integer dni,String apellido);
    Page<Estudiante> listarEstudiantesPaginadosOrdenadosPorDni(int pagina, int tamaño);
    void editarEstudiante(Long id, EstudianteDTO estudianteDTO);
    void eliminarEstudiante(Long id);
    Estudiante buscarEstudiantePorId(Long id);
}
