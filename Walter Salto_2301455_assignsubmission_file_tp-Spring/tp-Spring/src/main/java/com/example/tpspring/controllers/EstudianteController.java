package com.example.tpspring.controllers;

import com.example.tpspring.DTO.EstudianteDTO;

import com.example.tpspring.model.Estudiante;
import com.example.tpspring.services.EstudianteServiceImp;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/estudiante")
public class EstudianteController {

    EstudianteServiceImp estudianteServiceImp;

    public EstudianteController(EstudianteServiceImp estudianteServiceImp) {
        this.estudianteServiceImp = estudianteServiceImp;
    }

    @GetMapping
    public List<Estudiante> obtenerEstudiantes() {
        List<Estudiante> estudiantes = estudianteServiceImp.listarEstudiantes();
        return estudiantes;
    }

    @PostMapping
    public void guardarEstudiante(@RequestBody EstudianteDTO estudianteDTO){
        estudianteServiceImp.guardarEstudiante(estudianteDTO);
    }

    @GetMapping({"{id}"})
    public Estudiante buscarEstudiantePorId(@PathVariable Long id){

        return estudianteServiceImp.buscarEstudiantePorId(id);
    }

    @PutMapping("/{id}")
    public void editarEstudiante(@PathVariable Long id, @RequestBody EstudianteDTO estudianteDTO) {
        estudianteServiceImp.editarEstudiante(id, estudianteDTO);
    }

    @DeleteMapping("/{id}")
    public void eliminarEstudiante(@PathVariable Long id) {
        estudianteServiceImp.eliminarEstudiante(id);
    }

}
