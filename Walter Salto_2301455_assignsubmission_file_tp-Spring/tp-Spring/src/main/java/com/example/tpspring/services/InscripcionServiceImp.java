package com.example.tpspring.services;

import com.example.tpspring.DTO.InscripcionDTO;
import com.example.tpspring.enums.EstadoInscripcion;
import com.example.tpspring.model.Curso;
import com.example.tpspring.model.Estudiante;
import com.example.tpspring.model.Inscripcion;
import com.example.tpspring.repository.CursoRepository;
import com.example.tpspring.repository.EstudianteRepository;
import com.example.tpspring.repository.InscripcionRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.Period;
import java.util.Arrays;
import java.util.List;

@Service
public class InscripcionServiceImp implements InscripcionService{

    private  InscripcionRepository inscripcionRepository;
    private  EstudianteRepository estudianteRepository;
    private  CursoRepository cursoRepository;

    public InscripcionServiceImp(InscripcionRepository inscripcionRepository, EstudianteRepository estudianteRepository, CursoRepository cursoRepository) {
        this.inscripcionRepository = inscripcionRepository;
        this.estudianteRepository = estudianteRepository;
        this.cursoRepository = cursoRepository;
    }

    @Override
    public List<Inscripcion> obtenerInscripcionesRechazadasYPendientes() {
        List<EstadoInscripcion> estadosFiltrados = Arrays.asList(EstadoInscripcion.RECHAZADA, EstadoInscripcion.PENDIENTE);
        return inscripcionRepository.findByEstadoIn(estadosFiltrados);
    }

    @Override
    public List<Inscripcion> listarInscripcionesPorEstado(EstadoInscripcion estado) {
        return inscripcionRepository.findByEstado(estado);    }

    @Override
    public List<Inscripcion> listarInscripcionesPorEstadoConsultaNativa(EstadoInscripcion estado) {
        return inscripcionRepository.findByEstadoNativeQuery(estado);
    }

    @Override
    @Transactional
    public void registrarInscripcion(Inscripcion inscripcion) {
        if (!esMayorDeEdad(inscripcion.getEstudiante().getFechaDeNacimiento())) {
            throw new IllegalArgumentException("El estudiante debe ser mayor de edad");
        }

        inscripcion.setFechaInscripcion(LocalDate.now());
        inscripcion.setEstado(EstadoInscripcion.PENDIENTE);

        inscripcionRepository.save(inscripcion);
    }


    private boolean esMayorDeEdad(LocalDate fechaNacimiento) {
        LocalDate fechaActual = LocalDate.now();
        return Period.between(fechaNacimiento, fechaActual).getYears() >= 18;
    }

    public Inscripcion convertirInscripcionDTOaInscripcion(InscripcionDTO inscripcionDTO) {
        Inscripcion inscripcion = new Inscripcion();
        inscripcion.setFechaInscripcion(inscripcionDTO.getFechaInscripcion());
        inscripcion.setEstado(inscripcionDTO.getEstado());

        Estudiante estudiante = estudianteRepository.findById(inscripcionDTO.getEstudianteId())
                .orElseThrow(() -> new IllegalArgumentException("No se encontró el estudiante con el ID proporcionado"));
        inscripcion.setEstudiante(estudiante);

        Curso curso = cursoRepository.findById(inscripcionDTO.getCursoId())
                .orElseThrow(() -> new IllegalArgumentException("No se encontró el curso con el ID proporcionado"));
        inscripcion.setCurso(curso);

        return inscripcion;
    }


}
