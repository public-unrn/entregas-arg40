package com.example.tpspring.repository;

import com.example.tpspring.enums.EstadoInscripcion;
import com.example.tpspring.model.Curso;
import com.example.tpspring.model.Inscripcion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

@Repository
public interface InscripcionRepository extends JpaRepository<Inscripcion, Long> {

    List<Inscripcion> findAll();

    Optional<Inscripcion> findById(Long id);

    void deleteById(Long id);

    Inscripcion save(Inscripcion inscripcion);

    List<Inscripcion> findByEstadoIn(List<EstadoInscripcion> estados);

    List<Inscripcion> findByEstado(EstadoInscripcion estado);

    @Query(value = "SELECT * FROM inscripcion WHERE estado = ?1", nativeQuery = true)
    List<Inscripcion> findByEstadoNativeQuery(EstadoInscripcion estado);
}
