package com.example.tpspring.model;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "estudiante")
public class Estudiante {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "estudiante_id")
    public Long id;

    @Column(name = "nombre")
    public String nombre;

    @Column(name = "apellido")
    public String apellido;

    @Column(name = "email")
    public String email;

    @Column(name = "dni")
    public Integer dni;

    @Column(name = "fecha_de_nacimiento")
    public LocalDate fechaDeNacimiento;

    @Column(name = "edad")
    public Integer edad;

}
