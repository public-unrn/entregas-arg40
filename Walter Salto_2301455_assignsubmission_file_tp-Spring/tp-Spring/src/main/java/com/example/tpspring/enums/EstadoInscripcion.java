package com.example.tpspring.enums;

public enum EstadoInscripcion {
    ACEPTADA,
    RECHAZADA,
    PENDIENTE
}
