package com.example.tpspring.DTO;

import com.example.tpspring.enums.EstadoInscripcion;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
@Getter
@Setter
public class InscripcionDTO {
    private Long id;
    private LocalDate fechaInscripcion;
    private EstadoInscripcion estado;
    private Long cursoId;
    private Long estudianteId;
}
