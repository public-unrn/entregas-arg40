package com.example.tpspring.controllers;

import com.example.tpspring.DTO.CursoDTO;
import com.example.tpspring.services.CursoServiceImp;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/curso")
public class CursoController {


    CursoServiceImp cursoServiceImp;

    public CursoController(CursoServiceImp cursoServiceImp) {
        this.cursoServiceImp = cursoServiceImp;
    }

    @PostMapping
    public void guardarCurso(@RequestBody CursoDTO cursoDTO) {
        cursoServiceImp.guardarCurso(cursoDTO);
    }

}
