package com.example.tpspring.repository;


import com.example.tpspring.model.Estudiante;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

@Repository
public interface EstudianteRepository extends JpaRepository<Estudiante,Long> {

    List<Estudiante> findAll();

    Optional<Estudiante> findById(Long id);

    void deleteById(Long id);

    Estudiante save(Estudiante estudiante);

    List<Estudiante> findByDniGreaterThanAndApellido(int dni, String apellido);

    Estudiante findEstudianteById(Long estudiante);


}
