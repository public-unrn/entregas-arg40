package com.example.tpspring.repository;

import com.example.tpspring.model.Curso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface CursoRepository extends JpaRepository<Curso,Long> {


    List<Curso> findAll();

    Optional<Curso> findById(Long id);

    void deleteById(Long id);

    Curso save(Curso curso);

    List<Curso> findByFechaInicioAfter(LocalDate fecha);
}
