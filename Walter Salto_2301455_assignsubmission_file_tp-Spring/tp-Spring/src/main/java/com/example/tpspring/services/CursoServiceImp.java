package com.example.tpspring.services;

import com.example.tpspring.DTO.CursoDTO;
import com.example.tpspring.model.Curso;

import com.example.tpspring.repository.CursoRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class CursoServiceImp implements CursoService{

    private final CursoRepository cursoRepository;

    public CursoServiceImp(CursoRepository cursoRepository) {
        this.cursoRepository = cursoRepository;
    }

    @Override
    public List<Curso> listarCurso() {
        return cursoRepository.findAll();
    }

    @Override
    public List<Curso> listarCursosEmpezadosDespuesDe(LocalDate fecha) {
        return cursoRepository.findByFechaInicioAfter(fecha);
    }


    @Override
    public void guardarCurso(CursoDTO cursoDTO) {
        Curso curso = convertirCursoDTOaCurso(cursoDTO);
        cursoRepository.save(curso);
    }

    private Curso convertirCursoDTOaCurso(CursoDTO cursoDTO) {
        Curso curso = new Curso();
        curso.setNombre(cursoDTO.getNombre());
        curso.setDescripcion(cursoDTO.getDescripcion());
        curso.setFechaInicio(cursoDTO.getFechaInicio());
        curso.setFechaFin(cursoDTO.getFechaFin());
        return curso;
    }

}
