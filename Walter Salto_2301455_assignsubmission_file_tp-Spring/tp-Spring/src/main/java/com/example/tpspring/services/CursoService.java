package com.example.tpspring.services;

import com.example.tpspring.DTO.CursoDTO;
import com.example.tpspring.model.Curso;

import java.time.LocalDate;
import java.util.List;

public interface CursoService {

    List<Curso> listarCurso();
    List<Curso> listarCursosEmpezadosDespuesDe(LocalDate fecha);
    void guardarCurso(CursoDTO cursoDTO);
}
