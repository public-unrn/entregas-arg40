package com.example.tpspring.DTO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class EstudianteDTO {


    @JsonIgnore
    public Long id;

    public String nombre;

    public String apellido;

    public String email;

    public Integer dni;

    public LocalDate fechaDeNacimiento;


}
