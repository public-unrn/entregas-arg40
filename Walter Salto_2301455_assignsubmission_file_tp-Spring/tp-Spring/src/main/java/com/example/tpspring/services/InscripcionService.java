package com.example.tpspring.services;

import com.example.tpspring.enums.EstadoInscripcion;
import com.example.tpspring.model.Inscripcion;

import java.util.List;

public interface InscripcionService {

    List<Inscripcion> obtenerInscripcionesRechazadasYPendientes();
    List<Inscripcion> listarInscripcionesPorEstado(EstadoInscripcion estado);
    List<Inscripcion> listarInscripcionesPorEstadoConsultaNativa(EstadoInscripcion estado);
    void registrarInscripcion(Inscripcion inscripcion);

}
