package com.example.tpspring.services;


import com.example.tpspring.DTO.EstudianteDTO;
import com.example.tpspring.model.Estudiante;
import com.example.tpspring.repository.EstudianteRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.util.List;

@Service
public class EstudianteServiceImp implements EstudianteService  {

    EstudianteRepository estudianteRepository;

    public EstudianteServiceImp(EstudianteRepository estudianteRepository) {
        this.estudianteRepository = estudianteRepository;
    }

    @Override
    public List<Estudiante> listarEstudiantes() {
        return estudianteRepository.findAll();
    }

    @Override
    public List<Estudiante> buscarEstudiantePorDniYApellido(Integer dni, String apellido) {
        return estudianteRepository.findByDniGreaterThanAndApellido(dni,apellido);
    }

    @Override
    public Page<Estudiante> listarEstudiantesPaginadosOrdenadosPorDni(int pagina, int tamaño) {
        Pageable pageable = PageRequest.of(pagina, tamaño, Sort.by("dni").ascending());
        return estudianteRepository.findAll(pageable);
    }

    @Override
    public Estudiante buscarEstudiantePorId(Long id) {
        return estudianteRepository.findEstudianteById(id);
    }
    @Override
    public void editarEstudiante(Long id, EstudianteDTO estudianteDTO) {
        Estudiante estudianteExistente = estudianteRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Estudiante no encontrado con el ID proporcionado"));

        // Aplicar modificaciones al estudiante existente
        estudianteExistente.setNombre(estudianteDTO.getNombre());
        estudianteExistente.setApellido(estudianteDTO.getApellido());
        estudianteExistente.setEmail(estudianteDTO.getEmail());
        estudianteExistente.setDni(estudianteDTO.getDni());
        estudianteExistente.setFechaDeNacimiento(estudianteDTO.getFechaDeNacimiento());

        // Guardar los cambios en la base de datos
        estudianteRepository.save(estudianteExistente);
    }


    @Override
    public void eliminarEstudiante(Long id) {
        estudianteRepository.deleteById(id);
    }

    @Override
    public void guardarEstudiante(EstudianteDTO estudianteDTO) {

        Estudiante estudiante = convertirEstudianteDTOaEstudiante(estudianteDTO);
        estudianteRepository.save(estudiante);
    }

    private Estudiante convertirEstudianteDTOaEstudiante(EstudianteDTO estudianteDTO) {
        Estudiante estudiante = new Estudiante();
        estudiante.setNombre(estudianteDTO.getNombre());
        estudiante.setApellido(estudianteDTO.getApellido());
        estudiante.setEmail(estudianteDTO.getEmail());
        estudiante.setDni(estudianteDTO.getDni());
        estudiante.setFechaDeNacimiento(estudianteDTO.getFechaDeNacimiento());
        estudiante.setEdad(calcularEdad(estudianteDTO.getFechaDeNacimiento()));


        return estudiante;
    }


    private Integer calcularEdad(LocalDate fechaDeNacimiento) {
        LocalDate fechaActual = LocalDate.now();
        Period periodo = Period.between(fechaDeNacimiento, fechaActual);
        return periodo.getYears();
    }
}
