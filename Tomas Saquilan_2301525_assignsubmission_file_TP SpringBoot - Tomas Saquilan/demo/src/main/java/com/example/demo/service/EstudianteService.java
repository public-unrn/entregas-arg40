package com.example.demo.service;

import com.example.demo.domain.Estudiante;
import com.example.demo.dto.EstudianteDTO;
import com.example.demo.repository.EstudianteRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class EstudianteService {
    @Autowired
    private EstudianteRepository estudianteRepository;

    @Transactional
    public EstudianteDTO saveEstudiante (EstudianteDTO estudianteDTO){
        Estudiante estudiante = new Estudiante(
                null,
                estudianteDTO.getNombre(),
                estudianteDTO.getApellido(),
                estudianteDTO.getEmail(),
                estudianteDTO.getDni(),
                estudianteDTO.getFechaDeNacimiento()
        );

        estudianteRepository.save(estudiante);

        return estudianteDTO;
    }

    public List<EstudianteDTO> findAllEstudiantes() {
        return estudianteRepository.findAll().stream().
                map(e-> new EstudianteDTO(e.getNombre(), e.getApellido(),
                        e.getEmail(), e.getDni(), e.getFechaDeNacimiento(), e.getEdad()))
                .collect(Collectors.toList());
    }

    @Transactional
    public EstudianteDTO update (Long id, EstudianteDTO estudianteDTO){
        Estudiante estudiante = new Estudiante(id, estudianteDTO.getNombre(), estudianteDTO.getApellido(),
                estudianteDTO.getEmail(), estudianteDTO.getDni(), estudianteDTO.getFechaDeNacimiento());

        estudianteRepository.save(estudiante);

        return estudianteDTO;
    }

    public EstudianteDTO find (Long id){
        Optional<Estudiante> estudianteOptional = estudianteRepository.findById(id);
        if (estudianteOptional.isEmpty()){
            throw new RuntimeException("ID invalido");
        }

        Estudiante estudiante = estudianteOptional.get();

        return new EstudianteDTO(estudiante.getNombre(), estudiante.getApellido(),
                estudiante.getEmail(), estudiante.getDni(), estudiante.getFechaDeNacimiento(), estudiante.getEdad());
    }

    @Transactional
    public void delete(Long id) {
        Optional<Estudiante> estudianteOptional = estudianteRepository.findById(id);
        if(!estudianteOptional.isEmpty()) {
            throw new RuntimeException("No se puede eliminar el estudiante (asociado a una inscripcion)");
        }
        estudianteRepository.deleteById(id);
    }
}
