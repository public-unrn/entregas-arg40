package com.example.demo;

import com.example.demo.domain.*;
import com.example.demo.repository.*;
import com.example.demo.service.InscripcionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Autowired
	CursoRepository cursoRepository;

	@Autowired
	EstudianteRepository estudianteRepository;

	@Autowired
	InscripcionRepository inscripcionRepository;

	@Autowired
	InscripcionService inscripcionService;

	private void saveCursos() {
		cursoRepository.saveAll(Arrays.asList(
				new Curso(null, "Springboot", "Curso BE sobre Java/Springboot", LocalDate.of(2023, Month.JUNE, 20), LocalDate.of(2023,Month.JULY, 16)),
				new Curso(null, "React.js", "Curso FE sobre React", LocalDate.of(2019, Month.JUNE, 20), LocalDate.of(2023,Month.JULY, 16)),
				new Curso(null, "Solidity", "Curso sobre Smart Contracts/Blockchain", LocalDate.of(2023, Month.JUNE, 20), LocalDate.of(2023,Month.JULY, 16))
		));
	}

	private void saveEstudiantes() {
		estudianteRepository.saveAll(Arrays.asList(
				new Estudiante(null, "Tomas", "Saquilan", "tomassaquilan@hotmail.com", 42283011, LocalDate.of(1999,Month.DECEMBER, 6 )),
				new Estudiante(null, "Oscar", "Romero", "oromero@hotmail.com", 24313222, LocalDate.of(1993,Month.DECEMBER, 6 )),
				new Estudiante(null, "Benito", "Romero", "bromero@hotmail.com", 18563200, LocalDate.of(1994,Month.DECEMBER, 6 )),
				new Estudiante(null, "Lionel", "Messi", "lapulga@hotmail.com", 34512203, LocalDate.of(1997,Month.DECEMBER, 6 ))
		));
	}

	private void saveInscripciones() {
		List<Estudiante> estudiantes = estudianteRepository.findAll();
		List<Curso> cursos = cursoRepository.findAll();

		inscripcionRepository.saveAll(Arrays.asList(
				new Inscripcion(null, LocalDate.of(2023, Month.JUNE, 20), cursos.get(0), estudiantes.get(0), EstadoInscripcion.ACEPTADA),
				new Inscripcion(null, LocalDate.of(2023, Month.MARCH, 10), cursos.get(1), estudiantes.get(1), EstadoInscripcion.PENDIENTE),
				new Inscripcion(null, LocalDate.of(2023, Month.MAY, 2), cursos.get(1), estudiantes.get(2), EstadoInscripcion.RECHAZADA),
				new Inscripcion(null, LocalDate.of(2023, Month.FEBRUARY, 16), cursos.get(2), estudiantes.get(3), EstadoInscripcion.RECHAZADA)
		));
	}

	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx){
		return args -> {
			saveCursos();
			saveEstudiantes();
			saveInscripciones();

			inscripcionService.inscripcion(1L, 1L, LocalDate.of(2023, Month.FEBRUARY, 16), EstadoInscripcion.ACEPTADA);

			Page<Estudiante> estudiantePage = estudianteRepository.findAll(PageRequest.of(0, 4, Sort.by(Sort.Direction.ASC, "dni")));
			List<Estudiante> estudiantes = estudiantePage.getContent();

			for (Estudiante estudiante : estudiantes) {
				System.out.println("Nombre: " + estudiante.getNombre());
				System.out.println("Apellido: " + estudiante.getApellido());
				System.out.println("DNI: " + estudiante.getDni());
				System.out.println("Email: " + estudiante.getEmail());
				System.out.println("Edad: " + estudiante.getEdad());
				System.out.println("Fecha de nacimiento: " + estudiante.getFechaDeNacimiento());
				System.out.println("-------------------");
			}
		};
	}
}
