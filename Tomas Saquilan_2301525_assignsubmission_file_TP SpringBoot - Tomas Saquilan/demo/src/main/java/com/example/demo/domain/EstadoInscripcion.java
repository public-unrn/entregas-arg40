package com.example.demo.domain;

public enum EstadoInscripcion {
    ACEPTADA,
    RECHAZADA,
    PENDIENTE
}
