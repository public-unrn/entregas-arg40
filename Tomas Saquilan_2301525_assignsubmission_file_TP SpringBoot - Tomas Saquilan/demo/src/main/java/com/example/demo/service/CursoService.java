package com.example.demo.service;

import com.example.demo.domain.Curso;
import com.example.demo.dto.CursoDTO;
import com.example.demo.repository.CursoRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CursoService {

    @Autowired
    private CursoRepository cursoRepository;

    @Transactional
    public CursoDTO saveCurso(CursoDTO cursoDTO){
        Curso curso = new Curso(
                null,
                cursoDTO.getNombre(),
                cursoDTO.getDescripcion(),
                cursoDTO.getFechaDeInicio(),
                cursoDTO.getFechaDeFin()
        );

        cursoRepository.save(curso);

        return cursoDTO;
    }

    public List<CursoDTO> findAllCursos() {
        return cursoRepository.findAll().stream().
                map(c-> new CursoDTO(c.getNombre(), c.getDescripcion(),
                        c.getFechaDeInicio(), c.getFechaDeFin()))
                .collect(Collectors.toList());
    }
}
