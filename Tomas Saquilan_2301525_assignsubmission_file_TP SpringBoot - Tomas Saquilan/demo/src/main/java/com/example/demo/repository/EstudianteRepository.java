package com.example.demo.repository;

import com.example.demo.domain.Estudiante;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EstudianteRepository extends JpaRepository <Estudiante, Long> {
    @Query("select e from Estudiante e")
    List<Estudiante> listarEstudiantes();

    @Query("select e from Estudiante e where e.dni>20000000 and e.apellido = 'Romero'")
    List<Estudiante> listarRomero();

    //List<Estudiante> findAllBy();
    List<Estudiante> findByDniGreaterThanAndApellidoIgnoreCase(long dni, String apellido);
}
