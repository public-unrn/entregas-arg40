package com.example.demo.service;

import com.example.demo.domain.*;
import com.example.demo.dto.InscripcionDTO;
import com.example.demo.repository.*;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Validated
public class InscripcionService {

    @Autowired
    private CursoRepository cursoRepository;

    @Autowired
    private EstudianteRepository estudianteRepository;

    @Autowired
    private InscripcionRepository inscripcionRepository;

    @Transactional
    public InscripcionDTO saveInscripcion(InscripcionDTO inscripcionDTO){
        Inscripcion inscripcion = new Inscripcion(
                null,
                inscripcionDTO.getFechaDeInscripcion(),
                inscripcionDTO.getCurso(),
                inscripcionDTO.getEstudiante(),
                inscripcionDTO.getEstadoInscripcion()
        );

        inscripcionRepository.save(inscripcion);

        return inscripcionDTO;
    }

    public List<InscripcionDTO> findAllInscripciones() {
        return inscripcionRepository.findAll().stream().
                map(i-> new InscripcionDTO(i.getFechaDeInscripcion(), i.getCurso(), i.getEstudiante(), i.getEstadoInscripcion()))
                .collect(Collectors.toList());
    }

    @Transactional
    public void inscripcion (@NotNull @Positive Long cursoId, @NotNull @Positive Long estudianteId, LocalDate inscripcionFechaInscripcion, EstadoInscripcion estadoInscripcion) {

        if(inscripcionFechaInscripcion.isAfter(LocalDate.now())){
            throw new RuntimeException("Las fechas ingresadas no son validas");
        }

        Curso curso = cursoRepository.findById(cursoId).orElseThrow(()-> new RuntimeException("El id no pertenece a ningun curso"));

        Estudiante estudiante = estudianteRepository.findById(estudianteId).orElseThrow(()-> new RuntimeException("El id no pertenece a ningun estudiante"));

        if(!estudiante.esMayorEdad()){
            throw new RuntimeException("El customer es menor de edad");
        }

        if(estadoInscripcion != EstadoInscripcion.ACEPTADA && estadoInscripcion != EstadoInscripcion.PENDIENTE && estadoInscripcion != EstadoInscripcion.RECHAZADA){
            throw new RuntimeException("El estado de la inscripcion no es valida");
        }

        Inscripcion inscripcion = new Inscripcion(
                null, inscripcionFechaInscripcion, curso, estudiante, EstadoInscripcion.ACEPTADA
        );

        inscripcionRepository.save(inscripcion);
    }
}
