package com.example.demo.controller;

import com.example.demo.dto.CursoDTO;
import com.example.demo.service.CursoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/curso")
public class CursoController {

    @Autowired
    private CursoService cursoService;

    @PostMapping
    public CursoDTO save(@RequestBody CursoDTO cursoDTO){
        return cursoService.saveCurso(cursoDTO);
    }

    @GetMapping
    public List<CursoDTO> all() {
        return cursoService.findAllCursos();
    }
}
