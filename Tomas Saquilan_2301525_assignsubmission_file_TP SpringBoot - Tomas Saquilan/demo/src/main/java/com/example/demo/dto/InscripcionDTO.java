package com.example.demo.dto;

import com.example.demo.domain.Curso;
import com.example.demo.domain.EstadoInscripcion;
import com.example.demo.domain.Estudiante;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InscripcionDTO {
    private LocalDate fechaDeInscripcion;
    private Curso curso;
    private Estudiante estudiante;
    private EstadoInscripcion estadoInscripcion;
}
