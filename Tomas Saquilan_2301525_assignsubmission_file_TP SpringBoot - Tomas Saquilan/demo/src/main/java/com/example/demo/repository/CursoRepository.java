package com.example.demo.repository;

import com.example.demo.domain.Curso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;

public interface CursoRepository extends JpaRepository<Curso,Long> {
    @Query("select c from Curso c")
    List<Curso> listarTodosLosCursos();

    List <Curso> findAll();

    @Query("select c from Curso c where c.fechaDeInicio > TO_DATE('2020-02-01', 'YYYY-MM-DD')")
    List<Curso> listarCursosNuevos();

    List<Curso> findByFechaDeInicioAfter(LocalDate fechaInicio);
}
