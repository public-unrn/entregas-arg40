package com.example.demo.repository;

import com.example.demo.domain.EstadoInscripcion;
import com.example.demo.domain.Inscripcion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InscripcionRepository extends JpaRepository <Inscripcion, Long> {
    @Query("select i from Inscripcion i where i.estadoInscripcion = 'RECHAZADA' or i.estadoInscripcion = 'PENDIENTE'")
    List<Inscripcion> listarInscripcionesRechazadasOPendientes();

    @Query("select i from Inscripcion i where i.estudiante.dni = :dni")
    List<Inscripcion> listarInscripcionesPorDni(@Param("dni") int dni);

    @Query(value = "SELECT * FROM inscripcion i where i.estado_inscripcion = :estado", nativeQuery = true)
    List<Inscripcion> listarInscripcionesPorEstadoDeInscripcion(@Param("estado") String estado);

    List<Inscripcion> findByEstadoInscripcionIn(List<EstadoInscripcion> estados);
    List<Inscripcion> findByEstudianteDni(int dni);
}
