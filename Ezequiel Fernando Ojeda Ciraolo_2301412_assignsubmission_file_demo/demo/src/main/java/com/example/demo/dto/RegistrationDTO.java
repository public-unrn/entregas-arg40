package com.example.demo.dto;

import java.time.LocalDate;

public class RegistrationDTO {

    private Long studentId;
    private Long courseId;
    private LocalDate registrationDate;


    public RegistrationDTO(Long studentId, Long courseId, LocalDate registrationDate) {
        this.studentId = studentId;
        this.courseId = courseId;
        this.registrationDate = registrationDate;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }
}
