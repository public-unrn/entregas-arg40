package com.example.demo.repository;

import com.example.demo.domain.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student, Long> {

    @Query("SELECT c FROM Student c")
    List<Student> findAllStudents();

    @Query("SELECT c FROM Student c WHERE c.personalId > 20000000 AND c.lastName = 'Romero'")
    List<Student> findByIdPlus20MillionAndLastNameRomero();

    Page<Student> findAllByOrderByPersonalIdAsc(Pageable pageable);

}
