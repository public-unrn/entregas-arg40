package com.example.demo.controller;

import com.example.demo.dto.CourseDTO;
import com.example.demo.domain.Course;
import com.example.demo.repository.CourseRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

    @RestController
    @RequestMapping("/course")
    public class CourseController {

        private final CourseRepository courseRepository;

        public CourseController(CourseRepository courseRepository) {
            this.courseRepository = courseRepository;
        }

        @GetMapping
        public List<Course> getAllCourses() {
            return courseRepository.findAll();
        }
    }
