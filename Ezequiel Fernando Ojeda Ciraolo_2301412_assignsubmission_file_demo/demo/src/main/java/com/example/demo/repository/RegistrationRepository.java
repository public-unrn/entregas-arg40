package com.example.demo.repository;

import com.example.demo.domain.Registration;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RegistrationRepository  extends JpaRepository<Registration, Long> {
    @Query("SELECT c FROM Registration c WHERE c.registrationStatus = 'REJECTED' OR c.registrationStatus = 'PENDING'")
    List<Registration> findByRegistrationStatus();
    @Query("SELECT c FROM Registration c WHERE c.registrationStatus = 'ACCEPTED'")
    List<Registration> findByRegistrationStatus(Registration.RegistrationStatus registrationStatus);

    @Query(value = "SELECT * FROM registration c WHERE c.registration_status = :status", nativeQuery = true)
    List<Registration> findByRegistrationStatusNative(@Param("status") String status);

}
