package com.example.demo.dto;

import jakarta.persistence.Column;
import jakarta.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class StudentDTO {

    private String firstName;
    private String lastName;
    private String email;
    private int personalId;
    private LocalDate birthDate;
    private int age;

}
