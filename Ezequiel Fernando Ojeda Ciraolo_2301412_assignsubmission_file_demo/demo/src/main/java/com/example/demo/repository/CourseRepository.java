package com.example.demo.repository;

import com.example.demo.domain.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface CourseRepository  extends JpaRepository<Course, Long> {

    @Query("SELECT c FROM Course c")
    List<Course> findAllCourses();

    @Query("SELECT c FROM Course c WHERE c.startDate > TO_DATE('01/02/2020','dd/mm/YYYY')")
    List<Course> findByCourseDate();
}
