package com.example.demo;

import com.example.demo.domain.Course;
import com.example.demo.domain.Registration;
import com.example.demo.domain.Student;
import com.example.demo.repository.CourseRepository;
import com.example.demo.repository.RegistrationRepository;
import com.example.demo.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.time.LocalDate;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args); }

    @Autowired
    StudentRepository studentRepository;
    @Autowired
    RegistrationRepository registrationRepository;
    @Autowired
    CourseRepository courseRepository;

    @Bean
    public CommandLineRunner commandLineRunner (ApplicationContext ctx) {
        return args -> {

            // CURSOS
            Course course1 = new Course();
            course1.setCourseName("ApplicationDevelopment");
            course1.setDescription("SoftwareDevelopment");
            course1.setStartDate(LocalDate.of(2023, 6, 26));
            course1.setEndingDate(LocalDate.now());

            Course course2 = new Course();
            course2.setCourseName("BackendFundamentals");
            course2.setDescription("SoftwareDevelopment");
            course2.setStartDate(LocalDate.now());
            course2.setEndingDate(LocalDate.of(2023, 7, 26));

            // Verificación
            courseRepository.findAll();

            courseRepository.save(course1);
            courseRepository.save(course2);

            courseRepository.findAll();

            /* ESTUDIANTES */
            // Variables con identificador asignado
            Student studentLEG1100 = new Student();
            studentLEG1100.setFirstName("John");
            studentLEG1100.setLastName("Doe");
            studentLEG1100.setEmail("JohnDoe@email.com");
            studentLEG1100.setPersonalId(12345678);
            studentLEG1100.setBirthDate(LocalDate.of(2000, 12, 10));

            // Calcular la edad del estudiante
            studentLEG1100.calculateAge();

            Student studentLEG2200 = new Student();
            studentLEG2200.setFirstName("Mario");
            studentLEG2200.setLastName("Romero");
            studentLEG2200.setEmail("romeromario@email.com");
            studentLEG2200.setPersonalId(32165478);
            studentLEG2200.setBirthDate(LocalDate.of(2000, 12, 31));

            // Calcular la edad del estudiante;
            studentLEG2200.calculateAge();

            // página 1, tamaño 5
            int page1 = 1;
            int size5 = 5;
            Page<Student> studentsPage1 = studentRepository.findAllByOrderByPersonalIdAsc(PageRequest.of(page1, size5));
            studentsPage1.forEach(student -> System.out.println(student));

            // página 0, tamaño 2
            int page0 = 0;
            int size2 = 2;
            Page<Student> studentsPage0 = studentRepository.findAllByOrderByPersonalIdAsc(PageRequest.of(page0, size2));
            studentsPage0.forEach(student -> System.out.println(student));


            // Verificación
            studentRepository.findAll();

            studentRepository.save(studentLEG1100);
            studentRepository.save(studentLEG2200);

            studentRepository.findAll();

            // INSCRIPCIONES
            Registration registration1 = new Registration();
            registration1.setRegistrationDate(LocalDate.of(2023, 12, 10));
            registration1.setRegistrationStatus(Registration.RegistrationStatus.ACCEPTED);
            registration1.setStudent(studentLEG1100);

            Registration registration2 = new Registration();
            registration2.setRegistrationDate(LocalDate.of(2023, 12, 10));
            registration2.setRegistrationStatus(Registration.RegistrationStatus.ACCEPTED);
            registration2.setStudent(studentLEG2200);

            // Verificación
            registrationRepository.findAll();

            registrationRepository.save(registration1);
            registrationRepository.save(registration2);

            registrationRepository.findAll();
        };
    }

}
