package com.example.demo.controller;

import com.example.demo.domain.Registration;
import com.example.demo.dto.RegistrationDTO;
import com.example.demo.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/registration")
public class RegistrationController {

    private final RegistrationService registrationService;

    @Autowired
    public RegistrationController(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    @PostMapping
    public ResponseEntity<String> createRegistration(@RequestBody RegistrationDTO registrationDTO) {
        try {
            registrationService.createRegistration(
                    registrationDTO.getStudentId(),
                    registrationDTO.getCourseId(),
                    registrationDTO.getRegistrationDate()
            );
            return new ResponseEntity<>("Inscripción creada", HttpStatus.CREATED);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>("Error al crear la inscripción", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
