package com.example.demo.service;

import com.example.demo.domain.Course;
import com.example.demo.domain.Registration;
import com.example.demo.domain.Student;
import com.example.demo.repository.CourseRepository;
import com.example.demo.repository.RegistrationRepository;
import com.example.demo.repository.StudentRepository;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.time.LocalDate;
import java.util.Objects;
import java.util.Optional;

@Service
@Validated
public class RegistrationService {
    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private RegistrationRepository registrationRepository;

    public void Registration(@NotNull @Positive Long studentId, @NotNull @Positive Long courseId, LocalDate registrationDate){

        Student student = studentRepository.findById(studentId).get();

        if (!student.isAdult()) {
            throw new IllegalArgumentException("El estudiante debe ser mayor de edad.");
        }

        Optional<Course> courseOptional = courseRepository.findById(courseId);

        Registration registration = new Registration();

        registrationRepository.save(registration);
    }

    public void createRegistration(Long studentId, Long courseId, LocalDate registrationDate) {
    }
}
