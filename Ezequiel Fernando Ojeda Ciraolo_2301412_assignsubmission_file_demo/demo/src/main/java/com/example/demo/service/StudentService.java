package com.example.demo.service;

import com.example.demo.domain.Student;
import com.example.demo.dto.StudentDTO;
import com.example.demo.repository.StudentRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class StudentService {
    private StudentRepository studentRepository;

    public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public StudentDTO saveStudent(StudentDTO studentDTO) {
        Student student = new Student(
                studentDTO.getFirstName(),
                studentDTO.getLastName(),
                studentDTO.getEmail(),
                studentDTO.getPersonalId(),
                studentDTO.getAge()
        );

        studentRepository.save(student);
        return studentDTO;
    }

    public List<StudentDTO> findAll() {
        List<Student> students = studentRepository.findAll();
        List<StudentDTO> studentDTOs = new ArrayList<>();

        for (Student student : students) {
            StudentDTO dto = new StudentDTO(
                    student.getFirstName(),
                    student.getLastName(),
                    student.getEmail(),
                    student.getPersonalId(),
                    student.getBirthDate(),
                    student.getAge()
            );
            studentDTOs.add(dto);
        }

        return studentDTOs;
    }

    public StudentDTO findStudentById(Long id) {
        Optional<Student> optionalStudent = studentRepository.findById(id);
        if (optionalStudent.isPresent()) {
            Student student = optionalStudent.get();
            StudentDTO dto = new StudentDTO(
                    student.getFirstName(),
                    student.getLastName(),
                    student.getEmail(),
                    student.getPersonalId(),
                    student.getBirthDate(),
                    student.getAge()
            );
            return dto;
        } else {
            return null;
        }
    }


    public boolean deleteStudent(Long id) {
        try {
            studentRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
