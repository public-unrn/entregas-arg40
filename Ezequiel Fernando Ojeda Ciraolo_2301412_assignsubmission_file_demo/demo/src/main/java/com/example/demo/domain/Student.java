package com.example.demo.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.Period;

@Entity
@Table(name = "student")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "personal_email")
    private String email;

    @Column(name = "personal_id")
    private int personalId;

    @Column(name = "birth_date")
    private LocalDate birthDate;

    @Transient
    private int age;



    public boolean isAdult() {
        if (birthDate != null) {
            int age = Period.between(birthDate, LocalDate.now()).getYears();
            return age >= 18;
        }
        return false;
    }
    public Student(String firstName, String lastName, String email, int personalId, int age) {
    }
    public void calculateAge() {
        if (birthDate != null) {
            this.age = Period.between(birthDate, LocalDate.now()).getYears();
}
    }
}