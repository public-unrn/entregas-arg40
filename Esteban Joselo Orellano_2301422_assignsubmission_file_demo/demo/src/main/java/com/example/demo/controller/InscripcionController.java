package com.example.demo.controller;

import com.example.demo.Service.InscripcionService;
import com.example.demo.dto.InscripcionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/inscripcion")
public class InscripcionController {
    @Autowired
    private InscripcionService inscripcionService;
    @PostMapping
    public InscripcionDTO save(@RequestBody InscripcionDTO inscripcionDTO){
        return inscripcionService.saveInscripcion(inscripcionDTO);
    }
    @GetMapping
    public List<InscripcionDTO> all(){
        return inscripcionService.findAll();
    }
}
