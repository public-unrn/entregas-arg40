package com.unrn.inscripcion.service.implementations;

import com.unrn.inscripcion.domain.Curso;
import com.unrn.inscripcion.utils.enums.EstadoInscripcion;
import com.unrn.inscripcion.domain.Estudiante;
import com.unrn.inscripcion.domain.Inscripcion;
import com.unrn.inscripcion.dto.DTOInscripcion;
import com.unrn.inscripcion.repository.CursoRepository;
import com.unrn.inscripcion.repository.EstudianteRepository;
import com.unrn.inscripcion.repository.InscripcionRepository;
import com.unrn.inscripcion.service.interfaces.InscripcionInterface;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class InscripcionImpl implements InscripcionInterface {

    @Autowired
    private CursoRepository cursoRepository;
    @Autowired
    private EstudianteRepository estudianteRepository;
    @Autowired
    private InscripcionRepository inscripcionRepository;

    @Override
    @Transactional
    public Long create(LocalDate inscripcionFecha, @NotNull EstadoInscripcion estado, @NotNull @Positive Long idEstudiante, @NotNull @Positive Long idCurso) throws Exception {
        Long inscripcionId;
        Curso curso = cursoRepository.findById(idCurso).orElseThrow(() -> new RuntimeException("Error al obtener Curso"));
        Estudiante estudiante = estudianteRepository.findById(idEstudiante).orElseThrow(() -> new RuntimeException("Error al obtener Estudiante"));
        try {
            Inscripcion inscripcion = new Inscripcion(
                    null,
                    inscripcionFecha,
                    estado,
                    estudiante,
                    curso
            );

            inscripcionId = inscripcionRepository.save(inscripcion).getId();

        }catch (Exception e){
            throw new Exception("Error al crear inscripcion" + e.getMessage());
        }
        return inscripcionId;
    }

    @Override
    public List<DTOInscripcion> listarInscripcionEstado(EstadoInscripcion estado) throws Exception {
        List<DTOInscripcion> inscripcionList = inscripcionRepository.findByEstadoIs(estado)
                .stream().map(inscripcion -> new DTOInscripcion(inscripcion.getInscripcionFecha(), inscripcion.getEstado(), inscripcion.getEstudiante().getId(), inscripcion.getCurso().getId()))
                .collect(Collectors.toList());

        if(inscripcionList.isEmpty()){
            throw new Exception("No se obtuvieron Inscripciones");
        }
        return inscripcionList;
    }

    @Override
    public List<DTOInscripcion> listarInscripcionEstadoNativo(EstadoInscripcion estado) throws Exception {
        List<DTOInscripcion> inscripcionList = inscripcionRepository.findByEstado(estado)
                .stream().map(inscripcion -> new DTOInscripcion(inscripcion.getInscripcionFecha(), inscripcion.getEstado(), inscripcion.getEstudiante().getId(), inscripcion.getCurso().getId()))
                .collect(Collectors.toList());

        if(inscripcionList.isEmpty()){
            throw new Exception("No se obtuvieron Inscripciones");
        }

        return inscripcionList;
    }

    @Override
    public List<DTOInscripcion> listarInscripcionRechazadoOrPendiente() throws Exception {
        List<DTOInscripcion> inscripcionList = inscripcionRepository.findByEstadoIsOrEstadoIs(EstadoInscripcion.RECHAZADO,EstadoInscripcion.PENDIENTE)
                .stream().map(inscripcion -> new DTOInscripcion(inscripcion.getInscripcionFecha(), inscripcion.getEstado(), inscripcion.getEstudiante().getId(), inscripcion.getCurso().getId()))
                .collect(Collectors.toList());

        if(inscripcionList.isEmpty()){
            throw new Exception("No se obtuvieron Inscripciones");
        }
        return inscripcionList;
    }

}
