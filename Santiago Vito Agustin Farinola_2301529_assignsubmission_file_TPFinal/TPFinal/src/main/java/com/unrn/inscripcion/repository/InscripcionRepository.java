package com.unrn.inscripcion.repository;

import com.unrn.inscripcion.utils.enums.EstadoInscripcion;
import com.unrn.inscripcion.domain.Inscripcion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InscripcionRepository extends JpaRepository<Inscripcion,Long> {

    @Query("SELECT i FROM Inscripcion i WHERE i.estado = :estado")
    List<Inscripcion> findByEstado(@Param("estado") EstadoInscripcion estado);

    List<Inscripcion> findByEstadoIsOrEstadoIs(EstadoInscripcion rechazado, EstadoInscripcion pendiente);

    List<Inscripcion> findByEstadoIs(EstadoInscripcion estado);

}
