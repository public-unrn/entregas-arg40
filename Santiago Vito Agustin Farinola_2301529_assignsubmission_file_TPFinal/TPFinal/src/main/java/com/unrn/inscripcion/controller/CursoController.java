package com.unrn.inscripcion.controller;

import com.unrn.inscripcion.dto.DTOCurso;
import com.unrn.inscripcion.service.interfaces.CursoInterface;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/curso")
@AllArgsConstructor
public class CursoController {

    @Autowired
    private CursoInterface cursoInterface;

    @PostMapping("/create")
    public DTOCurso saveCurso(@RequestBody DTOCurso dto) throws Exception {
        return cursoInterface.create(dto);
    }

    @GetMapping("/listar")
    public List<DTOCurso> listarCursos() throws Exception {
        return cursoInterface.findAll();
    }

    @GetMapping("/listar/{fecha}")
    public List<DTOCurso> listarCursoDespuesDeFecha(@PathVariable LocalDate fecha) throws Exception {
        return cursoInterface.getListByFechaInicio(fecha);
    }

}
