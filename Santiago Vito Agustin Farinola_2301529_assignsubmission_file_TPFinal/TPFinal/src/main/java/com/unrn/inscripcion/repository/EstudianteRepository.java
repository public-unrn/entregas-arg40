package com.unrn.inscripcion.repository;


import com.unrn.inscripcion.domain.Estudiante;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EstudianteRepository extends JpaRepository<Estudiante,Long> {
    List<Estudiante> findByDniGreaterThanAndApellidoIs(Long dni, String apellido);
}
