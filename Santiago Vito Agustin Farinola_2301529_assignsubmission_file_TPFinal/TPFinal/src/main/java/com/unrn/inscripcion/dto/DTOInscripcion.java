package com.unrn.inscripcion.dto;

import com.unrn.inscripcion.utils.enums.EstadoInscripcion;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class DTOInscripcion {

    private LocalDate inscripcionFecha;
    private EstadoInscripcion estado;
    private Long idEstudiante;
    private Long idCurso;
}
