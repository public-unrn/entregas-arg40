package com.unrn.inscripcion.controller;

import com.unrn.inscripcion.utils.enums.EstadoInscripcion;
import com.unrn.inscripcion.dto.DTOInscripcion;
import com.unrn.inscripcion.service.interfaces.InscripcionInterface;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/inscripcion")
@AllArgsConstructor
public class InscripcionController {

    private InscripcionInterface inscripcionInterface;

    @PostMapping("/create")
    public Long saveInscripcion(DTOInscripcion dto) throws Exception {
        return inscripcionInterface.create(dto.getInscripcionFecha(),dto.getEstado(),dto.getIdEstudiante(),dto.getIdCurso());
    }

    @GetMapping("/listar")
    public List<DTOInscripcion> listarInscripcionEstado(@RequestParam("estado") EstadoInscripcion estado) throws Exception {
        return inscripcionInterface.listarInscripcionEstado(estado);
    }
    @GetMapping("/listarRechOPend")
    public List<DTOInscripcion> listarInscripcionRechazadoOrPendiente() throws Exception {
        return inscripcionInterface.listarInscripcionRechazadoOrPendiente();
    }

    @GetMapping("/listarNativo")
    public List<DTOInscripcion> listarInscripcionEstadoNativo(@RequestParam("estado") EstadoInscripcion estado) throws Exception {
        return inscripcionInterface.listarInscripcionEstadoNativo(estado);
    }

}
