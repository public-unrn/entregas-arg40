package com.unrn.inscripcion.service.interfaces;

import com.unrn.inscripcion.utils.enums.EstadoInscripcion;
import com.unrn.inscripcion.dto.DTOInscripcion;

import java.time.LocalDate;
import java.util.List;

public interface InscripcionInterface {

    Long create(LocalDate inscripcionFecha,
                         EstadoInscripcion estado,
                         Long idEstudiante,
                         Long idCurso) throws Exception;


    List<DTOInscripcion> listarInscripcionEstado(EstadoInscripcion estado) throws Exception;

    List<DTOInscripcion> listarInscripcionEstadoNativo(EstadoInscripcion estado) throws Exception;

    List<DTOInscripcion> listarInscripcionRechazadoOrPendiente() throws Exception;

}
