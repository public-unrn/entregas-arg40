package com.unrn.inscripcion.controller;


import com.unrn.inscripcion.dto.DTOEstudiante;
import com.unrn.inscripcion.service.interfaces.EstudianteInterface;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/estudiante")
@AllArgsConstructor
public class EstudianteController {

    @Autowired
    private EstudianteInterface estudianteInterface;

    @PostMapping("/create")
    public Long saveEstudiante(@RequestBody DTOEstudiante dto) throws Exception {
        return estudianteInterface.create(dto);
    }

    @PutMapping("/{id}")
    public Long updateEstudiante(@PathVariable(name = "id") final Long id,
                                 @RequestBody @Valid final DTOEstudiante dto) throws Exception {
        return estudianteInterface.update(id,dto);
    }

    @DeleteMapping("/{id}")
    @ApiResponse(responseCode = "204")
    public void deleteEstudiante(@PathVariable(name = "id") final Long id) throws Exception {
        estudianteInterface.delete(id);
    }


    @GetMapping("/{id}")
    public DTOEstudiante getById(@RequestParam("idEstudiante") Long idEstudiante) throws Exception {
        return estudianteInterface.findById(idEstudiante);
    }


    @GetMapping("/listar")
    public List<DTOEstudiante> listarEstudiantes() throws Exception {
        return estudianteInterface.findAll();
    }


    @GetMapping("/listar/{dni}/{apellido}")
    public List<DTOEstudiante> listarEstudianteDniM20AndApellido(@PathVariable Long dni,@PathVariable String apellido) throws Exception {
        return estudianteInterface.listarEstudianteDniM20AndApellido(dni, apellido);
    }

    @GetMapping("/listarPage/{pagina}/{cantidad}")
    public List<DTOEstudiante> listarEstudiantePag(@PathVariable int pagina, @PathVariable int cantidad) throws Exception {
        return estudianteInterface.listarEstudiantePag(pagina,cantidad);
    }

}
