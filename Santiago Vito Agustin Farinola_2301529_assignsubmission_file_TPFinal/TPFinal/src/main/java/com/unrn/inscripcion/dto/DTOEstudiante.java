package com.unrn.inscripcion.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DTOEstudiante {

    private String nombre;
    private String apellido;
    private String email;
    private Long dni;
    private LocalDate fechaNacimiento;
    private Long edad;
}
