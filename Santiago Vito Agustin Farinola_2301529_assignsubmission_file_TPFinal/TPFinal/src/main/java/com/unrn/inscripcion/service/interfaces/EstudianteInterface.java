package com.unrn.inscripcion.service.interfaces;

import com.unrn.inscripcion.domain.Estudiante;
import com.unrn.inscripcion.dto.DTOEstudiante;
import org.springframework.data.domain.Page;

import java.util.List;

public interface EstudianteInterface {

    DTOEstudiante findById(Long idEstudiante) throws Exception;

    List<DTOEstudiante> findAll() throws Exception;

    List<DTOEstudiante> listarEstudianteDniM20AndApellido(Long dni, String apellido) throws Exception;

    List<DTOEstudiante> listarEstudiantePag(int pagina, int cantidad) throws Exception;

    Long create(DTOEstudiante dto) throws Exception;

    void delete(Long id) throws Exception;

    Long update(Long id, DTOEstudiante estudiante) throws Exception;

}
