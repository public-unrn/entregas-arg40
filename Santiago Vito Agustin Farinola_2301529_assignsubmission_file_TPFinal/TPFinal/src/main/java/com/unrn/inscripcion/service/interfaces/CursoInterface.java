package com.unrn.inscripcion.service.interfaces;

import com.unrn.inscripcion.dto.DTOCurso;

import java.time.LocalDate;
import java.util.List;

public interface CursoInterface {

    List<DTOCurso> getListByFechaInicio(LocalDate fechaInicio) throws Exception;

    DTOCurso create(DTOCurso dto) throws Exception;

    List<DTOCurso> findAll() throws Exception;



}
