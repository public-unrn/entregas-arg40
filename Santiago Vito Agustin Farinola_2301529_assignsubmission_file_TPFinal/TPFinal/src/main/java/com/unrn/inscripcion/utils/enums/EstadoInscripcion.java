package com.unrn.inscripcion.utils.enums;

public enum EstadoInscripcion {
    ACEPTADO, PENDIENTE, RECHAZADO
}
