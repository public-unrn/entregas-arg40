package com.unrn.inscripcion.service.implementations;

import com.unrn.inscripcion.domain.Estudiante;
import com.unrn.inscripcion.domain.Inscripcion;
import com.unrn.inscripcion.dto.DTOEstudiante;
import com.unrn.inscripcion.repository.EstudianteRepository;
import com.unrn.inscripcion.repository.InscripcionRepository;
import com.unrn.inscripcion.service.interfaces.EstudianteInterface;
import com.unrn.inscripcion.service.interfaces.InscripcionInterface;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.net.ssl.SSLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EstudianteImpl implements EstudianteInterface {

    @Autowired
    private EstudianteRepository estudianteRepository;


    @Override
    @Transactional
    public Long create(DTOEstudiante dto) throws Exception {
        Long estudianteId;
        try{
            Estudiante estudiante = new Estudiante(
                    null,
                    dto.getNombre(),
                    dto.getApellido(),
                    dto.getEmail(),
                    dto.getDni(),
                    dto.getFechaNacimiento(),
                    dto.getEdad());

            if(!estudiante.mayorEdad()){
                throw new Exception("El estudiante es menor de edad");
            }

            estudianteId = estudianteRepository.save(estudiante).getId();

        }catch (Exception e){
            throw new Exception("Error al crear estudiante" + e.getMessage());
        }
        return estudianteId;
    }

    @Override
    @Transactional
    public void delete(@NotNull @Positive Long id) throws Exception {
        /* TODO llamada a metodo para eliminar inscripciones por estudiante*/
        // inscripcionInterface.deleteAllByEstudianteId(id);
        try {
            estudianteRepository.deleteById(id);
        }catch (Exception e){
            throw new Exception("Error al eliminar estudiante" + e.getMessage());
        }
    }

    @Override
    @Transactional
    public Long update(@NotNull @Positive Long id, DTOEstudiante dto) throws Exception {
        Long estudianteId;

        Estudiante estudiante = estudianteRepository.findById(id).orElseThrow(() -> new RuntimeException("Error al obtener Estudiante"));
        mapToEntity(dto, estudiante);

        try {
            estudianteId = estudianteRepository.save(estudiante).getId();
        }catch (Exception e){
            throw new Exception("Error al actualizar estudiante" + e.getMessage());
        }

        return estudianteId;
    }

    @Override
    public DTOEstudiante findById(@NotNull @Positive Long idEstudiante) throws Exception {
        DTOEstudiante dtoEstudiante = estudianteRepository.findById(idEstudiante)
                .map(estudiante -> mapToDTO(estudiante, new DTOEstudiante())).orElseThrow(ChangeSetPersister.NotFoundException::new);

        return dtoEstudiante;
    }

    @Override
    public List<DTOEstudiante> findAll() throws Exception {

        List<DTOEstudiante> estudianteList = estudianteRepository.findAll()
                .stream().map(estudiante -> new DTOEstudiante(estudiante.getNombre(),estudiante.getApellido(),estudiante.getEmail(),estudiante.getDni(), estudiante.getFechaNacimiento(),null))
                .collect(Collectors.toList());

        if(estudianteList.isEmpty()){
            throw new Exception("No se obtuvieron estudiantes");
        }

        return estudianteList;
    }

    @Override
    public List<DTOEstudiante> listarEstudianteDniM20AndApellido(@NotNull @Positive Long dni, String apellido) throws Exception {
        List<DTOEstudiante> estudianteList = new ArrayList<>();

        estudianteList = estudianteRepository.findByDniGreaterThanAndApellidoIs(dni, apellido)
                .stream().map(estudiante -> new DTOEstudiante(estudiante.getNombre(), estudiante.getApellido(), estudiante.getEmail(), estudiante.getDni(), estudiante.getFechaNacimiento(),null))
                .collect(Collectors.toList());

        if(estudianteList.isEmpty()){
            throw new Exception("No se obtuvieron estudiantes");
        }

        return estudianteList;
    }

    @Override
    public List<DTOEstudiante> listarEstudiantePag(@NotNull @Positive int pagina,@NotNull @Positive int cantidad) throws Exception {

        List<DTOEstudiante> estudianteList = estudianteRepository.findAll(PageRequest.of(pagina,cantidad, Sort.by("dni").ascending()))
                .stream().map(estudiante -> new DTOEstudiante(estudiante.getNombre(), estudiante.getApellido(), estudiante.getEmail(), estudiante.getDni(), estudiante.getFechaNacimiento(), null))
                .collect(Collectors.toList());

        if(estudianteList.isEmpty()){
            throw new Exception("No se obtuvieron estudiantes");
        }

        return estudianteList;
    }

    private DTOEstudiante mapToDTO(final Estudiante entity, final DTOEstudiante dto){

        dto.setNombre(entity.getNombre());
        dto.setApellido(entity.getApellido());
        dto.setEmail(entity.getEmail());
        dto.setDni(entity.getDni());
        dto.setFechaNacimiento(entity.getFechaNacimiento());

        return dto;
    }

    private Estudiante mapToEntity(final DTOEstudiante dto, final Estudiante entity){

        entity.setNombre(dto.getNombre());
        entity.setApellido(dto.getApellido());
        entity.setEmail(dto.getEmail());
        entity.setDni(dto.getDni());
        entity.setFechaNacimiento(dto.getFechaNacimiento());

        return entity;
    }


}
