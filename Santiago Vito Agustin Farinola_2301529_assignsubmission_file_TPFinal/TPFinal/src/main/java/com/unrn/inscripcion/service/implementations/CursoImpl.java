package com.unrn.inscripcion.service.implementations;

import com.unrn.inscripcion.domain.Curso;
import com.unrn.inscripcion.dto.DTOCurso;
import com.unrn.inscripcion.repository.CursoRepository;
import com.unrn.inscripcion.service.interfaces.CursoInterface;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.net.ssl.SSLException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CursoImpl implements CursoInterface {

    @Autowired
    private CursoRepository cursoRepository;

    @Override
    public List<DTOCurso> getListByFechaInicio(LocalDate fechaInicio) throws Exception {

        List<DTOCurso>  cursoList = cursoRepository.findByFechaInicioAfter(fechaInicio)
                .stream().map(curso -> new DTOCurso(curso.getNombre(), curso.getDescripcion(), curso.getFechaInicio(), curso.getFechaFin()))
                .collect(Collectors.toList());

        if(cursoList.isEmpty()){
            throw new Exception("No se obtuvieron cursos");
        }

        return cursoList;
    }

    @Override
    @Transactional
    public DTOCurso create(DTOCurso dto) throws Exception {
        try{
            Curso curso = new Curso(
                    null,
                    dto.getNombre(),
                    dto.getDescripcion(),
                    dto.getFechaInicio(),
                    dto.getFechaFin()
            );
            cursoRepository.save(curso);
        }catch (Exception e){
            throw new Exception("Error al crear Curso" + e.getMessage());
        }

        return dto;
    }

    @Override
    public List<DTOCurso> findAll() throws Exception {

        List<DTOCurso> cursoList = cursoRepository.findAll()
                .stream().map(c -> new DTOCurso(c.getNombre(),c.getDescripcion(),c.getFechaInicio(),c.getFechaFin()))
                .collect(Collectors.toList());

        if(cursoList.isEmpty()){
            throw new Exception("No se obtuvieron cursos");
        }

        return cursoList;
    }
}
