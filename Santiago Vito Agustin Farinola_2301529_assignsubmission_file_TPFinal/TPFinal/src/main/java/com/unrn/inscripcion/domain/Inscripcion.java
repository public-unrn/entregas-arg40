package com.unrn.inscripcion.domain;


import com.unrn.inscripcion.utils.enums.EstadoInscripcion;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "INSCRIPCION")
public class Inscripcion implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "INSCRIPCION_ID")
    private Long id;

    @Column(name = "INSCRIPCION_FECHA")
    private LocalDate inscripcionFecha;

    @Column(name = "ESTADO")
    private EstadoInscripcion estado;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "ESTUDIANTE_ID")
    private Estudiante estudiante;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "CURSO_ID")
    private Curso curso;



}