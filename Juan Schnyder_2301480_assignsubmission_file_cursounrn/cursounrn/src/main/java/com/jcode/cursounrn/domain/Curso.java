package com.jcode.cursounrn.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Table( name = "curso")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Curso {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private Long id;
    @Column( name = "nombre")
    private String nombre;
    @Column( name = "descripcion")
    private String descripcion;

    private LocalDate fechaDeInicio;
    private LocalDate fechaDeFin;



    @Override
    public String toString() {
        return "Curso{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", fechaDeInicio=" + fechaDeInicio +
                ", fechaDeFin=" + fechaDeFin +
                '}';
    }
}
