package com.jcode.cursounrn.services;

import com.jcode.cursounrn.domain.Estudiante;
import com.jcode.cursounrn.dto.EstudianteDTO;
import com.jcode.cursounrn.repository.EstudianteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class EstudianteService {

      @Autowired
      private EstudianteRepository estudianteRepository;


      public EstudianteDTO saveEstudiante(EstudianteDTO estudianteDTO){

          Estudiante estudiante = new Estudiante(

                  null,
                  estudianteDTO.getNombre(),
                  estudianteDTO.getApellido(),
                  estudianteDTO.getEmail(),
                  estudianteDTO.getDni(),
                  estudianteDTO.getFechaDeNacimiento()
          );

          estudianteRepository.save(estudiante);
          return estudianteDTO;

      }

      public List<EstudianteDTO> findAll() {

          return estudianteRepository.findAll()
                  .stream().map(e -> new EstudianteDTO(e.getNombre(),e.getApellido(),e.getEmail(),e.getDni(),e.getFechaDeNacimiento()))
                  .collect(Collectors.toList());

      }

      public EstudianteDTO update(Long id, EstudianteDTO estudianteDTO) {

          Estudiante estudiante = new Estudiante(id, estudianteDTO.getNombre(), estudianteDTO.getApellido(), estudianteDTO.getEmail(), estudianteDTO.getDni(), estudianteDTO.getFechaDeNacimiento());
          estudianteRepository.save(estudiante);
          return estudianteDTO;

      }

      public EstudianteDTO find(Long id) {

          Optional<Estudiante> estudianteOptional = estudianteRepository.findById(id);

          if(estudianteOptional.isEmpty()) {
              throw new RuntimeException("id que no existente");
          }

          Estudiante estudiante = estudianteOptional.get();

          return new EstudianteDTO(estudiante.getNombre(), estudiante.getApellido(), estudiante.getEmail(), estudiante.getDni(), estudiante.getFechaDeNacimiento());




      }

      public void delete(Long id) {
          estudianteRepository.deleteById(id);
      }

}
