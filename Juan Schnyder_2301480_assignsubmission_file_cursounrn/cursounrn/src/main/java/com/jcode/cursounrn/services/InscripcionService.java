package com.jcode.cursounrn.services;

import com.jcode.cursounrn.domain.Curso;
import com.jcode.cursounrn.domain.Estudiante;
import com.jcode.cursounrn.domain.Inscripcion;
import com.jcode.cursounrn.domain.Status;
import com.jcode.cursounrn.repository.CursoRepository;
import com.jcode.cursounrn.repository.EstudianteRepository;
import com.jcode.cursounrn.repository.InscripcionRepository;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.time.LocalDate;

@Service
@Validated
public class InscripcionService {

    @Autowired
    private EstudianteRepository estudianteRepository;

    @Autowired
    private CursoRepository cursoRepository;

    @Autowired
    private InscripcionRepository inscripcionRepository;


    @Transactional
    public void inscripcion(@NotNull @Positive Long estudianteId, @NotNull @Positive Long cursoId, LocalDate fechaDeInscripcion , Status status) {


        Estudiante estudiante = estudianteRepository
                .findById(estudianteId)
                .orElseThrow(() -> new RuntimeException("id del estudiante no valido"));



        Curso curso = cursoRepository
                .findById(cursoId)
                .orElseThrow(() -> new RuntimeException("id del curso no valido"));



        Inscripcion inscripcion = new Inscripcion(
                null,
                fechaDeInscripcion,
                curso,
                estudiante,
                status
        );

        inscripcionRepository.save(inscripcion);




    }

}
