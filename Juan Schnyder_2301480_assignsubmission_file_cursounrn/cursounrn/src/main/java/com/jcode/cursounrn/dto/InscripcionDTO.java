package com.jcode.cursounrn.dto;


import com.jcode.cursounrn.domain.Status;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;
@Data
@AllArgsConstructor
public class InscripcionDTO {

      private LocalDate fechaDeInscripcion;
      private Long curso;
      private Long estudiante;
      private Status status;

}
