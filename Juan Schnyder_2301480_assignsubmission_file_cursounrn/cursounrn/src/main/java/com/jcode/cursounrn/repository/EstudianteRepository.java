package com.jcode.cursounrn.repository;

import com.jcode.cursounrn.domain.Estudiante;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;



import java.util.List;

public interface EstudianteRepository extends JpaRepository<Estudiante, Long> {


    List<Estudiante> findByApellidoAndNombre(String apellido,String nombre);

    boolean existsByNombre(String nombre);




    @Query("SELECT e FROM Estudiante e WHERE e.edad > 20 OR e.nombre='fer'")
    List<Estudiante> findByNombreAndEdad();

    @Query("SELECT e FROM Estudiante e WHERE e.edad BETWEEN :minEdad AND :maxEdad")
    List<Estudiante> findByEdad(@Param("minEdad") int minEdad, @Param("maxEdad") int maxEdad);


    boolean existByNombre(String nombre);



}
