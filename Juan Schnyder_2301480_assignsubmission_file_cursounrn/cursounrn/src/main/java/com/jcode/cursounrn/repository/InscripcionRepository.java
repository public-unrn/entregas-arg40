package com.jcode.cursounrn.repository;

import com.jcode.cursounrn.domain.Inscripcion;
import com.jcode.cursounrn.domain.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import java.util.List;

public interface InscripcionRepository extends JpaRepository<Inscripcion, Long> {


    List<Inscripcion> findByStatusInscripcion(List<Status> status);

    @Query("SELECT i FROM Inscripcion i WHERE status = 'PENDIENTE' OR status = 'RECHAZADO'")
    List<Inscripcion> findByInscripcionPendienteOrRechazada();

   // @Query("SELECT i FROM Inscripcion i WHERE status = :status")
   // List<Inscripcion> findByStatus(@Param("status")Status status);

    @Query("SELECT i FROM Inscripcion i WHERE status = 'ACEPTADO', nativeQuery = true")
    List<Inscripcion> findAllTablaNativa(@Param("status") String status);



}
