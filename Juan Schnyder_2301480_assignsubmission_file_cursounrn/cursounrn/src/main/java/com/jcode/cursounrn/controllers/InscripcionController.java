package com.jcode.cursounrn.controllers;


import com.jcode.cursounrn.dto.InscripcionDTO;
import com.jcode.cursounrn.services.InscripcionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/inscripcion")
public class InscripcionController {

      @Autowired
      private InscripcionService inscripcionService;


      @PostMapping
      public void save(@RequestBody InscripcionDTO inscripcionDTO) {

            inscripcionService.inscripcion(inscripcionDTO.getEstudiante(), inscripcionDTO.getCurso(), inscripcionDTO.getFechaDeInscripcion(), inscripcionDTO.getStatus());

      }

}
