package com.jcode.cursounrn.domain;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;

@Entity
@Table( name = "inscripcion")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Inscripcion {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private Long id;
    @Column( name = "fechaDeInscripcion")
    private LocalDate fechaDeInscripcion;
    @ManyToOne
    @JoinColumn( name = "curso_id")
    private Curso curso;
    @ManyToOne
    @JoinColumn( name = "estudiante_id")
    private Estudiante estudiante;
    @Enumerated(EnumType.STRING)
    private Status status;



}
