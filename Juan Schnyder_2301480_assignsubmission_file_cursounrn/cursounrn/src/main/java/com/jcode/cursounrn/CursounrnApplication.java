package com.jcode.cursounrn;

import com.jcode.cursounrn.domain.Curso;
import com.jcode.cursounrn.domain.Estudiante;
import com.jcode.cursounrn.domain.Inscripcion;
import com.jcode.cursounrn.domain.Status;
import com.jcode.cursounrn.repository.CursoRepository;
import com.jcode.cursounrn.repository.EstudianteRepository;
import com.jcode.cursounrn.repository.InscripcionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;


import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;

@SpringBootApplication

public class CursounrnApplication {

	public static void main(String[] args) {
		SpringApplication.run(CursounrnApplication.class, args);
	}

	@Autowired
	EstudianteRepository estudianteRepository;

	@Autowired
	CursoRepository cursoRepository;

	@Autowired
	InscripcionRepository inscripcionRepository;


	private void saveEstudiante() {
		estudianteRepository.saveAll(Arrays.asList(
				new Estudiante(null, "fer", "c", "fer@email.com", 3424299L, LocalDate.of(1987,Month.MAY,15)),
				new Estudiante(null, "Juan", "s", "js@email.com", 2344299L, LocalDate.of(1997,Month.MAY,15)),
				new Estudiante(null, "andres", "J", "aj@email.com", 2342349L, LocalDate.of(1987,Month.MAY,15)),
				new Estudiante(null, "paula", "romero", "hj@email.com", 2332423L, LocalDate.of(1987,Month.MAY,15))
		));
	}

	private void saveCurso() {
		cursoRepository.saveAll(Arrays.asList(
				new Curso(null, "biologia", "materia", LocalDate.of(2023,Month.JULY,12), LocalDate.of(2023,Month.SEPTEMBER,12)),
				new Curso(null, "springboot", "materia", LocalDate.of(2023,Month.JULY,12), LocalDate.of(2023,Month.SEPTEMBER,12)),
				new Curso(null, "musica", "materia", LocalDate.of(2023,Month.JULY,12), LocalDate.of(2023,Month.SEPTEMBER,12))
		));
	}






	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		return args -> {


			saveEstudiante();
			saveCurso();
			estudianteRepository.findAll();

			//inscripcionRepository.findByStatus(Status.ACEPTADO);



		};
	}
}

