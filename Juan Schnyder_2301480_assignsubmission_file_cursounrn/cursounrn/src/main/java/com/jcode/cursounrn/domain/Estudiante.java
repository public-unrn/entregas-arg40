package com.jcode.cursounrn.domain;

import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.time.LocalDate;
import java.time.Period;

@Entity
@Table( name = "estudiante")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Estudiante {
       @Id
       @GeneratedValue( strategy = GenerationType.IDENTITY)
       private Long id;
       @Column( name = "nombre")
       private String nombre;
       @Column( name = "apellido")
       private String apellido;
       @Column( name = "email")
       private String email;
       @Column( name = "dni")
       private Long dni;
       @Column( name = "fechaDeNacimiento")
       private LocalDate fechaDeNacimiento;
       @Transient
       private int edad;

    public Estudiante(Long id, String nombre, String apellido, String email, Long dni, LocalDate fechaDeNacimiento) {
    }   // averiguar si esto es correcto, no me funcionaba estudianteService


    public static int edadActual(LocalDate fechaNacimiento) {
        return Period.between(fechaNacimiento, LocalDate.now()).getYears();
    }

    @Override
    public String toString() {
        return "Estudiante{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                ", email='" + email + '\'' +
                ", dni=" + dni +
                ", fechaDeNacimiento=" + fechaDeNacimiento +
                ", edad=" + edad +
                '}';
    }
}
