package com.jcode.cursounrn.services;

import com.jcode.cursounrn.domain.Curso;
import com.jcode.cursounrn.dto.CursoDTO;
import com.jcode.cursounrn.repository.CursoRepository;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CursoService {

    private final CursoRepository cursoRepository;

    public CursoService(final CursoRepository cursoRepository) {
        this.cursoRepository = cursoRepository;
    }

    public List<CursoDTO> findAll() {
        final List<Curso> cursos = cursoRepository.findAll(Sort.by("id"));
        return cursos.stream()
                .map(curso -> mapToDTO(curso, new CursoDTO(curso.getNombre(),curso.getDescripcion(),curso.getFechaDeInicio(),curso.getFechaDeFin())))
                .toList();
    }

    public CursoDTO get(final Long id) {
        return cursoRepository.findById(id)
                .map(curso -> mapToDTO(curso, new CursoDTO(curso.getNombre(), curso.getDescripcion(), curso.getFechaDeInicio(),curso.getFechaDeFin())))
                .orElseThrow();
    }

    public Long create(final CursoDTO cursoDTO) {
        final Curso curso = new Curso();
        mapToEntity(cursoDTO, curso);
        return cursoRepository.save(curso).getId();
    }

    public void update(final Long id, final CursoDTO cursoDTO) {
        final Curso curso = cursoRepository.findById(id)
                .orElseThrow();
        mapToEntity(cursoDTO, curso);
        cursoRepository.save(curso);
    }

    public void delete(final Long id) {
        cursoRepository.deleteById(id);
    }

    private CursoDTO mapToDTO(final Curso curso, final CursoDTO cursoDTO) {

        cursoDTO.setNombre(curso.getNombre());
        cursoDTO.setDescripcion(curso.getDescripcion());
        cursoDTO.setFechaDeInicio(curso.getFechaDeInicio());
        cursoDTO.setFechaDeFin(curso.getFechaDeFin());
        return cursoDTO;
    }

    private void mapToEntity(final CursoDTO cursoDTO, final Curso curso) {
        curso.setNombre(cursoDTO.getNombre());
        curso.setDescripcion(cursoDTO.getDescripcion());
        curso.setFechaDeInicio(cursoDTO.getFechaDeInicio());
        curso.setFechaDeFin(cursoDTO.getFechaDeFin());
    }

}
