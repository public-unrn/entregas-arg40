package com.jcode.cursounrn.controllers;

import com.jcode.cursounrn.dto.EstudianteDTO;
import com.jcode.cursounrn.services.EstudianteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/estudiante")
public class EstudianteController {
    @Autowired
    private EstudianteService estudianteService;



    @GetMapping
    public List<EstudianteDTO> all() {
        return estudianteService.findAll();
    }


    @GetMapping("/{id}")
    public EstudianteDTO find(@PathVariable Long id) {
        return estudianteService.find(id);
    }


    @PostMapping
    public EstudianteDTO save(@RequestBody EstudianteDTO estudianteDTO) {

        return estudianteService.saveEstudiante(estudianteDTO);

    }


    @PutMapping("/{id}")
    public EstudianteDTO update(@PathVariable Long id, @RequestBody EstudianteDTO userDTO) {

        return estudianteService.update(id, userDTO);

    }


    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {

        estudianteService.delete(id);

    }








}
