package com.jcode.cursounrn.repository;

import com.jcode.cursounrn.domain.Curso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import java.time.LocalDate;
import java.util.List;

public interface CursoRepository extends JpaRepository<Curso, Long> {

    @Query("SELECT c FROM Curso c")
    List<Curso> findAllCursos();

    @Query("SELECT c FROM Curso c WHERE c.fechaDeInicio > :fecha")
    List<Curso> findAllFecha(@Param("fechaDeInicio")LocalDate fechaDeInicio);

    List<Curso> findByDescripcion(String descripcion);


}
