package com.cursoSpring.exceptions;

public class MenorDeEdadException extends RuntimeException {
    public MenorDeEdadException(String message) {
        super(message);
    }
}

