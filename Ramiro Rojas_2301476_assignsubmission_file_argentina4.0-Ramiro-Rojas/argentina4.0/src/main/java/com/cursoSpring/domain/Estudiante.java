package com.cursoSpring.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
@Entity
@Table(name = "estudiante")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Estudiante {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idEstudiante;
    @Column
    private String nombre;
    @Column
    private String apellido;
    @Column
    private String email;
    @Column
    private Integer dni;
    @Column(name = "fecha_nacimiento")
    private LocalDate fechaNacimiento;
    @Transient
    private Integer edad;

    public boolean esMayorDeEdad() {
        return edad >= 18;
    }
}
