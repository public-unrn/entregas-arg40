package com.cursoSpring.domain;

public enum Estado {
    ACEPTADA,
    RECHAZADA,
    PENDIENTE
}
