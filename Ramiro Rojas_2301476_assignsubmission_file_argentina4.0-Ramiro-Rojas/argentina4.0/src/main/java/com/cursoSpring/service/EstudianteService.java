package com.cursoSpring.service;

import com.cursoSpring.domain.Estudiante;
import com.cursoSpring.dto.EstudianteDTO;
import com.cursoSpring.repository.EstudianteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service

public class EstudianteService {
    @Autowired
    private EstudianteRepository estudianteRepository;

    public EstudianteDTO save(EstudianteDTO estudianteDTO){
        Estudiante estudiante = new Estudiante(
                estudianteDTO.getIdEstudiante(),
                estudianteDTO.getNombre(),
                estudianteDTO.getApellido(),
                estudianteDTO.getEmail(),
                estudianteDTO.getDni(),
                estudianteDTO.getFechaNacimiento(),
                estudianteDTO.getEdad()
        );
        estudianteRepository.save(estudiante);
        return estudianteDTO;
    }

    public List<EstudianteDTO> findAll(){
        return estudianteRepository.findAll()
                .stream().map(c -> new EstudianteDTO(
                        c.getIdEstudiante(),
                        c.getNombre(),
                        c.getApellido(),
                        c.getEmail(),
                        c.getDni(),
                        c.getFechaNacimiento(),
                        c.getEdad()
                )).collect(Collectors.toList());
    }

    public EstudianteDTO update(Long id, EstudianteDTO estudianteDTO){
        Estudiante estudiante = new Estudiante(
                id,
                estudianteDTO.getNombre(),
                estudianteDTO.getApellido(),
                estudianteDTO.getEmail(),
                estudianteDTO.getDni(),
                estudianteDTO.getFechaNacimiento(),
                estudianteDTO.getEdad()
        );
        estudianteRepository.save(estudiante);
        return estudianteDTO;
    }

    public EstudianteDTO find(Long id){
        Optional<Estudiante> estudianteOptional = estudianteRepository.findById(id);
        if (estudianteOptional.isEmpty()){
            throw new RuntimeException("Estudiante id no encontrado");
        }
        Estudiante estudiante = estudianteOptional.get();

        return new EstudianteDTO(estudiante.getIdEstudiante(),
                estudiante.getNombre(),
                estudiante.getApellido(),
                estudiante.getEmail(),
                estudiante.getDni(),
                estudiante.getFechaNacimiento(),
                estudiante.getEdad()
        );
    }

    public void delete(Long id){
        estudianteRepository.deleteById(id);
    }

    public Page<Estudiante> listarEstudiantesPaginadosOrdenadosPorDni(int nroPagina, int tamañoPagina) {
        // Crear un objeto Pageable para indicar la paginación y el orden
        Pageable pageable = PageRequest.of(nroPagina, tamañoPagina, Sort.by("dni").ascending());

        // Llamar al método findAllByOrderByDniAsc con el objeto pageable
        return estudianteRepository.findAllByOrderByDniAsc(pageable);
    }



}
