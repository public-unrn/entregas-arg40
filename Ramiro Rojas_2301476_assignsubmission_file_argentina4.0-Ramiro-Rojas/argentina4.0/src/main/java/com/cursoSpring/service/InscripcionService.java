package com.cursoSpring.service;

import com.cursoSpring.domain.Curso;
import com.cursoSpring.domain.Estado;
import com.cursoSpring.domain.Estudiante;
import com.cursoSpring.domain.Inscripcion;
import com.cursoSpring.dto.InscripcionDTO;
import com.cursoSpring.exceptions.MenorDeEdadException;
import com.cursoSpring.repository.CursoRepository;
import com.cursoSpring.repository.EstudianteRepository;
import com.cursoSpring.repository.InscripcionRepository;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class InscripcionService {
    @Autowired
    private InscripcionRepository inscripcionRepository;

    @Autowired
    private EstudianteRepository estudianteRepository;

    @Autowired
    private CursoRepository cursoRepository;

    public InscripcionDTO save(InscripcionDTO inscripcionDTO){
        Estudiante estudiante = estudianteRepository.findById(inscripcionDTO.getIdEstudiante())
                .orElseThrow(() -> new RuntimeException("No se encontró el estudiante con el ID proporcionado."));

        Curso curso = cursoRepository.findById(inscripcionDTO.getIdCurso())
                .orElseThrow(() -> new RuntimeException("No se encontró el curso con el ID proporcionado."));

        Inscripcion inscripcion = new Inscripcion(
            inscripcionDTO.getIdInscripcion(),
            inscripcionDTO.getFechaInscripcion(),
            estudiante,
            curso,
            inscripcionDTO.getEstado()
        );

        inscripcionRepository.save(inscripcion);
        return inscripcionDTO;
    }
    public List<Inscripcion> listarInscripcionesPorEstadoNativo(String estado) {
        return inscripcionRepository.findAllByEstadoNativo(estado);
    }
    @Transactional
    public void inscribirEstudianteACurso(@NotNull @Positive Long estudianteId, @NotNull @Positive Long cursoId) {
        Estudiante estudiante = estudianteRepository.findById(estudianteId)
                .orElseThrow(() -> new RuntimeException("No se encontró el estudiante con el ID proporcionado."));

        Curso curso = cursoRepository.findById(cursoId)
                .orElseThrow(() -> new RuntimeException("No se encontró el curso con el ID proporcionado."));

        if(!estudiante.esMayorDeEdad()) {
            throw new MenorDeEdadException("El estudiante es menor de edad");

        }
        Inscripcion inscripcion = new Inscripcion();
        inscripcion.setFechaInscripcion(java.time.LocalDate.now());
        inscripcion.setEstudiante(estudiante);
        inscripcion.setCurso(curso);
        inscripcion.setEstado(Estado.ACEPTADA);
        inscripcionRepository.save(inscripcion);
    }
}
