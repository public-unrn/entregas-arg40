package com.cursoSpring.service;

import com.cursoSpring.domain.Curso;
import com.cursoSpring.dto.CursoDTO;
import com.cursoSpring.repository.CursoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CursoService {
    @Autowired
    private CursoRepository cursoRepository;
    public CursoDTO save(CursoDTO cursoDTO){
        Curso curso = new Curso(
                cursoDTO.getIdCurso(),
                cursoDTO.getNombre(),
                cursoDTO.getDescripcion(),
                cursoDTO.getFechaInicio(),
                cursoDTO.getFechaFin()
        );
        cursoRepository.save(curso);
        return cursoDTO;
    }
}
