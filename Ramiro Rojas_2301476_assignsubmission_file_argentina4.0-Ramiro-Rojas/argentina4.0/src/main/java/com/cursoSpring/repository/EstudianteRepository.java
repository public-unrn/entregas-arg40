package com.cursoSpring.repository;

import com.cursoSpring.domain.Estudiante;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EstudianteRepository extends JpaRepository<Estudiante, Long> {
    // Método para listar todos los estudiantes
    List<Estudiante> findAll();

    // Método para listar todos los estudiantes que tengan un dni mayor a 20M y que su apellido sea
    //“Romero”
    List<Estudiante> findByDniGreaterThanAndApellido(Integer dni, String apellido);

    // Método para listar todos los estudiantes de forma paginada y ordenada ascendente por DNI
    Page<Estudiante> findAllByOrderByDniAsc(Pageable pageable);

}
