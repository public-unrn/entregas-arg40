package com.cursoSpring.repository;

import com.cursoSpring.domain.Curso;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface CursoRepository extends JpaRepository<Curso, Long> {
    // Método para listar todos los cursos
    List<Curso> findAll();

    // Método para listar todos los cursos que hayan empezado después de "01/02/2020"
    List<Curso> findByFechaInicioAfter(LocalDate fechaInicio);


}
