package com.cursoSpring.repository;

import com.cursoSpring.domain.Estado;
import com.cursoSpring.domain.Inscripcion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InscripcionRepository extends JpaRepository<Inscripcion, Long> {
    // Listar todas las inscripciones rechazadas o pendientes
    // Método para listar todas las inscripciones con el estado especificado
    //Listar todas las inscripciones en base a un parámetro de estado
    List<Inscripcion> findByEstadoIn(Estado... estados);

    //Listar todas las inscripciones en base a un parámetro de estado utilizando consulta nativa
    @Query(value = "SELECT * FROM inscripcion WHERE estado = :estadoParam", nativeQuery = true)
    List<Inscripcion> findAllByEstadoNativo(@Param("estadoParam") String estado);

}
