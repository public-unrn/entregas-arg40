package com.cursoSpring;

import com.cursoSpring.domain.Curso;
import com.cursoSpring.repository.EstudianteRepository;
import com.cursoSpring.repository.CursoRepository;
import com.cursoSpring.service.EstudianteService;
import com.cursoSpring.service.InscripcionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import com.cursoSpring.domain.Estudiante;
import org.springframework.data.domain.Page;

import java.util.Arrays;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Autowired
	EstudianteRepository estudianteRepository;

	@Autowired
	CursoRepository cursoRepository;

	@Autowired
	InscripcionService inscripcionService;

	@Autowired
	EstudianteService estudianteService;

	private void saveEstudiantes() {
		estudianteRepository.saveAll(Arrays.asList(
			new Estudiante(1L, "Juan", "Perez", "juanperez@mail.com", 12345678, null, 20),
			new Estudiante(2L, "Maria", "Gomez", "mgomez@mail.com", 87654321, null, 17),
			new Estudiante(3L, "Pedro", "Gonzalez", " pedrog@mail.com", 11223344, null, 15),
			new Estudiante(4L, "Ana", "Fernandez", "anafdez@gmail.com", 44332211, null, 18)
		));
	}

	private void saveCursos() {
		cursoRepository.saveAll(Arrays.asList(
			new Curso(11L, "Java", "Curso de Java", null, null),
			new Curso(12L, "Python", "Curso de Python", null, null),
			new Curso(13L, "JavaScript", "Curso de JavaScript", null, null)
		));
	}

	@Bean
	public CommandLineRunner commandLineRunner (ApplicationContext ctx) {
		return args -> {
			saveCursos();
			saveEstudiantes();
			/*
			// Validando que el estudiante sea mayor de edad(1L y 4L) sea el que se inscriba
			inscripcionService.inscribirEstudianteACurso(1L, 11L); //Juan, 20 años
			inscripcionService.inscribirEstudianteACurso(2L, 11L); //Maria, 17 años
			inscripcionService.inscribirEstudianteACurso(4L, 13L); //Ana, 18 años
			*/

			// Listando todos los estudiantes paginados ordenados por DNI ascendente
			Page<Estudiante> pagina1Tamano5 = estudianteService.listarEstudiantesPaginadosOrdenadosPorDni(1, 5);
			Page<Estudiante> pagina0Tamano2 = estudianteService.listarEstudiantesPaginadosOrdenadosPorDni(0, 2);

			// Imprimiendo los resultados de la consulta paginada
			System.out.println("Página 1, tamaño 5:");
			pagina1Tamano5.forEach(System.out::println);

			System.out.println("Página 0, tamaño 2:");
			pagina0Tamano2.forEach(System.out::println);
		};
	};
}


