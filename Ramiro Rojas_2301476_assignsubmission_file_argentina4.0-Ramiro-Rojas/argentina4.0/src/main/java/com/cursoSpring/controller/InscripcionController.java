package com.cursoSpring.controller;
import com.cursoSpring.dto.InscripcionDTO;
import com.cursoSpring.dto.EstudianteDTO;
import com.cursoSpring.dto.CursoDTO;
import com.cursoSpring.service.InscripcionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/inscripciones")
public class InscripcionController {
    @Autowired
    InscripcionService inscripcionService;

    @PostMapping
    public InscripcionDTO save(@RequestBody InscripcionDTO inscripcionDTO){
        return inscripcionService.save(inscripcionDTO);
    }

}
