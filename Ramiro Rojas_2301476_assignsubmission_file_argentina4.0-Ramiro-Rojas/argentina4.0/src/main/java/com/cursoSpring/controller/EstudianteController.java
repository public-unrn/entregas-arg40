package com.cursoSpring.controller;
import com.cursoSpring.dto.EstudianteDTO;
import com.cursoSpring.service.EstudianteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/estudiantes")
public class EstudianteController {
    @Autowired
    private EstudianteService estudianteService;

    @PostMapping
    public EstudianteDTO save(@RequestBody EstudianteDTO estudianteDTO){
        return estudianteService.save(estudianteDTO);
    }

    @GetMapping
    public Iterable<EstudianteDTO> findAll(){
        return estudianteService.findAll();
    }

    @GetMapping("/{id}")
    public EstudianteDTO findById(@RequestParam Long id){
        return estudianteService.find(id);
    }

    @PutMapping
    public EstudianteDTO update(@RequestParam Long id, @RequestBody EstudianteDTO estudianteDTO){
        return estudianteService.update(id, estudianteDTO);
    }

    @DeleteMapping
    public void delete(@RequestParam Long id){
        estudianteService.delete(id);
    }

}
