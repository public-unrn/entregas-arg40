package com.cursoSpring.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class EstudianteDTO {
    private Long idEstudiante;
    private String nombre;
    private String apellido;
    @NotBlank
    @Email
    private String email;
    private Integer dni;
    private LocalDate fechaNacimiento;
    private Integer edad;

}
