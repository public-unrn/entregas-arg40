package com.cursoSpring.dto;

import com.cursoSpring.domain.Estado;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class InscripcionDTO {

    private Long idInscripcion;
    private LocalDate fechaInscripcion;
    private Long idEstudiante;
    private Long idCurso;
    private Estado estado;

}
