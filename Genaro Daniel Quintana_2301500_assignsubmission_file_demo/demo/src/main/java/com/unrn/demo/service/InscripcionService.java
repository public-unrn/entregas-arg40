package com.unrn.demo.service;

import com.unrn.demo.repository.CursoRepository;
import com.unrn.demo.repository.EstudianteRepository;
import com.unrn.demo.repository.InscripcionRepository;
import com.unrn.demo.domain.InscripcionDomain;


import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import java.time.LocalDate;

@Service
@Validated
public class InscripcionService {

    @Autowired
        private EstudianteRepository estudianteRepository;
    @Autowired
        private CursoRepository cursoRepository;

    @Autowired
        private InscripcionRepository inscripcionRepository;

    @Transactional

    public void inscribir(@NotNull @Positive Long EstudianteId, @NotNull @Positive Long CursoId, LocalDate fechaInscripcion) {

        InscripcionDomain inscripcionDomain = new InscripcionDomain(

                null,
                fechaInscripcion,
                EstudianteId,
                CursoId

        );

        inscripcionRepository.save(inscripcionDomain);
    }
}
