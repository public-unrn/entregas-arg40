package com.unrn.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.unrn.demo.domain.CursoDomain;
public interface CursoRepository extends JpaRepository<CursoDomain, Long> {
}
