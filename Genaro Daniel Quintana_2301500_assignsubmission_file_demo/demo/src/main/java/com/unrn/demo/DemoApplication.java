package com.unrn.demo;

import com.unrn.demo.domain.EstudianteDomain;
import com.unrn.demo.repository.EstudianteRepository;
import com.unrn.demo.domain.CursoDomain;
import com.unrn.demo.repository.CursoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
		@Autowired
		EstudianteRepository estudianteRepository;

		@Autowired
		CursoRepository cursoRepository;

	private void saveEstudiante() {
		estudianteRepository.saveAll(Arrays.asList(
				new EstudianteDomain(null, "Ale", "Huaiquilican", "ahuaquilican@email.com", "111", 25),
				new EstudianteDomain(null, "Jose", "Luis", "jl@email.com", "222", 35),
				new EstudianteDomain(null, "Mauro", "Juan", "mj@email.com", "333", 20),
				new EstudianteDomain(null, "Horacio", "Juan", "hj@email.com", "444", 15)
		));
	}

	private void saveInscripcion() {
		cursoRepository.saveAll(Arrays.asList(
				new CursoDomain(null, "Python", "Gratis", 1999, 2000),
				new CursoDomain(null, "Java", "Gratis", 2009, 2010),
				new CursoDomain(null, "C#", "Gratis", 2014, 2015)
		));
	}



		@Bean

			public CommandLineRunner commandLineRunner(ApplicationContext ctx){

				return args -> {

					saveEstudiante();
					saveInscripcion();

				};
	}
}