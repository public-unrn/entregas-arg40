package com.unrn.demo.controller;

import com.unrn.demo.dto.InscripcionDTO;
import com.unrn.demo.service.InscripcionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
@RequestMapping("/inscripcion")
public class InscripcionController {

    @Autowired
        private InscripcionService inscripcionService;


    @PostMapping
        public void save(@RequestBody InscripcionDTO inscripcionDTO) {
            inscripcionService.inscribir(inscripcionDTO.getEstudiante(), inscripcionDTO.getCurso(), inscripcionDTO.getFechaInscripcion());

    }

}
