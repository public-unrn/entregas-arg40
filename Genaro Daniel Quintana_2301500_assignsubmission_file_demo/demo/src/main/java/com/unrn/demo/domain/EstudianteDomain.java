package com.unrn.demo.domain;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
@Entity
@Table (name = "Estudiante")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class EstudianteDomain {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

        @Column (name = "DNI")
            private Integer dni;

        @Column (name = "Nombres")
            private String nombre;

        @Column (name = "Apellidos")
            private String apellido;

        @Column (name = "Fecha de nacimiento")
            private LocalDate fechaDeNacimiento;

    @Transient
    private Integer edad;

        @Column (name = "Telefono")
            private String telefono;

        @Column (name="Email")
            private String email;

    public EstudianteDomain(Long id, String nombre, String apellido, Integer dni, LocalDate fechaDeNacimiento, Integer edad, String telefono, String email) {
    }

    public EstudianteDomain(Object o, String ale, String huaiquilican, String mail, String number, int i) {
    }


    @Override
    public String toString() {

        return "Estudiante{" +

                "ID=" + id +
                ", Nombres'" + nombre + '\'' +
                ", Apellidos='" + apellido + '\'' +
                ", email='" + email + '\'' +
                ", Telefono='" + telefono + '\'' +
                ", Edad=" + edad +
                '}';

    }


}
