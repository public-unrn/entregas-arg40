package com.unrn.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class EstudianteDTO {

    private Integer dni;

    private String nombre;

    private String apellido;

    private LocalDate fechaDeNacimiento;

    private Integer edad;

    private String telefono;

    private String email;


    public EstudianteDTO(String nombre, String apellido, Integer dni, Integer edad, LocalDate fechaDeNacimiento, String telefono, String email) {
    }

    public EstudianteDTO(String nombre, String apellido, Integer dni, LocalDate fechaDeNacimiento, Integer edad, String telefono, String email) {
    }
}
