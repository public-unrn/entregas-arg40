package com.unrn.demo.domain;

import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Table ( name= "curso")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class CursoDomain {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;

        @Column (name = "Nombre")
            private String nombre;
        @Column (name = "Descripcion")
            private String descripcion;
        @Column (name = "Fecha de inicio")
            private LocalDate fechaInicio;
        @Column (name = "Fecha de Finalización")
            private LocalDate fechaFin;

    public CursoDomain(Long id, String python, String gratis, int i, int i1) {
    }
}
