package com.unrn.demo.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
@Entity
@Table (name = "inscripcion")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class InscripcionDomain {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long id;

        @Column (name = "fechaInscripcion")
             private LocalDate fechaInscripcion;
        @Column (name = "estado")
             private String estado;


    @ManyToOne
    @JoinColumn(name = "Curso_id")
    private CursoDomain cursoDomain;

    @ManyToOne
    @JoinColumn(name = "Estudiante_id")
    private EstudianteDomain etudiante;

    public InscripcionDomain(Object o, LocalDate fechaInscripcion, Long estudianteId, Long cursoId) {
    }

}
