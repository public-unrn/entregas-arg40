package com.unrn.demo.service;

import com.unrn.demo.domain.EstudianteDomain;
import com.unrn.demo.dto.EstudianteDTO;
import com.unrn.demo.repository.EstudianteRepository;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class EstudianteService {

    @Autowired
    private EstudianteRepository estudianteRepository;


    public EstudianteDTO saveEstudiante(EstudianteDTO estudianteDTO){
        EstudianteDomain estudianteDomain = new EstudianteDomain(

                null,
                estudianteDTO.getNombre(),
                estudianteDTO.getApellido(),
                estudianteDTO.getDni(),
                estudianteDTO.getFechaDeNacimiento(),
                estudianteDTO.getEdad(),
                estudianteDTO.getTelefono(),
                estudianteDTO.getEmail()

        );

    estudianteRepository.save(estudianteDomain);

    return estudianteDTO;

    }

    public List<EstudianteDTO> findAll() {
        return estudianteRepository.findAll()
                .stream().map(c -> new EstudianteDTO (c.getNombre(),c.getApellido(), c.getDni(),c.getFechaDeNacimiento(),c.getEdad(), c.getTelefono(), c.getEmail()))
                .collect(Collectors.toList());

    }
    public EstudianteDTO update(Long id, EstudianteDTO estudianteDTO) {

        EstudianteDomain estudianteDomain = new EstudianteDomain(id, estudianteDTO.getNombre(), estudianteDTO.getApellido(), estudianteDTO.getDni(), estudianteDTO.getFechaDeNacimiento(), estudianteDTO.getEdad(), estudianteDTO.getTelefono(), estudianteDTO.getEmail());

        estudianteRepository.save(estudianteDomain);

        return estudianteDTO;

    }

   public EstudianteDTO find(Long id) {

       Optional<EstudianteDomain> estudianteDomainOptional = estudianteRepository.findById(id);

       if (estudianteDomainOptional.isEmpty()) {
           throw new RuntimeException("Id NO VALIDO");

       }

       EstudianteDomain estudianteDomain = estudianteDomainOptional.get();

       return new EstudianteDTO(estudianteDomain.getNombre(), estudianteDomain.getApellido(), estudianteDomain.getDni(), estudianteDomain.getEdad(), estudianteDomain.getFechaDeNacimiento(), estudianteDomain.getTelefono(), estudianteDomain.getEmail());

   }
       public void delete (Long id) { estudianteRepository.deleteById(id);}
   }
