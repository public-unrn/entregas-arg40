package com.unrn.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.unrn.demo.domain.InscripcionDomain;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
public interface InscripcionRepository extends JpaRepository<InscripcionDomain, Long> {

    @Query(value = "SELECT distinct i.* from inscripcion as i" +
            "inner join estudiante as es on es.id=i.estudiante_id"+
            "inner join curso as cu on cu.id=i.curso_id" ,
            nativeQuery = true
    )

    List<InscripcionDomain> findAllInscripcion();


}
