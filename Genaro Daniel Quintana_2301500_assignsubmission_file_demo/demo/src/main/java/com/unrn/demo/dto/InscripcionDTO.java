package com.unrn.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;
@Data
@AllArgsConstructor
public class InscripcionDTO {

    private LocalDate fechaInscripcion;

    private Long estudiante;

    private String estado;

    private Long curso;






}
