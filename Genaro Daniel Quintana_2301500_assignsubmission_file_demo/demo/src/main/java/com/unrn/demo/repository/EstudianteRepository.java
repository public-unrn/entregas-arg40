package com.unrn.demo.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import com.unrn.demo.domain.EstudianteDomain;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EstudianteRepository extends JpaRepository<EstudianteDomain, Long> {

    List<EstudianteDomain> findByapellidoAndnombre(String apellido, String nombre);

    List<EstudianteDomain> findByapellidoStartingWith(String prefix);

    boolean existsBynombre(String nombre);

    long countBynombre(String nombre);


}
