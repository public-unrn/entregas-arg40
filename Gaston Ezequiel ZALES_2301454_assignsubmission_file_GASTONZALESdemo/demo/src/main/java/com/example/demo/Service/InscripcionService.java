package com.example.demo.Service;

import com.example.demo.domain.Curso;
import com.example.demo.domain.Estado;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;
import com.example.demo.dto.EstudianteDTO;
import com.example.demo.dto.InscripcionDTO;
import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.repository.InscripcionRepository;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Validated
public class InscripcionService {
    @Autowired
    CursoRepository cursorepository;
    @Autowired
    EstudianteRepository estudianterepository;
    @Autowired
    InscripcionRepository inscripcionrepository;


    @Transactional

    public void Anotar(LocalDate fechainscripcion, Estado estado, @NotNull @Positive Long cursoid, @NotNull @Positive Long estudianteid) {

        Estudiante estudiante = estudianterepository
                .findById(estudianteid)
                .orElseThrow(() -> new RuntimeException("El id del customer no es valido"));
        if (!estudiante.esMayorEdad()) {
            throw new RuntimeException("El customer es menor de edad.");
        }


        Curso curso = cursorepository
                .findById(cursoid)
                .orElseThrow(() -> new RuntimeException("El id del car no es valido"));

        Inscripcion inscripcion = new Inscripcion(
                null,
                fechainscripcion,
                estado,
                curso,
                estudiante
        );
        inscripcionrepository.save(inscripcion);

    }
    public List<InscripcionDTO> findAll() {
        return inscripcionrepository.findAll()
                .stream().map(c -> new InscripcionDTO(c.getFechadeinscripcion(), c.getEstado(), c.getCurso().getId(), c.getEstudiante().getId()))
                .collect(Collectors.toList());
    }
}

