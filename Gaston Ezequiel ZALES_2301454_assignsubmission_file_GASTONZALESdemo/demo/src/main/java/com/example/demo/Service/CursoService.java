package com.example.demo.Service;
import com.example.demo.domain.Curso;
import com.example.demo.dto.CursoDTO;
import com.example.demo.repository.CursoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Validated


public class CursoService {
    @Autowired
    CursoRepository cursorepository;
    public CursoDTO saveCurso(CursoDTO cursoDTO){
        Curso curso=new Curso(null,
                cursoDTO.getDescripcion(),
                cursoDTO.getFechainicio(),
                cursoDTO.getFechafin()
        );
        cursorepository.save(curso);

        return  cursoDTO;

    }
   public List<CursoDTO> findAll(){
      return cursorepository.findAll()
                .stream().map(c -> new CursoDTO(c.getDescripcion(),c.getFechainicio(),c.getFechafin()))
                .collect(Collectors.toList());
    }

}
