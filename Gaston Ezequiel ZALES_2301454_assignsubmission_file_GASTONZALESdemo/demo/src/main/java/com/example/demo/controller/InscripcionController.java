package com.example.demo.controller;

import com.example.demo.Service.CursoService;
import com.example.demo.Service.InscripcionService;
import com.example.demo.dto.CursoDTO;
import com.example.demo.dto.EstudianteDTO;
import com.example.demo.dto.InscripcionDTO;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/Inscricion")
public class InscripcionController {
    @Autowired
    private InscripcionService inscripcionService;
    @PostMapping
    public void save(@RequestBody InscripcionDTO inscripcionDTO){
        inscripcionService.Anotar(inscripcionDTO.getFechadeinscripcion(),inscripcionDTO.getEstado(),inscripcionDTO.getCurso(),inscripcionDTO.getEstudiante());
    }
}
