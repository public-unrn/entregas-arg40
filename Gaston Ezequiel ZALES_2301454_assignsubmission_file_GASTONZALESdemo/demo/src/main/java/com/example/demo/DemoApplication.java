package com.example.demo;

import com.example.demo.Service.InscripcionService;
import com.example.demo.domain.Curso;
import com.example.demo.domain.Estado;
import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import com.example.demo.domain.Estudiante;


import java.time.LocalDate;
import java.util.Arrays;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
	@Autowired
	EstudianteRepository estudianteRepository;
	@Autowired
	CursoRepository cursorepository;
	@Autowired
	InscripcionService inscripcionService;
	public void saveestudiante() {
		estudianteRepository.saveAll(Arrays.asList(
				new Estudiante(null, "Gaston", "Romero", "Gz@gmail.com", 43790568,LocalDate.of(1998,6,22), 25),
				new Estudiante(null, "Giorno", "Giovana", "Giogio@gmail.com", 36588790,LocalDate.of(1969,6,20), 46),
				new Estudiante(null, "Jotaro", "Cujo", "jojo@gmail.com", 40639790,LocalDate.of(1959,7,2), 64),
				new Estudiante(null, "Joline", "Cujo", "joline@gmail.com", 40639790,LocalDate.of(1994,7,2), 28)

		));
	}
	public void savecurso() {
		cursorepository.saveAll(Arrays.asList(
				new Curso(null, "Informatica",  LocalDate.of(2019,5,1), LocalDate.of(2025,11,5)) ,
				new Curso(null, "Matematicas",  LocalDate.of(2018,3,5), LocalDate.of(2024,4,6)),
				new Curso(null, "Administracion", LocalDate.of(2015,2,8), LocalDate.of(2028,5,7))
		));
	}


	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx){
		return args -> {
			saveestudiante();
			savecurso();

			inscripcionService.Anotar(LocalDate.of(2020,1,12),
					Estado.RECHAZADO,
					1L,
					1L
			);
			inscripcionService.Anotar(LocalDate.of(2020,2,9),
					Estado.ACEPTADO,
					2L,
					2L
			);
			inscripcionService.Anotar(LocalDate.of(2020,3,7),
					Estado.PENDIENTE,
					3L,
					3L
			);
			inscripcionService.Anotar(LocalDate.of(2020,2,9),
					Estado.ACEPTADO,
					3L,
					4L
			);

			estudianteRepository.findAll();
		};
	}
}
