package com.example.demo.repository;
import com.example.demo.domain.Curso;
import com.example.demo.domain.Estudiante;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EstudianteRepository extends JpaRepository<Estudiante, Long> {
    //Listar todos los estudiantes
    @Query("SELECT c From Estudiante c")
    List<Estudiante> findAllEstudiantes();
    //Listar todos los estudiantes que tengan un dni mayor a 20M y que su apellido sea
    //“Romero”
    @Query("SELECT c From Estudiante c WHERE c.dni > 20000000 OR c.lastName = 'Romero'")
    List<Estudiante> findBydniorlast();

    ///consultasderivadas
    //Listar todos los estudiantes
   List<Estudiante> findAll();

   //Listar todos los estudiantes que tengan un dni mayor a 20M y que su apellido sea
   //“Romero”
    List<Estudiante> findByDniGreaterThanAndLastNameIs(int dni,String lastname);

    //Listar todos los estudiantes de forma paginada y ordenada ascendente por DNI
    //Probar las siguientes combinaciones:
    @Query("SELECT c From Estudiante c ORDER BY c.dni ASC")
    List<Estudiante> findByOrder();
}
