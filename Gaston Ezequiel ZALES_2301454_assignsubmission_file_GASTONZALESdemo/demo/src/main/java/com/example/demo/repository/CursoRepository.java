package com.example.demo.repository;
import com.example.demo.domain.Curso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface CursoRepository extends JpaRepository<Curso, Long>{
    //Listar todos los cursos
    @Query("SELECT c From Curso c")
    List<Curso> findAllCursos();

    //Listar todos los cursos que hayan empezado después de “01/02/2020”
    @Query("SELECT c From Curso c Where c.fechainicio > :date")
    List<Curso> findAllPostDay(@Param("date") LocalDate date);

    //Listar todos los cursos (Derivada)
    List<Curso> findAll();
}
