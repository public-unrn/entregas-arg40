package com.example.demo.repository;
import com.example.demo.domain.Estado;
import com.example.demo.domain.Inscripcion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface InscripcionRepository extends JpaRepository<Inscripcion, Long>{
    //Listar todas las inscripciones rechazadas o pendiente
    @Query("SELECT c From Inscripcion c Where c.estado='PENDIENTE' OR c.estado='RECHAZADO'")
    List<Inscripcion> findAllPendienteOrRecahzada();

    //● Listar todas las inscripciones en base a un parámetro de estado
    @Query("SELECT c From Inscripcion c Where c.estado= :estado")
    List<Inscripcion> findByEstado(@Param("estado") Estado estado);

    //Listar todas las inscripciones en base a un parámetro de estado utilizando consulta
    //nativa
    @Query(value = "SELECT * From Inscripcion c Where c.estado= :estado",nativeQuery = true)
    List<Inscripcion> findByEstadoNat(@Param("estado") Estado estado);


    //////CONSULTAS DERIVADAS
    //Listar todas las inscripciones rechazadas o pendiente
    List<Inscripcion> findByEstadoIs(Estado estado);

    List<Inscripcion> findByEstadoContaining(Estado estado);
    //Listar todas las inscripciones en base a un parámetro de estado
    List<Inscripcion> findByEstadoIn(List<Estado> estados);
    List<Inscripcion> findByEstadoIsOrEstadoIs(Estado pend,Estado recha);

}
