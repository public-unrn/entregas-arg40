package com.example.demo.services;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import com.example.demo.dto.CourseDTO;
import com.example.demo.entities.Course;
import com.example.demo.repository.CourseRepository;

import jakarta.transaction.Transactional;
import jakarta.validation.constraints.NotNull;

@Service
@Validated
public class CourseService {
	@Autowired
	CourseRepository courseRepository;
	
	@Transactional
	public void createCourse(@NotNull String courseName, @NotNull LocalDate startDate, @NotNull LocalDate endDate, @NotNull String description) {
		try {
			if(startDate.isAfter(endDate)) {
				throw new RuntimeException("La fecha de inicio no puede ser después de la fecha de fin");
			}
		} catch (RuntimeException err) {
			System.out.println(err);
		}
		Course course = new Course(courseName, startDate, endDate, description);
		courseRepository.save(course);
	}
	
	public CourseDTO saveCourse(CourseDTO courseDTO) {
		Course course = new Course(
				courseDTO.getCourseName(),
				courseDTO.getStartDate(),
				courseDTO.getEndDate(),
				courseDTO.getDescription()
				);
		courseRepository.save(course);
		return courseDTO;
	}
}
