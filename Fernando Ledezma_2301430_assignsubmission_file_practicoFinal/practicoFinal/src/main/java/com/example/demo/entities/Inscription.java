package com.example.demo.entities;

/*Importaciones*/
import java.time.LocalDate;

import com.example.demo.status.Status;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/*Anotaciones*/
@Entity
@Table(name = "inscripcion")
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class Inscription {
	
	/*Métodos*/
	@Override
	public String toString() {
		return "Inscription [ id =" + id + "\ncourse_id = " + course_id
				+ "\nstudent_id = " + student_id + "\n startDate = " + startDate + ", status = " + status + " ]";
	}
	
	/*Propiedades*/
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "course_id")
	private Course course_id;
	
	@ManyToOne
	@JoinColumn(name = "student_id")
	private Student student_id;
	
	@Column(name = "start_date", nullable = false)
	@Setter
	private LocalDate startDate;
	
	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private Status status;
}








