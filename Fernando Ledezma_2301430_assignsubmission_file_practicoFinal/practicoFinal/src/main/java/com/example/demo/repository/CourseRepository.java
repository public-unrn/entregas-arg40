package com.example.demo.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.entities.Course;
@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {
	/*Ambos métodos hacen lo mismo, hice los dos para prácticar*/
	List<Course> findAllByStartDateAfter(LocalDate startDate);
	
	@Query("SELECT c FROM Course c WHERE c.startDate > :startDate")
	List<Course> findByStartDateAfter(@Param("startDate") LocalDate startDate);
}
