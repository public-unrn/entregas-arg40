package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.entities.Inscription;
import com.example.demo.status.Status;

@Repository
public interface InscriptionRepository extends JpaRepository<Inscription, Long> {
	List<Inscription> findByStatusIn(Status[] status);
	
	//Ambas hacen lo mismo, hice ambos por práctica
	List<Inscription> findByStatus(Status status);
	
	@Query(value = "SELECT * FROM inscripcion ins WHERE ins.status = :status", nativeQuery = true)
	List<Inscription> findAllByStatus(@Param("status") String status); //El Enum Status persiste en la DB como String, entonces en vez de pasarle un Enum en consulta sql nativa debo pasar como String
	
	//Ambas hacen lo mismo, hice ambos por práctica
	@Query(value = "SELECT DISTINCT ins.* FROM inscripcion as ins"
			+ " INNER JOIN curso as cur ON cur.id=ins.course_id"
			+ " INNER JOIN estudiante as est ON est.id=ins.student_id"
			+ " ORDER BY ins.id ASC",
			nativeQuery = true)
	List<Inscription> findAllByOrderByIdAsc();
	
	List<Inscription> findByOrderByIdAsc();
	
	//Para poder eliminar las entidades que comparten relaciones intermedias debía eliminar la entidad relación primero, de lo contrario saltaba una excepción.
	@Query(value = "SELECT CASE WHEN COUNT(*) > 0 THEN TRUE ELSE FALSE END FROM inscripcion ins WHERE ins.student_id = :id", nativeQuery = true)
	boolean existsByStudentId(@Param("id") Long id);
	
	@Modifying
	@Query(value = "DELETE FROM inscripcion WHERE student_id = :id", nativeQuery = true)
	void deleteByStudentId(@Param("id") Long id);
}



