package com.example.demo.services;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import com.example.demo.dto.StudentDTO;
import com.example.demo.entities.Student;
import com.example.demo.repository.InscriptionRepository;
import com.example.demo.repository.StudentRepository;

import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;

@Service
@Validated
public class StudentService {
	@Autowired
	StudentRepository studentRepository;
	
	@Autowired
	InscriptionRepository inscriptionRepository;
	
	public void createStudent(@NotNull String surName, @NotNull String forName, @NotNull LocalDate birthDate, @NotNull Long dni, @NotNull String email) {
		Student student = new Student(surName, forName, birthDate, dni, email);
		studentRepository.save(student);
	}
	
	public StudentDTO saveStudent(StudentDTO studentDTO) {
		Student student = new Student (
				studentDTO.getSurName(),
				studentDTO.getForName(),
				studentDTO.getBirthDate(),
				studentDTO.getDni(),
				studentDTO.getMail()
				);
		studentRepository.save(student);
		return studentDTO;
	}
	
	public StudentDTO updateStudent(@NotNull @Positive Long id, StudentDTO studentDTO) {
		Student student = new Student(id, studentDTO.getSurName(), studentDTO.getForName(), studentDTO.getBirthDate(), studentDTO.getDni(), studentDTO.getMail());
		studentRepository.save(student);
		return studentDTO;
	}
	@Transactional
	public void deleteStudent(@NotNull @Positive Long id) {
		if(inscriptionRepository.existsByStudentId(id)) {//Comprobamos si el estudiante existe en el repositorio de inscripción
			inscriptionRepository.deleteByStudentId(id);//si existe eliminanos la inscripción y luego el estudiante, de lo contrario me saltaba una excepción
		}
		studentRepository.deleteById(id);
	}
	
	public List<StudentDTO> findAllStudents(){
		return studentRepository.findAll()
				.stream()
				.map(entity -> new StudentDTO(entity.getSurName(), entity.getForName(), entity.getBirthDate(), entity.getDni(), entity.getEmail()))
				.collect(Collectors.toList());
	}
	@Transactional
	public StudentDTO findStudent(@NotNull @Positive Long id) {
		Optional<Student> optionalStudent = studentRepository.findById(id);
		try {
			if(optionalStudent.isEmpty()) {
				throw new EntityNotFoundException("No se encontrado el estudiante");
			}
		} catch(EntityNotFoundException err) {
			System.out.println(err);
		}
		Student student = optionalStudent.get();
		return new StudentDTO(student.getSurName(), student.getForName(), student.getBirthDate(), student.getDni(), student.getEmail());
	}
}








