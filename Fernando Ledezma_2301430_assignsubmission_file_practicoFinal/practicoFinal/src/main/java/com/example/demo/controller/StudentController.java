package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.StudentDTO;
import com.example.demo.services.StudentService;


@RestController
@RequestMapping("/estudiante")
public class StudentController {
	@Autowired
	private StudentService studentService;
	
	@PostMapping
	public StudentDTO save(@RequestBody StudentDTO studentDTO) {
		return studentService.saveStudent(studentDTO);
	}
	
	@PutMapping("/{id}")
	public StudentDTO update(@PathVariable Long id, StudentDTO studentDTO) {
		return studentService.updateStudent(id, studentDTO);
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id) {
		studentService.deleteStudent(id);
	}
	
	@GetMapping
	public List<StudentDTO> all() {
		return studentService.findAllStudents();
	}

	@GetMapping(value = "/{id}")
	public StudentDTO get(@PathVariable Long id) {
		return studentService.findStudent(id);
	}
}











