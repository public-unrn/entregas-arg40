package com.example.demo.entities;

/*Importaciones*/
import java.time.LocalDate;
import java.time.Period;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/*Anotaciones*/
@Entity
@Table(name = "estudiante")
@NoArgsConstructor
@Getter
public class Student {
	
	/*Constructor*/
	public Student(String surName, String forName, LocalDate birthDate, Long dni, String email) {
		this.surName = surName;
		this.forName = forName;
		this.birthDate = birthDate;
		this.dni = dni;
		this.email = email;
		setCurrentAge();
	}
	public Student(Long id, String surName, String forName, LocalDate birthDate, Long dni, String email) {
		this.id = id;
		this.surName = surName;
		this.forName = forName;
		this.birthDate = birthDate;
		this.dni = dni;
		this.email = email;
		setCurrentAge();
	}
	/*Métodos*/
	public void setCurrentAge() {
		currentAge = Period.between(birthDate, LocalDate.now()).getYears();
	}
	public boolean isAdult() {
		return Period.between(birthDate, LocalDate.now()).getYears() >= 18;
	}
	@Override
	public String toString() {
		return "Student [id=" + id + ", surName=" + surName + ", forName=" + forName + ", birthDate=" + birthDate
				+ ", dni=" + dni + ", email=" + email + ", currentAge=" + currentAge + "]";
	}

	/*Propiedades*/
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "sur_name", nullable = false)
	@Size(min = 2, max = 20)
	@Setter
	private String surName;
	
	@Column(name = "for_name", nullable = false)
	@Size(min = 2, max = 20)
	@Setter
	private String forName;
	
	@Column(name = "birth_date", nullable = false)
	@Setter
	private LocalDate birthDate;
	
	@Column(nullable = false)
	@Setter
	private Long dni;
	
	@Column(name = "mail", nullable = false)
	@Email
	@Size(min = 5, max = 30)
	@Setter
	private String email;
	
	@Transient
	private int currentAge;
}









