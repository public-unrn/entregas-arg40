package com.example.demo.services;

import java.time.LocalDate;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import com.example.demo.entities.Course;
import com.example.demo.entities.Inscription;
import com.example.demo.entities.Student;
import com.example.demo.repository.CourseRepository;
import com.example.demo.repository.InscriptionRepository;
import com.example.demo.repository.StudentRepository;
import com.example.demo.status.Status;

import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;

@Service
@Validated
public class InscriptionService {
	/*Inyección de dependencias*/
	@Autowired
	private StudentRepository studentRepository;
	@Autowired
	private CourseRepository courseRepository;
	@Autowired
	private InscriptionRepository inscriptionRepository;
	
	@Transactional
	public void courseInscription(@NotNull @Positive Long course_id, @NotNull @Positive  Long student_id, @NotNull LocalDate startDate) {
		Random random = new Random();
        Status status;
        Optional<Course> optionalCourse = courseRepository.findById(course_id);
        Optional<Student> optionalStudent = studentRepository.findById(student_id);
        
        /*Comprobamos la existencia de las entidades y guardamos en caso que existan, si no lanzamos una excepción*/
		try {
			Course course = optionalCourse.orElseThrow(() -> new EntityNotFoundException("No se encontrado la entidad curso"));
			Student student = optionalStudent
					.filter(entity -> entity.isAdult())
					.orElseThrow(() -> new EntityNotFoundException("No se ha encontrado la entidad estudiante que cumpla con los requisitos"));
			
			if(course.getStartDate().isAfter(startDate)) {//Si la fecha de inscripción es antes del inicio del curso, evalua entre los estados aceptado y pendiente
				int index = random.nextInt(Status.values().length - 1);
		        status =  Status.values()[index];
			} else {//Si la fecha de inscripción fue después de la fecha de inicio del curso queda automáticamente rechazado
				status = Status.RECHAZADO;
			}
			
			/*Almacenamos el objeto en la base de datos*/
			Inscription inscription = new Inscription(null, course, student, startDate, status);
			inscriptionRepository.save(inscription);
		} catch(EntityNotFoundException err) {
			System.out.println(err);
		}
	}
}








