package com.example.demo.generateEntities;

import java.time.LocalDate;
import java.time.Period;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.services.CourseService;
import com.example.demo.services.StudentService;

@Component
public class EntityBuilder {
	/*Inyección de servicio*/
	@Autowired
	StudentService studentService;
	@Autowired
	CourseService courseService;
	
	/*Objetos globales*/
	private static Random random = new Random();

	public void createStudent() {
        String[] surNameContainer = { "Gonzalez", "Rodriguez", "Lopez", "Martinez", "Garcia", "Hernandez", "Perez", "Sanchez", 
                "Fernandez", "Rivera", "Rojas", "Diaz", "Moreno", "Jimenez", "Ortega", "Reyes", "Mendoza", "Delgado",
                "Romero", "Vargas", "Navarro", "Morales", "Guerrero", "Cruz", "Cortes", "Castillo", "Ramos", "Acosta",
                "Soto", "Valenzuela", "Flores", "Medina", "Nuñez", "Chavez", "Molina", "Herrera", "Gomez", "Campos",
                "Alvarez", "Rosales", "Ponce", "Escobar", "Rios", "Carvajal", "Solis", "Romero", "Ramirez",
                "Avila", "Valdez", "Esquivel", "Rojas", "Beltran", "Cervantes", "Godinez", "Barrera", "Torres"
            };
        String[] nameContainer = {"Juan", "Maria", "Pedro", "Ana", "Luis", "Carolina", "Javier", "Lucia", "Gabriel", "Valentina",
                "Diego", "Camila", "Santiago", "Isabella", "Andres", "Mariana", "Carlos", "Daniela", "Manuel", "Laura",
                "Fernando", "Sofia", "Miguel", "Valeria", "Ricardo", "Alejandra", "Gonzalo", "Natalia", "Felipe", "Paula",
                "Sebastian", "Catalina", "Jorge", "Juliana", "David", "Isabel", "Pablo", "Daniella", "Antonio", "Gabriela",
                "Jose", "Elena", "Raul", "Alexandra", "Roberto", "Veronica", "Mario", "Stephanie", "Oscar", "Jennifer"
            };
        
        String surName = surNameContainer[random.nextInt(surNameContainer.length)];
        String forName = nameContainer[random.nextInt(nameContainer.length)];
        LocalDate birthDate = generateDate(1980, 2011);
        long dni = generateDni(birthDate);
        String typeMail = random.nextInt(3) < 2 ? "@gmail.com" : "@yahoo.com";
        String email = (surName + forName + typeMail).toLowerCase();
        
        /*Creamos el estudiante a través del servicio encargado*/
        studentService.createStudent(surName, forName, birthDate, dni, email);
	}
	
	public void generateCourse() {
		String courseContainer[] = { "Introduccion a la Programacion", "Algoritmos y Estructuras de Datos", "Programacion Orientada a Objetos",
	            "Desarrollo Web con HTML y CSS", "Bases de Datos y SQL", "Desarrollo de Aplicaciones Moviles", "Diseño de Interfaz de Usuario",
	            "Programacion Funcional", "Seguridad Informatica", "Inteligencia Artificial y Aprendizaje Automatico", "Desarrollo de Software Agil",
	            "Testing y QA", "Arquitectura de Software", "Despliegue y Administracion de Aplicaciones", "Programacion en Java",
	            "Programacion en Python", "Programacion en C++", "Programacion en JavaScript", "Programacion en Ruby", "Programacion en PHP"
		};
		String courseName = courseContainer[random.nextInt(courseContainer.length)];
		String defaultDescription = "Curso orientado a personas que desean conocer mas sobre el mundo de la programacion";
		LocalDate startDate = generateDate(2021, 2022);
		LocalDate endDate = generateDate(2022, 2023);
		
		/*Creamos el curso a través del servicio encargado*/
		courseService.createCourse(courseName, startDate, endDate, defaultDescription);
	}
	
	 public LocalDate generateDate(int minRange, int maxRange) {
	        int year = random.nextInt(maxRange - minRange) + minRange;
	        int month = random.nextInt(1, 13);
	        int day = random.nextInt(1, 29); // Considerando solo hasta el día 28 por simplicidad
	        return LocalDate.of(year, month, day);
	 }
	 
	 private long generateDni(LocalDate birthDate) {
		 int age = Period.between(birthDate, LocalDate.now()).getYears();
		 return   age > 35 ? random.nextLong(30000000L - 20000000L) + 20000000L
				: age > 30 ? random.nextLong(35000000L - 30000000L) + 30000000L
				: age > 25 ? random.nextLong(40000000L - 35000000L) + 35000000L
				: age > 20 ? random.nextLong(45000000L - 40000000L) + 40000000L
				: age > 15 ? random.nextLong(50000000L - 45000000L) + 45000000L
				: random.nextLong(53000000L - 50000000L) + 50000000L;
	 }
}
