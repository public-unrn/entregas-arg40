package com.example.demo.entities;

/*Importaciones*/
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/*Anotaciones*/
@Entity
@Table(name = "curso")
@NoArgsConstructor
@Getter
public class Course {
	/*Constructor*/
	public Course(String courseName, LocalDate startDate, LocalDate endDate, String description) {
		this.courseName = courseName;
		this.startDate = startDate;
		this.endDate = endDate;
		this.description = description;
		setDays();
	}
	
	/*Métodos*/
	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
		setDays();
	}
	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
		setDays();
	}
	public void setDays() {
		if(!Objects.isNull(startDate) && !Objects.isNull(endDate)) {
			days = (int) ChronoUnit.DAYS.between(startDate, endDate);
		}
	}
	
	@Override
	public String toString() {
		return "Course [ id = " + id + ", courseName = " + courseName + ", startDate = "
				+ startDate + ", endDate = " + endDate + ", days = " + days + "\ndescription = " + description + " ]";
	}
	
	/*Propiedades*/
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "course_name", nullable = false)
	@Setter
	private String courseName;
	
	@Column(name = "start_date", nullable = false)
	private LocalDate startDate;
	
	@Column(name = "end_date", nullable = false)
	private LocalDate endDate;
	
	@Column(name = "course_duration_days")
	private int days;
	
	@Column(length = 200)
	@Setter
	private String description;
}
