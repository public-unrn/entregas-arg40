package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.CourseDTO;
import com.example.demo.services.CourseService;

@RestController
@RequestMapping("/curso")
public class CourseController {
	@Autowired
	CourseService courseService;
	
	@PostMapping
	public CourseDTO save(@RequestBody CourseDTO courseDTO) {
		return courseService.saveCourse(courseDTO);
	}
}
