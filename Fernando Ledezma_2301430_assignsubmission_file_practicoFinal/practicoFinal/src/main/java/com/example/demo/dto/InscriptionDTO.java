package com.example.demo.dto;

import java.time.LocalDate;


import lombok.AllArgsConstructor;
import lombok.Data;
@Data
@AllArgsConstructor
public class InscriptionDTO {
	private Long course_id;
	private Long student_id;
	private LocalDate startDate;
}
