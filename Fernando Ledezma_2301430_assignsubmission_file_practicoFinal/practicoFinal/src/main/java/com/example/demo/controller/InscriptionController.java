package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.InscriptionDTO;
import com.example.demo.services.InscriptionService;

@RestController
@RequestMapping("/inscripcion")
public class InscriptionController {
	@Autowired
	InscriptionService inscriptionService;
	@PostMapping
	public void save(InscriptionDTO inscriptionDTO) {
		inscriptionService.courseInscription(inscriptionDTO.getCourse_id(), inscriptionDTO.getStudent_id(), inscriptionDTO.getStartDate());
	}
}
