package com.example.demo.dto;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class StudentDTO {
	String surName;
	String forName;
	LocalDate birthDate;
	Long dni;
	String mail;
}
