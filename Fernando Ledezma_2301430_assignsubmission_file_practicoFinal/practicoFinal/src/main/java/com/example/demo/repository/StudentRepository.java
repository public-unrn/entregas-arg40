package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.entities.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
	List<Student> findAllByDniGreaterThanAndSurName(Long dni, String surName);
	
	boolean existsBySurName(String surName);
	
	@Query("Select c FROM Student c")
	List<Student> findAllStudent();
	
	@Query("SELECT c FROM Student c WHERE YEAR(CURRENT_DATE) - YEAR(c.birthDate) BETWEEN :minAge AND :maxAge")
	List<Student> findByAgeRange(@Param("minAge") int minAge, @Param("maxAge") int maxAge);
}
