package com.example.demo.dto;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CourseDTO {
	private String courseName;
	private LocalDate startDate;
	private LocalDate endDate;
	private String description;
}
