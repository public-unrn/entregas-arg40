package com.example.demo;

import java.time.LocalDate;
import java.util.Random;

/*Importaciones*/
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.PageRequest;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.domain.Sort;

import com.example.demo.generateEntities.EntityBuilder;
import com.example.demo.repository.CourseRepository;
import com.example.demo.repository.InscriptionRepository;
import com.example.demo.repository.StudentRepository;
import com.example.demo.services.InscriptionService;
import com.example.demo.status.Status;

@SpringBootApplication
public class DemoApplication {
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
	
	/*Inyección de repositorios*/
	@Autowired
	CourseRepository courseRepository;
	@Autowired
	StudentRepository studentRepository;
	@Autowired
	InscriptionRepository inscriptionRepository;
	/*Inyección de servicios*/
	@Autowired
	InscriptionService inscriptionService;
	/*Inyección de otros componentes*/
	@Autowired
	EntityBuilder entityBuilder;
	
	@Bean
	public CommandLineRunner runProgram(ApplicationContext ctx) {
		return args -> {
			createAndSaveEntities();
			listCourses();
			listStudents();
			generateInscriptions();
			listInscriptions();
		};
	}
	private void createAndSaveEntities() {
		//Parámetros
		int cant = 10;
		int courseCant = cant > 10 ? 4 : 2;
		
		//Creamos las entidades
		for(int i = 0; i < cant; i++) {
			entityBuilder.createStudent();
			if(i % courseCant == 0) {//Para que no se creen tantos cursos
				entityBuilder.generateCourse();
			}
		}
	}
	
	private void listCourses() {
		//Parámetros de la consulta
		LocalDate startDate = LocalDate.of(2021, 7, 17);
		
		//Consultas al repositorio de los cursos
		System.out.println("\nListado de cursos");
		courseRepository.findAll().forEach(System.out::println);
		
		System.out.println("\nCURSOS QUE EMPEZARON DESPUES DE: " + startDate);
		courseRepository.findAllByStartDateAfter(startDate).forEach(System.out::println);
		
		System.out.println("\nLISTADO DE CURSOS ORDENADOS POR NOMBRE ASCENDENTE");
		courseRepository.findAll(Sort.by(Sort.Direction.ASC, "courseName")).forEach(System.out::println);;
	}
	
	private void listStudents() {
		//Parámetros de las consultas
		String surName = "Romero";
		long dni = 20000000L;
		PageRequest pageable = PageRequest.of(0, 5, Sort.by("dni").ascending());
		int minRange = 13, maxRange = 18;
		
		//Consultas al repositorio de estudiantes
		/*
		 Como el ejercicio 1 indicaba que la edad no debía persistir en la base de datos, puse un método para que cuando recuperemos la entidad podamos reestablecer
		 la edades, si no salta las edades más adelante no se trata de error sino que la propiedad no persiste en la base y hay que establecerla manualmente.
		 */
		System.out.println("\nLISTADO DE ESTUDIANTES:");
		studentRepository.findAll().forEach(entity -> {
			entity.setCurrentAge();
			System.out.println(entity);
		});
		
		if(studentRepository.existsBySurName(surName)) {
			System.out.println("\nLISTADO DE ESTUDIANTES POR DNI MAYOR A '" + dni + "' Y APELLIDO '" + surName + "'");
			studentRepository.findAllByDniGreaterThanAndSurName(dni, surName).forEach(System.out::println);
		}
		
		System.out.println("\nLISTADO PAGINADO DE ESTUDIANTES ORDENADOS POR DNI ASCENDENTE");
		studentRepository.findAll(pageable).forEach(System.out::println);;
		
		System.out.println("\nLISTADO DE ESTUDIANTES ENTRE LOS " + minRange + " Y " + maxRange);
		studentRepository.findByAgeRange(minRange, maxRange).forEach(element -> {
			element.setCurrentAge();//La propiedad currentAge es @Transient por lo cual no persiste en la DB, entonces a través del método podemos reestablecer ese valor en el objeto.
			System.out.println(element);
		});
	}
	
	private void generateInscriptions() {
		/*Generamos una inscripción automática por cada alumno dentro del repositorio*/
		Random random = new Random();
		long numberOfCourse = courseRepository.count() + 1;
		int numberOfStudent = (int) studentRepository.count();
		
		System.out.println("\nGENERAMOS LAS INSCRIPCIONES");
		for(int i = 1; i <= numberOfStudent; i++) {
			long course_id = random.nextLong(numberOfCourse - 1) + 1;
			LocalDate inscriptionDate = entityBuilder.generateDate(2021, 2022);
			inscriptionService.courseInscription(course_id, (long) i, inscriptionDate);
		}
	}
	private void listInscriptions() {
		
		Status[] status = {Status.PENDIENTE, Status.RECHAZADO};
		
		System.out.println("\nLISTADO DE INSCRIPCIONES CON ESTADO " + status[0] + " y " + status[1]);
		inscriptionRepository.findByStatusIn(status).forEach(System.out::println);
		
		System.out.println("\nLISTADO POR ESTADO " + Status.ACEPTADO + " EN SQL NATIVO");
		inscriptionRepository.findAllByStatus(Status.ACEPTADO.toString()).forEach(System.out::println);
		
		System.out.println("\nLISTADO DE TODAS LAS INSCRIPCIONES Y SUS RELACIONES POR SQL NATIVO");
		inscriptionRepository.findAllByOrderByIdAsc().forEach(System.out::println);
	}
}









