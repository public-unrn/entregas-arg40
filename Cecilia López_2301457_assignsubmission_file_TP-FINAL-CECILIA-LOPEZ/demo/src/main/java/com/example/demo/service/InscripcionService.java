package com.example.demo.service;

import com.example.demo.domain.Curso;
import com.example.demo.domain.EstadoInscripcion;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;
import com.example.demo.dto.InscripcionDTO;
import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.repository.InscripcionRepository;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Validated

public class InscripcionService {

    @Autowired
    private CursoRepository cursoRepository;

    @Autowired
    private EstudianteRepository estudianteRepository;

    @Autowired
    private InscripcionRepository inscripcionRepository;

    //Creación de una inscripción
    @Transactional
    public void registrarInscripcion(@NotNull EstadoInscripcion estado, @NotNull LocalDate fechaInscripcion, @NotNull @Positive(message = "El id del estudiante no puede ser negativo") Long idEstudiante, @NotNull @Positive(message = "El id del curso no puede ser negativo") Long idCurso) {

        Estudiante estudiante = estudianteRepository
                .findById(idEstudiante)
                .orElseThrow( () -> new RuntimeException("El id del estudiante no es válido."));


        if(estudiante.calcularEdad()<18){
            throw new RuntimeException("El estudiante debe ser mayor de edad");
        }

        Curso curso = cursoRepository
                .findById(idCurso)
                .orElseThrow( () -> new RuntimeException("El id del curso no es válido."));

        Inscripcion inscripcion = new Inscripcion(null, estado, fechaInscripcion, curso, estudiante);

        inscripcionRepository.save(inscripcion);

    };

    // Consulta de todas los inscripciones
    public List<InscripcionDTO> findAll() {
        return inscripcionRepository.findAll()
                .stream().map(i -> new InscripcionDTO(i.getId(), i.getEstado(), i.getFechaDeInscripcion(), i.getCurso().getId(), i.getEstudiante().getId()))
                .collect(Collectors.toList());
    }

    // Consulta de una inscripción en particular por id
    public InscripcionDTO find(@NotNull @Positive Long id) {
        Inscripcion inscripcion = inscripcionRepository
                .findById(id)
                .orElseThrow(() -> new RuntimeException("El id de la inscripción no es válido."));

        return new InscripcionDTO(inscripcion.getId(), inscripcion.getEstado(), inscripcion.getFechaDeInscripcion(), inscripcion.getCurso().getId(), inscripcion.getEstudiante().getId());
    }

    // Actualización de una inscripción
    @Transactional
    public InscripcionDTO update(@NotNull @Positive Long id, InscripcionDTO inscripcionDTO) {

        Curso curso = cursoRepository
                .findById(inscripcionDTO.getCursoId())
                .orElseThrow(() -> new RuntimeException("El id del curso no es válido."));

        Estudiante estudiante = estudianteRepository
                .findById(inscripcionDTO.getEstudianteId())
                .orElseThrow(() -> new RuntimeException("El id del estudiante no es válido."));

        Inscripcion inscripcion = new Inscripcion(id, inscripcionDTO.getEstado(), inscripcionDTO.getFechaDeInscripcion(), curso, estudiante);

        inscripcionRepository.save(inscripcion);

        return inscripcionDTO;
    }

    // Eliminación de una inscripción
    @Transactional
    public void delete(@NotNull @Positive Long id) {

        inscripcionRepository.deleteById(id);
    }
}
