package com.example.demo.service;

import com.example.demo.dto.CursoDTO;
import com.example.demo.domain.Curso;
import com.example.demo.repository.CursoRepository;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Validated
public class CursoService {

    @Autowired
    private CursoRepository cursoRepository;

    // Creacion de un curso
    @Transactional
    public CursoDTO saveCurso(CursoDTO cursoDTO){
        Curso curso = new Curso(
                null,
                cursoDTO.getNombre(),
                cursoDTO.getDescripcion(),
                cursoDTO.getFechaDeInicio(),
                cursoDTO.getFechaDeFin()
        );

        cursoRepository.save(curso);

        return cursoDTO;
    }

    // Consulta de todos los cursos
    public List<CursoDTO> findAll() {
        return cursoRepository.findAll()
                .stream().map(c -> new CursoDTO(c.getId(),c.getNombre(),c.getDescripcion(),c.getFechaDeInicio(),c.getFechaDeFin()))
                .collect(Collectors.toList());
    }

    // Consulta de un curso en particular por id
    public CursoDTO find(@NotNull @Positive Long id) {
        Curso curso = cursoRepository
                .findById(id)
                .orElseThrow(() -> new RuntimeException("El id del curso no es válido."));

        return new CursoDTO(curso.getId(), curso.getNombre(), curso.getDescripcion(),curso.getFechaDeInicio(),curso.getFechaDeFin());
    }

    // Actualización de un curso
    @Transactional
    public CursoDTO update(@NotNull @Positive Long id, CursoDTO cursoDTO) {
        Curso curso = new Curso(id, cursoDTO.getNombre(), cursoDTO.getDescripcion(), cursoDTO.getFechaDeInicio(),cursoDTO.getFechaDeFin());

        cursoRepository.save(curso);

        return cursoDTO;
    }

    // Eliminación de un curso
    @Transactional
    public void delete(@NotNull @Positive Long id) {

        cursoRepository.deleteById(id);
    }

}
