package com.example.demo.repository;

import com.example.demo.domain.Curso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface CursoRepository extends JpaRepository<Curso, Long> {

    // Listar todos los cursos con @Query
    @Query("SELECT c FROM Curso c ")
    List<Curso> findAllCursos();

    // Listar todos los cursos mediante consulta derivada
    List<Curso> findBy(); /* Al no especificar ninguna propiedad luego del By, trae todos los cursos.
    Es la alternativa que encontré para no usar el método predefinido findAll() */

    // Listar todos los cursos que hayan empezado después de “01/02/2020” con @Query
    @Query("SELECT c FROM Curso c WHERE c.fechaDeInicio > CAST('2020/02/01' AS java.time.LocalDate)")
    List<Curso> findAllCursosPost2020();

    // Alternativa: Listar todos los cursos que hayan empezado después de una fecha usando parámetros
    @Query("SELECT c FROM Curso c WHERE c.fechaDeInicio > :fecha")
    List<Curso> findAllCursosPost(@Param("fecha") LocalDate fecha);

    // Listar todos los cursos que hayan empezado después de “01/02/2020” mediante consulta derivada
    List<Curso> findAllByFechaDeInicioAfter(LocalDate fecha);
}
