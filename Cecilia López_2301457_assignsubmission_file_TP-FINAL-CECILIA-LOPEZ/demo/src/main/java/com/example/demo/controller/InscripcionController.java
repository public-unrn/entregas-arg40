package com.example.demo.controller;

import com.example.demo.dto.InscripcionDTO;
import com.example.demo.service.InscripcionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/inscripcion")
public class InscripcionController {

    @Autowired
    private InscripcionService inscripcionService;

    // Creación de una inscripción
    @PostMapping
    public void save(@RequestBody InscripcionDTO inscripcionDTO) {
        inscripcionService.registrarInscripcion(inscripcionDTO.getEstado(), inscripcionDTO.getFechaDeInscripcion(), inscripcionDTO.getEstudianteId(), inscripcionDTO.getCursoId());
    }

    // Consulta de todas los inscripciones
    @GetMapping
    public List<InscripcionDTO> all() {
        return inscripcionService.findAll();
    }

    // Consulta de una inscripción en particular por id
    @GetMapping("/{id}")
    public InscripcionDTO find(@PathVariable Long id) {
        return inscripcionService.find(id);
    }

    // Actualización de una inscripción
    @PutMapping("/{id}")
    public InscripcionDTO update(@PathVariable Long id, @RequestBody InscripcionDTO inscripcionDTO) {
        return inscripcionService.update(id, inscripcionDTO);
    }

    // Eliminación de una inscripción
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        inscripcionService.delete(id);
    }


}
