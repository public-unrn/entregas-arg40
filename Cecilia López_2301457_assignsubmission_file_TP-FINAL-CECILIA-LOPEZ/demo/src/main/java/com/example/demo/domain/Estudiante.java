package com.example.demo.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.Period;

@Entity
@Table(name = "estudiante")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Estudiante {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private int dni;

    private String email;

    private String apellido;

    private String nombre;

    @Column(name = "fecha_de_nacimiento")
    private LocalDate fechaDeNacimiento;

    @Transient
    private int edad;

    public Estudiante(Long id, int dni, String email, String apellido, String nombre, LocalDate fechaDeNacimiento){
        this.id = id;
        this.dni = dni;
        this.email = email;
        this.apellido = apellido;
        this.nombre = nombre;
        this.fechaDeNacimiento = fechaDeNacimiento;
        this.edad = calcularEdad();

    }

    /* Para verificar si el estudiante es mayor de edad, utilizo este método en lugar del visto
    en clase (que retornaba un booleano) porque al no persistir la edad en la BD,
    la edad siempre era cero al llamar a la función */
    public int calcularEdad() {
        LocalDate fechaActual = LocalDate.now();
        Period periodo = Period.between(fechaDeNacimiento, fechaActual);
        return periodo.getYears();
    }

}