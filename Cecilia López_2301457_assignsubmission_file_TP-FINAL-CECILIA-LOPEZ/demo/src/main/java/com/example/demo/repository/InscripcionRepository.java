package com.example.demo.repository;

import com.example.demo.domain.EstadoInscripcion;
import com.example.demo.domain.Inscripcion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InscripcionRepository extends JpaRepository<Inscripcion, Long> {

    // Listar todas las inscripciones rechazadas o pendiente con @Query
    @Query("SELECT i FROM Inscripcion i WHERE i.estado = 'RECHAZADA' OR i.estado = 'PENDIENTE'")
    List<Inscripcion> findAllInscripcionesNoAceptadas();

    // Listar todas las inscripciones rechazadas o pendiente mediante consulta derivada
    List<Inscripcion> findByEstadoIsOrEstadoIs(EstadoInscripcion rechazado, EstadoInscripcion pendiente);

    // Listar todas las inscripciones en base a un parámetro de estado con @Query
    @Query("SELECT i FROM Inscripcion i WHERE i.estado = :estado")
    List<Inscripcion> findByStatus(@Param("estado") EstadoInscripcion estado);

    // Listar todas las inscripciones en base a un parámetro de estado utilizando consulta nativa
    @Query(value = "SELECT * FROM inscripcion i WHERE estado = :estadoString", nativeQuery = true)
    List<Inscripcion> findByStatusNativa(@Param("estadoString") String estadoString);

    // Listar todas las inscripciones en base a un parámetro de estado como consulta derivada
    List<Inscripcion> findByEstado(EstadoInscripcion estado);
}
