package com.example.demo.controller;

import com.example.demo.dto.CursoDTO;
import com.example.demo.service.CursoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/curso")
public class CursoController {

    @Autowired
    private CursoService cursoService;

    // Creacion de un curso
    @PostMapping
    public CursoDTO save(@RequestBody CursoDTO cursoDTO) {
        return cursoService.saveCurso(cursoDTO);
    }

    // Consulta de todos los cursos
    @GetMapping
    public List<CursoDTO> all() {
        return cursoService.findAll();
    }

    // Consulta de un curso en particular por id
    @GetMapping("/{id}")
    public CursoDTO find(@PathVariable Long id) {
        return cursoService.find(id);
    }

    // Actualización de un curso
    @PutMapping("/{id}")
    public CursoDTO update(@PathVariable Long id, @RequestBody CursoDTO cursoDTO) {
        return cursoService.update(id, cursoDTO);
    }

    // Eliminación de un curso
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        cursoService.delete(id);
    }

}
