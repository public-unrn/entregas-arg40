package com.example.demo.domain;

public enum EstadoInscripcion {
    ACEPTADA("Aceptada"), 
    RECHAZADA("Rechazada"), 
    PENDIENTE("Pendiente");

    private String estado;

    EstadoInscripcion(String estado) {
        this.estado = estado;
    }

}
