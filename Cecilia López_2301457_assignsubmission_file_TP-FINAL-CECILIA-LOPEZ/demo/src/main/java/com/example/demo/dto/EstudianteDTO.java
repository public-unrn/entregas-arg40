package com.example.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class EstudianteDTO {

    private Long id; // Le paso el id porque quiero que lo traiga en la consulta
    private int dni;
    private String email;
    private String apellido;
    private String nombre;
    private LocalDate fechaDeNacimiento;
    // no le paso la edad porque la calculo a partir de la fecha de nacimiento y no la persisto

}
