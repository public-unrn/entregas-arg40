package com.example.demo.dto;

import com.example.demo.domain.EstadoInscripcion;
import lombok.AllArgsConstructor;
import lombok.Data;
import java.time.LocalDate;

@Data
@AllArgsConstructor
public class InscripcionDTO {

    private Long id;
    private EstadoInscripcion estado;
    private LocalDate fechaDeInscripcion;
    private Long cursoId;
    private Long estudianteId;

}
