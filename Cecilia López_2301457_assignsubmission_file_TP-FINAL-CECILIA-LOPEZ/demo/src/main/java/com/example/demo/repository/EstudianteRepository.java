package com.example.demo.repository;

import com.example.demo.domain.Estudiante;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EstudianteRepository extends JpaRepository<Estudiante, Long> {

    // Listar todos los estudiantes con @Query
    @Query("SELECT e FROM Estudiante e ")
    List<Estudiante> findAllEstudiantes();

    // Listar todos los estudiantes mediante consulta derivada
    List<Estudiante> findBy();

    /* Listar todos los estudiantes que tengan un dni mayor a 20M y que su apellido sea
    “Romero” con @Query */
    @Query("SELECT e from Estudiante e WHERE e.dni > 20000000 AND e.apellido = 'Romero'")
    List<Estudiante> findAllRomeroConDniMayorA20M();

    /* Listar todos los estudiantes que tengan un dni mayor a 20M y que su apellido sea
    “Romero” mediante consulta derivada */
    List<Estudiante> findByApellidoAndDniGreaterThan(String apellido, int dni);

    // Listar todos los estudiantes de forma paginada y ordenada ascendente por DNI
    Page<Estudiante> findAllByOrderByDniAsc(Pageable pageable);
    // No puedo paserle parámetros al pageable desde el repository, entonces deberia hacer a mano las consultas siguientes:
    // pagina 1, tamaño 5:
    // estudianteRepository.findAllByOrderByDniAsc(PageRequest.of(1,5,Sort.Direction.ASC, "dni"))
    // pagina 0, tamaño 2:
    // estudianteRepository.findAllByOrderByDniAsc(PageRequest.of(0,2,Sort.Direction.ASC, "dni"))

}
