package com.example.demo;

import com.example.demo.domain.Curso;
import com.example.demo.domain.EstadoInscripcion;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;
import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.repository.InscripcionRepository;
import com.example.demo.service.InscripcionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.time.LocalDate;
import java.util.Arrays;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {

		SpringApplication.run(DemoApplication.class, args);
	}

	@Autowired
	EstudianteRepository estudianteRepository;

	@Autowired
	CursoRepository cursoRepository;

	@Autowired
	InscripcionRepository inscripcionRepository;

	@Autowired
	InscripcionService inscripcionService;

	private void saveEstudiantes() {
		estudianteRepository.saveAll(Arrays.asList(
			new Estudiante(null, 30678543, "cecilia@gmail.com", "Lopez", "Cecilia", LocalDate.of(1984, 11, 19)),
			new Estudiante(null, 35678543, "sebastian@gmail.com", "Defeo", "Sebastian", LocalDate.of(1982,11,5)),
			new Estudiante(null, 19678534, "juan@gmail.com", "Romero", "Juan", LocalDate.of(1962,11,11)),
			new Estudiante(null, 30678556, "julian@gmail.com", "Romero", "Julian", LocalDate.of(1980,1,15)),
			new Estudiante(null, 40678555, "maria@gmail.com", "Romero", "Maria", LocalDate.of(2003,3,8)),
			new Estudiante(null, 67855680, "victoria@gmail.com", "Gomez", "Victoria", LocalDate.of(2008,8,6))
		));
	}

	private void saveCursos() {
		cursoRepository.saveAll(Arrays.asList(
			new Curso(null, "Inglés I", "Inglés para principiantes", LocalDate.of(2023,7,20), LocalDate.of(2023,12,20)),
			new Curso(null, "Inglés II", "Inglés intermedio", LocalDate.of(2023,7,20), LocalDate.of(2023,12,20)),
		 	new Curso(null, "Inglés III", "Inglés avanzado", LocalDate.of(2023,7,20), LocalDate.of(2023,12,20)),
			new Curso(null, "Francés I", "Francés para principiantes", LocalDate.of(2023,7,20), LocalDate.of(2023,12,20)),
			new Curso(null, "Francés II", "Francés intermedio", LocalDate.of(2023,7,20), LocalDate.of(2023,12,20)),
			new Curso(null, "Chino I", "Chino para principiantes", LocalDate.of(2023,7,20), LocalDate.of(2023,12,20)),
			new Curso(null, "Ruso I", "Ruso para principiantes", LocalDate.of(2009,7,20), LocalDate.of(2009,12,20))
		));
	}

	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx){
		return args -> {
			saveEstudiantes();
			saveCursos();

			// Registro de inscripciones para pruebas
			inscripcionService.registrarInscripcion(EstadoInscripcion.PENDIENTE, LocalDate.now(),1L,7L);
			inscripcionService.registrarInscripcion(EstadoInscripcion.PENDIENTE, LocalDate.now(),1L,6L);
			inscripcionService.registrarInscripcion(EstadoInscripcion.PENDIENTE, LocalDate.now(),2L,6L);
			inscripcionService.registrarInscripcion(EstadoInscripcion.PENDIENTE, LocalDate.now(),3L,1L);
			inscripcionService.registrarInscripcion(EstadoInscripcion.RECHAZADA, LocalDate.now(),5L,6L);
			inscripcionService.registrarInscripcion(EstadoInscripcion.RECHAZADA, LocalDate.now(),4L,3L);
			inscripcionService.registrarInscripcion(EstadoInscripcion.ACEPTADA, LocalDate.now(),3L,5L);
			inscripcionService.registrarInscripcion(EstadoInscripcion.ACEPTADA, LocalDate.now(),2L,4L);
			inscripcionService.registrarInscripcion(EstadoInscripcion.ACEPTADA, LocalDate.now(),4L,3L);

			System.out.println("HolaMundo");
		};
	}
}
