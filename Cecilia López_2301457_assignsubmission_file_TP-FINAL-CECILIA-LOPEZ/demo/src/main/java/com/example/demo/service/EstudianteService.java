package com.example.demo.service;

import com.example.demo.domain.Estudiante;
import com.example.demo.dto.EstudianteDTO;
import com.example.demo.repository.EstudianteRepository;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Validated
public class EstudianteService {

    @Autowired
    private EstudianteRepository estudianteRepository;

    // Creación de estudiante
    @Transactional
    public EstudianteDTO saveEstudiante(EstudianteDTO estudianteDTO){
        Estudiante estudiante = new Estudiante(
                null,
                estudianteDTO.getDni(),
                estudianteDTO.getEmail(),
                estudianteDTO.getApellido(),
                estudianteDTO.getNombre(),
                estudianteDTO.getFechaDeNacimiento()
                // La edad la calculo con un método y no la guardo en la BD.
        );

        estudianteRepository.save(estudiante);

        return estudianteDTO;
    }

    // Consulta de todos los estudiantes
    public List<EstudianteDTO> findAll() {
        return estudianteRepository.findAll()
                .stream().map(e -> new EstudianteDTO(e.getId(), e.getDni(),e.getEmail(),e.getApellido(),e.getNombre(),e.getFechaDeNacimiento()))
                .collect(Collectors.toList());
    }

    // Consulta de un estudiante en particular por id
    public EstudianteDTO find(@NotNull @Positive Long id) {
        Optional<Estudiante> estudianteOptional = estudianteRepository.findById(id);

        if (estudianteOptional.isEmpty()) {
            throw new RuntimeException("Id inválido");
        }

        Estudiante estudiante = estudianteOptional.get();

        return new EstudianteDTO(estudiante.getId(), estudiante.getDni(),estudiante.getEmail(), estudiante.getApellido(), estudiante.getNombre(), estudiante.getFechaDeNacimiento());
    }

    // Actualización de un estudiante
    @Transactional
    public EstudianteDTO update(@NotNull @Positive Long id, EstudianteDTO estudianteDTO) {
        Estudiante estudiante = new Estudiante(id, estudianteDTO.getDni(), estudianteDTO.getEmail(), estudianteDTO.getApellido(), estudianteDTO.getNombre(), estudianteDTO.getFechaDeNacimiento());

        estudianteRepository.save(estudiante);

        return estudianteDTO;
    }

    // Eliminación de un estudiante
    @Transactional
    public void delete(@NotNull @Positive Long id) {

        estudianteRepository.deleteById(id);
    }

}
