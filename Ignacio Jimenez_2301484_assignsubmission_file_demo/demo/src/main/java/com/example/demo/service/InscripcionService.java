package com.example.demo.service;

import com.example.demo.domain.Curso;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;
import com.example.demo.dto.InscripcionDTO;
import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.repository.InscripcionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Validated
public class InscripcionService {

    @Autowired
    private EstudianteRepository estudianteRepository;

    @Autowired
    private CursoRepository cursoRepository;

    @Autowired
    private InscripcionRepository inscripcionRepository;



    @Transactional
    public InscripcionDTO agregarInscripcion(InscripcionDTO inscripcionDTO, Long cursoId, Long estudianteId){

        Curso curso = cursoRepository.
                findById(cursoId)
                .orElseThrow(() -> new RuntimeException("El id del curso no fue encontrado"));

        Estudiante estudiante = estudianteRepository.
                findById(estudianteId)
                .orElseThrow(() -> new RuntimeException("El id del estudiante no fue encontrado"));

        Inscripcion inscripcion = new Inscripcion(
                null,
                inscripcionDTO.getFechaDeInscripcion(),
                inscripcionDTO.getEstado(),
                curso,
                estudiante
        );

        inscripcionRepository.save(inscripcion);

        return  inscripcionDTO;
    }

    public List<InscripcionDTO> listarInscripciones() {
        return inscripcionRepository.findAll()
                .stream().map(i -> new InscripcionDTO(
                        i.getFechaDeInscripcion(),
                        i.getEstado(),
                        i.getCurso(),
                        i.getEstudiante()))
                .collect(Collectors.toList());
    }

    @Transactional
    public InscripcionDTO actualizarInscripcion(Long id,@RequestBody InscripcionDTO inscripcionDTO){

        Optional<Inscripcion> inscripcionOptional = inscripcionRepository
                .findById(id)
                .stream().findFirst();

        if (inscripcionOptional.isPresent()){
            Inscripcion inscripcion = inscripcionOptional.get();
            inscripcion.setFechaDeInscripcion(inscripcionDTO.getFechaDeInscripcion());
            inscripcion.setEstado(inscripcionDTO.getEstado());
            inscripcion.setCurso(inscripcionDTO.getCursoID());
            inscripcion.setEstudiante(inscripcionDTO.getEstudianteID());

            inscripcionRepository.save(inscripcion);
        }

        return inscripcionDTO;
    }

    public InscripcionDTO buscarInscripcion(Long id){

        Optional<Inscripcion> inscripcionOptional = inscripcionRepository.findById(id);

        if(inscripcionOptional.isEmpty()){
            throw  new RuntimeException("El id ingresado de la inscripcion  no se encontro");
        }

        Inscripcion inscripcion = inscripcionOptional.get();

        return new InscripcionDTO(
                inscripcion.getFechaDeInscripcion(),
                inscripcion.getEstado(),
                inscripcion.getCurso(),
                inscripcion.getEstudiante());
    }

    @Transactional
    public void eliminarInscripcion(Long id){

        inscripcionRepository.deleteById(id);

    }
}
