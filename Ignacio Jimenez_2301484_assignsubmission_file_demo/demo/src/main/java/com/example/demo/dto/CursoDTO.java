package com.example.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CursoDTO {
    private String nombre;
    private String descripcion;
    private LocalDate fechaDeInicio;
    private LocalDate fechaDeFin;

}
