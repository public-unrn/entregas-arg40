package com.example.demo.repository;

import com.example.demo.domain.Estudiante;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EstudianteRepository extends JpaRepository<Estudiante, Long> {

    @Query("SELECT e FROM Estudiante e")
    List<Estudiante> findAllEstudiantes();

    @Query("SELECT e FROM Estudiante e WHERE e.dni > 20000000 AND e.apellido = 'Romero'")
    List<Estudiante> findByDniAndName();

    @Query("SELECT e FROM Estudiante e ORDER BY e.dni ASC")
    List<Estudiante> findOrderAscEstudiantes();

    List<Estudiante> findByOrderByNombreAsc();



    List<Estudiante> findByNombre(String nombre);


    boolean existsByNombre(String nombre);

    List<Estudiante> findByApellido(String apellido);


    List<Estudiante> findByApellidoStartingWith(String prefix);

    long countByNombre(String nombre);

    long countByApellido(String apellido);
}
