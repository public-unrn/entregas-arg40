package com.example.demo;

import com.example.demo.domain.Curso;
import com.example.demo.domain.Estado;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;
import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.repository.InscripcionRepository;
import com.example.demo.service.InscripcionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.util.Arrays;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Autowired
	EstudianteRepository estudianteRepository ;
	@Autowired
	CursoRepository cursoRepository;

	@Autowired
	InscripcionRepository inscripcionRepository;
	//InscripcionService inscripcionService;
	private void CrearEstudiantes() {
		estudianteRepository.saveAll(Arrays.asList(
				new Estudiante(null, "Carlos", "Acuña", "carlos@hotmail.com", 28875167, LocalDate.of(1984,11,20), 40),
				new Estudiante(null, "Eduardo", "Chabela", "Eduardo@hotmail.com", 34423313, LocalDate.of(1983,9,22), 36),
				new Estudiante(null, "Marta", "Alias", "marta@hotmail.com", 22323649, LocalDate.of(1956,2,16), 65),
				new Estudiante(null, "German", "Romero", "german@hotmail.com", 26342456, LocalDate.of(2004,4,12), 18),
				new Estudiante(null, "Marti", "Paz", "marti@hotmail.com", 34354112, LocalDate.of(2007,8,3), 15),
				new Estudiante(null, "Rodri", "Jimenez", "rodri@hotmail.com", 2312456, LocalDate.of(2006,3,1), 18),
				new Estudiante(null, "Lucero", "Garcia", "lucero@hotmail.com", 46235327, LocalDate.of(2012,1,23), 12)
		));
	}

	private void CrearCursos() {
		cursoRepository.saveAll(Arrays.asList(
				new Curso(null, "Programación con JAVA", "En este curso aprenderemos programacion desde cero en Java", LocalDate.of(2023,8,23), LocalDate.of(2023,2,12)),
				new Curso(null, "Introducción a Python", "Curso introductorio a la programación en Python", LocalDate.of(2018,1,24), LocalDate.of(2023,9,1)),
				new Curso(null, "Desarrollo Web con React", "Aprende a crear aplicaciones web usando React", LocalDate.now(), LocalDate.now())
		));
	}

	private void CrearInscripciones() {
		inscripcionRepository.saveAll(Arrays.asList(
				new Inscripcion(null, LocalDate.now(), Estado.Aceptado, cursoRepository.getReferenceById(1L), estudianteRepository.getReferenceById(2L)),
				new Inscripcion(null, LocalDate.now(), Estado.Pendiente, cursoRepository.getReferenceById(2L), estudianteRepository.getReferenceById(1L)),
				new Inscripcion(null, LocalDate.now(), Estado.Rechazado, cursoRepository.getReferenceById(3L), estudianteRepository.getReferenceById(5L)),
				new Inscripcion(null, LocalDate.now(), Estado.Pendiente, cursoRepository.getReferenceById(3L), estudianteRepository.getReferenceById(4L)),
				new Inscripcion(null, LocalDate.now(), Estado.Aceptado, cursoRepository.getReferenceById(1L), estudianteRepository.getReferenceById(3L)),
				new Inscripcion(null, LocalDate.now(), Estado.Aceptado, cursoRepository.getReferenceById(1L), estudianteRepository.getReferenceById(1L))
		));
	}

	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		return args -> {
			CrearEstudiantes();
			CrearCursos();
			CrearInscripciones();


			estudianteRepository.findAll();


			estudianteRepository.findAll(PageRequest.of(0, 2, Sort.by(Sort.Direction.ASC, "dni")));
			estudianteRepository.findAll(PageRequest.of(1, 5, Sort.by(Sort.Direction.ASC, "dni")));
		};
	}
}
