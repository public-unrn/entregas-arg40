package com.example.demo.Services;

import com.example.demo.DTO.EstudianteDTO;
import com.example.demo.Domain.Estudiante;
import com.example.demo.Repository.EstudianteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Validated
public class EstudianteService {

    @Autowired
    private EstudianteRepository estudianteRepository;

    public EstudianteDTO saveEstudiante(EstudianteDTO estudianteDTO){
        Estudiante estudiante = new Estudiante(
                null,
                estudianteDTO.getFirstName(),
                estudianteDTO.getSecondName(),
                estudianteDTO.getDni(),
                estudianteDTO.getEmail(),
                estudianteDTO.getFechaDeNacimiento(),
                estudianteDTO.getEdad()
        );

        estudianteRepository.save(estudiante);
        return estudianteDTO;
    }

    public List<EstudianteDTO> findAll(){
        return estudianteRepository.findAll()
                .stream().map(c -> new EstudianteDTO(c.getFirstName(), c.getSecondName(), c.getDni(), c.getEmail(), c.getFechaDeNaciemiento(), c.getEdad()))
                .collect(Collectors.toList());
    }

    public List<EstudianteDTO> findById(Long estudianteId){

        return estudianteRepository.findById(estudianteId)
                .stream().map(c -> new EstudianteDTO(c.getFirstName(), c.getSecondName(), c.getDni(), c.getEmail(), c.getFechaDeNaciemiento(), c.getEdad()))
                .collect(Collectors.toList());

    }

    public EstudianteDTO update(Long id, EstudianteDTO estudianteDTO) {
        Estudiante estudiante = new Estudiante(id, estudianteDTO.getFirstName(), estudianteDTO.getSecondName(), estudianteDTO.getDni(), estudianteDTO.getEmail(), estudianteDTO.getFechaDeNacimiento(), estudianteDTO.getEdad());

        estudianteRepository.save(estudiante);

        return estudianteDTO;

    }

    public void delete(Long Id){
        estudianteRepository.deleteById(Id);
    }

}
