package com.example.demo.Domain;

public enum Estado {
    Pendiente,
    Aprobado,
    Rechazado
}
