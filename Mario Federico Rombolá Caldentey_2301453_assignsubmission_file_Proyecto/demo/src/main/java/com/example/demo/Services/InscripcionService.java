package com.example.demo.Services;


import com.example.demo.DTO.EstudianteDTO;
import com.example.demo.DTO.InscripcionDTO;
import com.example.demo.Domain.Curso;
import com.example.demo.Domain.Estado;
import com.example.demo.Domain.Estudiante;
import com.example.demo.Domain.Inscripcion;
import com.example.demo.Repository.CursoRepository;
import com.example.demo.Repository.EstudianteRepository;
import com.example.demo.Repository.InscripcionRepository;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.stream.BaseStream;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Validated
public class InscripcionService {

    @Autowired
    private EstudianteRepository estudianteRepository;

    @Autowired
    private CursoRepository cursoRepository;

    @Autowired
    private InscripcionRepository inscripcionRepository;

    public void Inscripcion (@NotNull @Positive Long estudianteId, @NotNull @Positive Long cursoId, LocalDate fechaDeInscripcion, Estado estado){


        Estudiante estudiante = estudianteRepository
                .findById(estudianteId)
                .orElseThrow( () -> new RuntimeException("El id del estudiante no existe"));

        if (!estudiante.esMayorEdad()){
            throw new RuntimeException("El estudiante es menor de edad");
        }

        Curso curso = cursoRepository
                .findById(cursoId)
                .orElseThrow( () -> new RuntimeException("El id del curso no existe"));

        Inscripcion inscripcion = new Inscripcion(
                null,
                curso,
                estudiante,
                fechaDeInscripcion,
                estado);

        inscripcionRepository.save(inscripcion);


    }

    public InscripcionDTO saveInscripcion(InscripcionDTO inscripcionDTO){

        Curso curso = cursoRepository
                .findById(inscripcionDTO.getCurso_id())
                .orElseThrow(()-> new RuntimeException("El id curso no es valido"));

        Estudiante estudiante = estudianteRepository
                .findById(inscripcionDTO.getEstudiante_id())
                .orElseThrow(()-> new RuntimeException("El id del estudiante no es valido"));


        Inscripcion inscripcion = new Inscripcion(
                null,
                curso,
                estudiante,
                inscripcionDTO.getFechaDeInscripcion(),
                inscripcionDTO.getEstado());

        inscripcionRepository.save(inscripcion);
        return inscripcionDTO;

    }

    public List<InscripcionDTO> findAll(){
        return inscripcionRepository.findAll()
                .stream().map(c -> new InscripcionDTO(c.getCurso().getId(), c.getEstudainte().getId(), c.getFechaDeInscripcon(), c.getEstado()))
                .collect(Collectors.toList());
    }

    public List<InscripcionDTO> findById(Long inscripcionId){

        return inscripcionRepository.findById(inscripcionId)
                .stream().map(c -> new InscripcionDTO(c.getCurso().getId(), c.getEstudainte().getId(), c.getFechaDeInscripcon(), c.getEstado()))
                .collect(Collectors.toList());

    }

    public InscripcionDTO update(Long id, InscripcionDTO inscripcionDTO){

        Curso curso = cursoRepository
                .findById(inscripcionDTO.getCurso_id())
                .orElseThrow(()-> new RuntimeException("El id curso no es valido"));

        Estudiante estudiante = estudianteRepository
                .findById(inscripcionDTO.getEstudiante_id())
                .orElseThrow(()-> new RuntimeException("El id del estudiante no es valido"));

        Inscripcion inscripcion = new Inscripcion(id, curso, estudiante, inscripcionDTO.getFechaDeInscripcion(), inscripcionDTO.getEstado());

        inscripcionRepository.save(inscripcion);
        return inscripcionDTO;


    }

    public void delete(Long Id){
        inscripcionRepository.deleteById(Id);
    }


}

