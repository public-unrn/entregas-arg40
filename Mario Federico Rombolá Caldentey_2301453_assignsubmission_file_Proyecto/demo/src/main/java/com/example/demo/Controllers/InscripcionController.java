package com.example.demo.Controllers;

import com.example.demo.DTO.InscripcionDTO;
import com.example.demo.Services.InscripcionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/Inscripcion")
public class InscripcionController {
    @Autowired
    private InscripcionService inscripcionService;


    @PostMapping
    public InscripcionDTO save(@RequestBody InscripcionDTO inscripcionDTO){
        return inscripcionService.saveInscripcion(inscripcionDTO);
    }

    @GetMapping
    public List<InscripcionDTO> all(){
        return inscripcionService.findAll();
    }

    @GetMapping("/{id}")
    public List <InscripcionDTO> find(@PathVariable Long id){
        return inscripcionService.findById(id);
    }

    @PutMapping("/{id}")
    public InscripcionDTO update(@PathVariable Long id, @RequestBody InscripcionDTO inscripcionDTO){
        return inscripcionService.update(id, inscripcionDTO);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id){
        inscripcionService.delete(id);
    }


}
