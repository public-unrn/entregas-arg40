package com.example.demo.Repository;

import com.example.demo.Domain.Curso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface CursoRepository extends JpaRepository<Curso, Long> {

    //**Listar Todos Los Cursos**//
    @Query("SELECT c FROM Curso c")
    List<Curso> findAllCursos();

    //**Listar todos los cursos que empezaron  destpues de 01/02/2020**//
    @Query ("SELECT c FROM Curso c WHERE c.fechaDeInicio > :fechaDeInicio")
    List<Curso> findCursosByDate(@Param("fechaDeInicio") LocalDate fechaDeInicio);


    //**DERIVADAS**//

    //**Listar todos los cursos consulta derivada**//
    List<Curso> findAll();


    //**Listar Cursos inciados despues de 01/02/2020**//
    List<Curso> findCursoByFechaDeInicioAfter(LocalDate fechaDeInicio);


    //**Listar Cursos varias consultas Derivadas**//
    List<Curso> findByDescription(String descripcion);

    boolean existsByName(String nombre);

    long countByName(String nombre);

}
