package com.example.demo;

import com.example.demo.Domain.Curso;
import com.example.demo.Domain.Estado;
import com.example.demo.Domain.Estudiante;
import com.example.demo.Repository.CursoRepository;
import com.example.demo.Repository.EstudianteRepository;
import com.example.demo.Repository.InscripcionRepository;
import com.example.demo.Services.InscripcionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.util.Arrays;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Autowired
	EstudianteRepository estudianteRepository;

	@Autowired
	CursoRepository cursoRepository;

	@Autowired
	InscripcionRepository inscripcionRepository;

	@Autowired
	InscripcionService inscripcionService;

	private void saveEstudiantes(){

		estudianteRepository.saveAll(Arrays.asList(

				new Estudiante(null, "Federico", "Rombola", 29201152, "fabfour07@gmail.com", LocalDate.of(1981, 12, 15), 41),
				new Estudiante(null, "Jorge", "Castro",19925278, "jcastro@hotmail.com", LocalDate.of(1982, 11, 25), 54),
				new Estudiante(null, "Carolina", "Pullman", 30552565, "capull@gmail.com", LocalDate.of(1986, 6, 23), 37),
				new Estudiante(null, "Juan", "Romero", 32325652, "juanr@gmail.com", LocalDate.of(1990, 1, 29),33),
				new Estudiante(null, "Luciana", "Mora", 35562256, "lucymora@gmail.com", LocalDate.of(1987, 5, 25), 36)

		));

	}

	private void saveCursos(){
		cursoRepository.saveAll(Arrays.asList(

				new Curso(null, "Aplicaciones Autoconfigurables", "Springboot", LocalDate.of(2023, 6, 13), LocalDate.of(2023, 6, 7)),
				new Curso(null, "Aplicaiones Movil", "Flutter", LocalDate.of(2023, 1, 12), LocalDate.of(2023, 3, 15)),
				new Curso(null, "Front End", "Angular", LocalDate.of(2023, 5, 2), LocalDate.of(2023, 9, 30)),
				new Curso(null, "Seguridad Informatica", "Python", LocalDate.of(2023, 7, 12), LocalDate.of(2023, 9, 13))
		));

	}
	private void saveInscripciones(){


		inscripcionService.Inscripcion(1L, 1L, LocalDate.of(2022, 12, 20), Estado.Pendiente);
		inscripcionService.Inscripcion(4L, 2L, LocalDate.of(2023, 6, 11), Estado.Rechazado);
		inscripcionService.Inscripcion(3L, 3L, LocalDate.of(2023, 4, 1), Estado.Aprobado);
		inscripcionService.Inscripcion(5L, 4L, LocalDate.of(2022, 6, 20), Estado.Aprobado);
	}


	@Bean
	public CommandLineRunner commandLineRunner (ApplicationContext ctx){
		return args -> {
			saveEstudiantes();
			saveCursos();
			saveInscripciones();

			System.out.print("Estudiantes Ordenados y Paginados");
			for (Estudiante aux: estudianteRepository.findAll(PageRequest.of(0, 2, Sort.by(Sort.Direction.DESC, "dni")))
			) {

			}

		};
	}

}
