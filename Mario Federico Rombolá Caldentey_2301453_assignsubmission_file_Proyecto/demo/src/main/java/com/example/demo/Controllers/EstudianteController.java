package com.example.demo.Controllers;

import com.example.demo.DTO.EstudianteDTO;
import com.example.demo.Services.EstudianteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping ("/Estudiante")
public class EstudianteController {

    @Autowired
    private EstudianteService estudianteService;

    @PostMapping
    public EstudianteDTO save(@RequestBody EstudianteDTO estudianteDTO){
        return estudianteService.saveEstudiante(estudianteDTO);
    }

    @GetMapping
    public List<EstudianteDTO> all(){
        return estudianteService.findAll();
    }

    @GetMapping("/{id}")
    public List <EstudianteDTO> find(@PathVariable Long id){
        return estudianteService.findById(id);
    }

    @PutMapping("/{id}")
    public EstudianteDTO update(@PathVariable Long id, @RequestBody EstudianteDTO estudianteDTO){
        return estudianteService.update(id, estudianteDTO);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id){
        estudianteService.delete(id);
    }

}
