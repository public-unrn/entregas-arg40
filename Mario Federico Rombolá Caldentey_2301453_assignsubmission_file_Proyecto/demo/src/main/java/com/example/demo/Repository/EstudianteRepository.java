package com.example.demo.Repository;

import com.example.demo.Domain.Estudiante;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EstudianteRepository extends JpaRepository<Estudiante, Long> {

    //**Consulta todos los estudiantes**//
    @Query("SELECT c FROM Estudiante c")
    List<Estudiante> findAllEstudiantes();

    //**Consulta estudiantes con DNI mayor que y Apellido**//
    @Query ("SELECT c FROM Estudiante c WHERE c.dni > 20000000 AND c.secondName='Romero'")
    List<Estudiante> findByDNIandSecondName();

    //**DERIVADAS**//

    //**Listar todos los estudiantes**//
    List<Estudiante> findAll();


    //**Litar Estudiante por dni mayor que y Apellido**//
    List<Estudiante> findByDniGreaterThanAndSecondNameIs(int dni, String secondName );

    //**Consultas Derivadas Varias**//
    List<Estudiante> findBySecondNameAndFirstName(String secondName, String firstName);

    List<Estudiante> findByDniAndSecondName(int dni, String secondName);

    List<Estudiante> findBySecondNameStartingWith(String prefix);


}
