package com.example.demo.Domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Table(name = "estudiante")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Estudiante {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @Column(name = "nombre")
    private String firstName;

    @Column(name = "apellido")
    private String secondName;

    private int dni;

    private String email;

    @Column(name = "fecha_de_nacimiento")
    private LocalDate fechaDeNaciemiento;

    private int edad;

    public boolean esMayorEdad() {
        return edad >= 18;
    }
}