package com.example.demo.Services;

import com.example.demo.DTO.CursoDTO;
import com.example.demo.Domain.Curso;
import com.example.demo.Repository.CursoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Validated
public class CursoService {

    @Autowired
    private CursoRepository cursoRepository;

public CursoDTO saveCurso(CursoDTO cursoDTO){

    Curso curso = new Curso(
            null,
            cursoDTO.getDescripcion(),
            cursoDTO.getName(),
            cursoDTO.getFechaDeInicio(),
            cursoDTO.getFechaDeFin()
    );

    cursoRepository.save(curso);
    return cursoDTO;

}

    public List<CursoDTO> findAll(){
        return cursoRepository.findAll()
                .stream().map(c -> new CursoDTO(c.getDescription(), c.getName(), c.getFechaDeInicio(), c.getFechaDeFin()))
                .collect(Collectors.toList());
    }

    public List<CursoDTO> findById(Long cursoId){

        return cursoRepository.findById(cursoId)
                .stream().map(c -> new CursoDTO(c.getDescription(), c.getName(), c.getFechaDeInicio(), c.getFechaDeFin()))
                .collect(Collectors.toList());

    }

    public CursoDTO update(Long id, CursoDTO cursoDTO) {
        Curso curso = new Curso(id, cursoDTO.getDescripcion(), cursoDTO.getName(), cursoDTO.getFechaDeInicio(), cursoDTO.getFechaDeFin());

        cursoRepository.save(curso);

        return cursoDTO;

    }

    public void delete(Long Id){
        cursoRepository.deleteById(Id);
    }



}
