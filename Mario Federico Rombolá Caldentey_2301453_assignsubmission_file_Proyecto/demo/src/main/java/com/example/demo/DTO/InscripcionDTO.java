package com.example.demo.DTO;

import com.example.demo.Domain.Estado;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class InscripcionDTO {

    private Long curso_id;
    private Long estudiante_id;
    private LocalDate fechaDeInscripcion;
    private Estado estado;

}
