package com.example.demo.DTO;


import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class EstudianteDTO {

    private String firstName;
    private String secondName;
    private int dni;
    private String email;
    private LocalDate fechaDeNacimiento;
    private int edad;

}
