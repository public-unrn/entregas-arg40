package com.example.demo.Repository;

import com.example.demo.Domain.Estado;
import com.example.demo.Domain.Inscripcion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InscripcionRepository extends JpaRepository<Inscripcion, Long> {

    //**Buscar Inscripcion segun estado**//
    @Query("SELECT c FROM Inscripcion c WHERE c.estado = Rechazado OR c.estado = Pendiente")
    List<Inscripcion> findByInscripcionRechazadasOPendientes();

    //**Buscar todas las inscripciones segun parametro estado**//
    @Query ("SELECT c FROM Inscripcion c WHERE c.estado = estado")
    List<Inscripcion> findByEstado(@Param("estado") String estado);

    //**Buscar todas las inscripciones de forma nativa**//
    @Query (value = "SELECT * FROM Inscripcion WHERE estado = estado", nativeQuery = true)
    List<Inscripcion> findAllByEstadoNativa(String estado);

    //**DERIVADAS**//

    //**Buscar Inscripciones segun estado**//
    List<Inscripcion> findByEstadoIsOrEstadoIs(Estado rechazado, Estado pendiente);

    //**Buscar tpdas las Inscripciones segun parametro estado**//
    List<Inscripcion> findByEstadoIs(Estado estado);

}



