package com.example.demo.Domain;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Table (name = "inscripcion")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Inscripcion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @ManyToOne
    @JoinColumn(name = "curso_id")
    private Curso curso;

    @ManyToOne
    @JoinColumn (name = "estudiante_id")
    private Estudiante estudainte;

    @Column (name = "fecha_de_inscripcion")
    private LocalDate fechaDeInscripcon;

    @Enumerated (EnumType.STRING)
    private Estado estado;


}
