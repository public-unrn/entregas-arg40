package com.example.demo.Controllers;


import com.example.demo.DTO.CursoDTO;
import com.example.demo.Services.CursoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping ("/Curso")
public class CursoController {

    @Autowired
    private CursoService cursoService;

    @PostMapping
    public CursoDTO save(@RequestBody CursoDTO cursoDTO){
        return cursoService.saveCurso(cursoDTO);
    }

    @GetMapping
    public List<CursoDTO> all(){
        return cursoService.findAll();
    }

    @GetMapping("/{id}")
    public List <CursoDTO> find(@PathVariable Long id){
        return cursoService.findById(id);
    }

    @PutMapping("/{id}")
    public CursoDTO update(@PathVariable Long id, @RequestBody CursoDTO cursoDTO){
    return cursoService.update(id, cursoDTO);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id){
        cursoService.delete(id);
    }
}
