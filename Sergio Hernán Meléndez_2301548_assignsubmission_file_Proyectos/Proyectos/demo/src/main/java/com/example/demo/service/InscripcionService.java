package com.example.demo.service;

import com.example.demo.domain.Curso;
import com.example.demo.domain.Estado;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;
import com.example.demo.dto.InscripcionDTO;
import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.repository.InscripcionRepository;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
//TP 4 - Modulo 3
@Service
public class InscripcionService {

    @Autowired
    private CursoRepository cursoRepository;

    @Autowired
    private EstudianteRepository estudianteRepository;

    @Autowired
    private InscripcionRepository inscripcionRepository;

    @Transactional
    public  void insc(LocalDate fechaDeInscripcion, Estado estado, @NotNull  @Positive Long cursoId, @NotNull @Positive Long estudianteId){
    //pasar curso y estudiante como tipo Long
    //

        Curso curso = cursoRepository
                .findById(cursoId)
                .orElseThrow(() -> new RuntimeException("El Id del curso no es valido"));

        Estudiante estudiante = estudianteRepository
                .findById(estudianteId)
                .orElseThrow(() -> new RuntimeException("El Id del estudiante no es valido"));

        Inscripcion inscripcion = new Inscripcion(
                null,
                fechaDeInscripcion,
                estado,
                curso,
                estudiante
        );

        inscripcionRepository.save(inscripcion);

    }
//Fuera de TP 4
//Muestra todas las inscripciones
    public List<InscripcionDTO> findAll() {
        return inscripcionRepository.findAll()
                .stream().map(c -> new InscripcionDTO(c.getFechaDeInscripcion(), c.getEstado(), c.getCurso().getId(), c.getEstudiante().getId()))
                .collect(Collectors.toList());
    }
//elimina una inscripcion
    public void delete(Long id){
        inscripcionRepository.deleteById(id);
    }
}
