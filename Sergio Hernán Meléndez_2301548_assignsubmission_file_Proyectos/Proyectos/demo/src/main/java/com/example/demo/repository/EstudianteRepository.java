package com.example.demo.repository;


import com.example.demo.domain.Estudiante;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
//TP Módulo 2
public interface EstudianteRepository extends JpaRepository<Estudiante, Long> {
    @Query("SELECT c FROM Estudiante c")
    List<Estudiante> findAllEstudiante();
    //Listar todos los estudiantes

    @Query("SELECT c FROM Estudiante c WHERE c.dni > 20000000L AND c.apellido ='Romero'")
    List<Estudiante> findAllDniAndApellido();
    //Listar todos los estudiantes que tengan un dni mayor a 20M y que su apellido sea "Romero"


    @Query("SELECT c FROM Estudiante c ORDER BY c.dni ASC")
    List<Estudiante> findAllPageable(Pageable pageable);
    //Listar todos los estudiantes de forma pagina y ordeanda por DNI


    @Query("SELECT c FROM Estudiante c ORDER BY c.dni ASC LIMIT :size OFFSET :page")
    List<Estudiante> findAllEstudianteOrderAndPage(@Param("size")int size,@Param("page")int page);
    //Para consultar por curiosidad


}