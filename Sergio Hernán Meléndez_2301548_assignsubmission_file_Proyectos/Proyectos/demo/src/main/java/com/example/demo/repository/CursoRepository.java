package com.example.demo.repository;


import com.example.demo.domain.Curso;
import org.springframework.data.annotation.Id;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
//TP Módulo 2
public interface CursoRepository extends JpaRepository<Curso, Long> {

    @Query("SELECT c FROM Curso c")
    List<Curso> findAllCurso();
    //Listar todos los cursos

    @Query("SELECT c FROM Curso c WHERE c.fechaDeInicio > :fechaDeInicio" ) //LocalDate.of(año,mes,día)
    List<Curso>findByFecha(@Param ("fechaDeInicio") LocalDate fechaDeInicio );
    //Listar tods los cursos que hayan empezado después de "01/02/2020"

    List<Curso>findByFechaDeInicioGreaterThan(LocalDate fechaDeInicio);//LocalDate.of(año,mes,día)
    //Consulta derivada

}
