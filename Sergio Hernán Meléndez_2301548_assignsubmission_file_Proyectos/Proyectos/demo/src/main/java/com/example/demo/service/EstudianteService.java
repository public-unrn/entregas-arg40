package com.example.demo.service;

import com.example.demo.domain.Estudiante;
import com.example.demo.dto.EstudianteDTO;
import com.example.demo.repository.EstudianteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class EstudianteService {

    @Autowired
    private EstudianteRepository estudianteRepository;

//TP 4 - Módulo 3
//guarda estudiante
    public EstudianteDTO saveEStudiante(EstudianteDTO estudianteDTO){
        Estudiante estudiante = new Estudiante (
                null,
                estudianteDTO.getNombre(),
                estudianteDTO.getApellido(),
                estudianteDTO.getEmail(),
                estudianteDTO.getDni(),
                estudianteDTO.getFechaDeNacimiento()
        );

        estudianteRepository.save(estudiante);

        return estudianteDTO;
    }
//muestra todos los estudiantes
    public List<EstudianteDTO> findAll() {
        return estudianteRepository.findAll()
                .stream().map(c -> new EstudianteDTO(c.getNombre(), c.getApellido(), c.getEmail(), c.getDni(),c.getFechaDeNacimiento()))
                .collect(Collectors.toList());
    }
//actualiza estudiente
    public EstudianteDTO update(Long id, EstudianteDTO estudianteDTO) {
        Estudiante estudiante = new Estudiante(id,estudianteDTO.getNombre(),estudianteDTO.getApellido(),estudianteDTO.getEmail(),estudianteDTO.getDni(),estudianteDTO.getFechaDeNacimiento());

        estudianteRepository.save(estudiante);

        return estudianteDTO;
    }
//muestra estudiante por id
    public EstudianteDTO find(Long id) {
        Optional<Estudiante> estudianteOptional = estudianteRepository.findById(id);

        if (estudianteOptional.isEmpty()) {
            throw new RuntimeException("Id invalido");
        }

        Estudiante estudiante = estudianteOptional.get();

        return new EstudianteDTO(estudiante.getNombre(),estudiante.getApellido(), estudiante.getEmail(),estudiante.getDni(),estudiante.getFechaDeNacimiento());
    }

//elimina estudiante
    public void delete(Long id){
        estudianteRepository.deleteById(id);
    }

}
