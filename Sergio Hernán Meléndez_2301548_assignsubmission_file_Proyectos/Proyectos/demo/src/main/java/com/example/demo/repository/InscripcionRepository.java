package com.example.demo.repository;

import com.example.demo.domain.Estado;
import com.example.demo.domain.Inscripcion;
import jakarta.persistence.Id;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
//TP Módulo 2
public interface InscripcionRepository extends JpaRepository<Inscripcion, Long> {

    @Query("SELECT c FROM Inscripcion c WHERE c.estado = 1 OR c.estado = 2 ")
    List<Inscripcion> findAllByEstadoOrEstado();
    //Listar todas las inscripciones rechazadas o pendientes

    @Query("SELECT c FROM Inscripcion c WHERE c.estado LIKE :paramEstado")
    List<Inscripcion> findByParam(@Param("paramEstado") Estado paramEstado);
    //Listar todas las inscripciones en base a un parámetro de estado

    @Query(value = "SELECT c FROM Inscripcion c WHERE c.estado LIKE :paramEstado", nativeQuery = true)
    List<Inscripcion> findByParamEstado(@Param("paramEstado") Estado paramEstado);
    //Listar todas las inscripciones en base a un parámetro de estado utilizando consulta nativa


    List<Inscripcion> findByEstado(Estado estado);
    //Consulta derivada

}
