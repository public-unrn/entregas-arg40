package com.example.demo.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;


// Actividad Modelado
@Entity
@Table(name = "estudiante")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Estudiante {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long Id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "apellido")
    private String apellido;

    @Column(name = "email")
    private String email;

    @Column(name = "dni")
    private Long dni;

    @Column(name = "fecha_de_nacimeinto")
    private LocalDate fechaDeNacimiento;

    public Long edad(LocalDate fechaDeNacimiento) {

          return (ChronoUnit.YEARS.between(fechaDeNacimiento, LocalDate.now()));
    }
 }
