package com.example.demo;

import com.example.demo.domain.Curso;
import com.example.demo.domain.Estado;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;
import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.repository.InscripcionRepository;
import com.example.demo.service.InscripcionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.time.LocalDate;
import java.util.Arrays;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) { SpringApplication.run(DemoApplication.class, args);}


	@Autowired
	CursoRepository cursoRepository;

	@Autowired
	EstudianteRepository estudianteRepository;

	@Autowired
	InscripcionRepository inscripcionRepository;

	@Autowired
	InscripcionService inscripcionService;

	private void saveCurso(){
		cursoRepository.saveAll(Arrays.asList(
				new Curso(null,"Java","Medio",LocalDate.of(2023,8,01),LocalDate.of(2023,8,31)),
				new Curso(null,"Java","Basico",LocalDate.of(2023,8,01),LocalDate.of(2023,8,31)),
				new Curso(null,"C#","Basico",LocalDate.of(2023,8,01),LocalDate.of(2023,8,31)),
				new Curso(null,"C#","Avanzado",LocalDate.of(2023,8,01),LocalDate.of(2023,8,31)),
				new Curso(null,"Python","Basico",LocalDate.of(2023,8,01),LocalDate.of(2023,8,31)),
				new Curso(null,"Python","Avanzado",LocalDate.of(2023,8,01),LocalDate.of(2023,8,31))
		));
	}

	private void saveEstudiante(){
		estudianteRepository.saveAll(Arrays.asList(
				new Estudiante(null,"Carlos","Garcia", "JG@gm.com",43111222L,LocalDate.of(2000,01,02)),
				new Estudiante(null,"Marcelo","Romero", "MP@gm.com",45333444L,LocalDate.of(2001,02,03)),
				new Estudiante(null,"Tatiana","Mendoza", "TM@gm.com",46000111L,LocalDate.of(2002,03,04)),
				new Estudiante(null,"Ana","Suarez","AS@gm.com",44008000L,LocalDate.of(2003,04,05)),
				new Estudiante(null,"Sofia","Gonzalez","SG@gm.com",44008010L,LocalDate.of(2004,05,06)),
				new Estudiante(null,"Jorge","Romero","JM@gm.com",2101000L,LocalDate.of(1970,06,07))
		));
	}


	private void saveInscripcion(){
		inscripcionRepository.saveAll(Arrays.asList(
				new Inscripcion(null,LocalDate.of(2023,06,26), Estado.ACEPTADO, cursoRepository.findById(2L).get(),estudianteRepository.findById(3L).get()),
				new Inscripcion(null,LocalDate.of(2023,06,27), Estado.RECHAZADO,cursoRepository.findById(1L).get(),estudianteRepository.findById(2L).get()),
				new Inscripcion(null,LocalDate.of(2023,06,28), Estado.PENDIENTE,cursoRepository.findById(3L).get(),estudianteRepository.findById(1L).get())
		));
	}





	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx ) {
		return args -> {
			saveCurso();
			saveEstudiante();
			saveInscripcion();

           /* inscripcionService.insc(
					"28/06/2023",
					Estado.PENDIENTE,
					cursoRepository.findById(3L).get(),
					estudianteRepository.findById(1L).get()
            );*/




		};
	}
}
