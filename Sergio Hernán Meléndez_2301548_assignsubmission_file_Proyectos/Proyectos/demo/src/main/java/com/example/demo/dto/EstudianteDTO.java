package com.example.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class EstudianteDTO {


    private String nombre;
    private String apellido;
    private String email;
    private Long dni;
    private LocalDate fechaDeNacimiento;

}
