package com.example.demo.service;

import com.example.demo.domain.Curso;
import com.example.demo.dto.CursoDTO;
import com.example.demo.repository.CursoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CursoService {

    @Autowired
    private CursoRepository cursoRepository;
//TP Módulo 3
    public CursoDTO saveCurso(CursoDTO cursoDTO){
        Curso curso = new Curso (
            null,
            cursoDTO.getNombre(),
            cursoDTO.getDescripcion(),
            cursoDTO.getFechaDeInicio(),
            cursoDTO.getFechaDeFin()
        );

        cursoRepository.save(curso);

        return cursoDTO;
    }
//-----------------------------------------------------------------
//Fuera de TP
    public List<CursoDTO> findAll() {
        return cursoRepository.findAll()
                .stream().map(c -> new CursoDTO( c.getNombre(), c.getDescripcion(), c.getFechaDeInicio(), c.getFechaDeFin()))
                .collect(Collectors.toList());
    }

    public CursoDTO update(Long id, CursoDTO cursoDTO) {
        Curso curso = new Curso(id,cursoDTO.getNombre(),cursoDTO.getDescripcion(),cursoDTO.getFechaDeInicio(),cursoDTO.getFechaDeFin());

        cursoRepository.save(curso);

        return cursoDTO;
    }

    public CursoDTO find(Long id) {
        Optional<Curso> cursoOptional = cursoRepository.findById(id);

        if (cursoOptional.isEmpty()) {
            throw new RuntimeException("Id invalido");
        }

        Curso curso = cursoOptional.get();

        return new CursoDTO(curso.getNombre(),curso.getDescripcion(), curso.getFechaDeInicio(),curso.getFechaDeFin());
    }


    public void delete(Long id){
        cursoRepository.deleteById(id);
    }

}
