package com.example.demo.controller;

import com.example.demo.dto.CursoDTO;
import com.example.demo.dto.InscripcionDTO;
import com.example.demo.service.InscripcionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/controller")
public class InscripcionController {

    @Autowired
    private InscripcionService inscripcionService;
//TP Módulo3
    @PostMapping
    public void save(@RequestBody InscripcionDTO inscripcionDTO) {
        inscripcionService.insc(inscripcionDTO.getFechaDeInscripcion(),inscripcionDTO.getEstado(),inscripcionDTO.getCurso(),inscripcionDTO.getEstudiante());
    }
//------------------------------

//Fuera de TP

    @GetMapping
    public List<InscripcionDTO> all() {
        return inscripcionService.findAll();
    }


    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id){
    inscripcionService.delete(id);
}

}
