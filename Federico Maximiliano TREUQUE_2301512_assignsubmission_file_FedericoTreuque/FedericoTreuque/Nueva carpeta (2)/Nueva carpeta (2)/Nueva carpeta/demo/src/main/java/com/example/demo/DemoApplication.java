package com.example.demo;

import com.example.demo.Service.CursoService;
import com.example.demo.Service.InscripcionService;
import com.example.demo.domain.Curso;
import com.example.demo.domain.Estado;
import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.repository.InscripcionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import com.example.demo.domain.Estudiante;


import java.time.LocalDate;
import java.util.Arrays;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Autowired
	EstudianteRepository estudianteRepository;

	@Autowired
	CursoRepository cursorepository;


	@Autowired
	InscripcionService inscripcionService;
	public void saveEstudiante() {
		estudianteRepository.saveAll(Arrays.asList(
				new Estudiante(null, "Ale", "Huaiquilican", "ale@gmail.com", 28790358,LocalDate.of(1998,2,10), 25),
				new Estudiante(null, "Maxi", "Seguel", "maxi@gmail.com", 38790658,LocalDate.of(1988,6,20), 35),
				new Estudiante(null, "Pablo", "Romero", "pablo@gmail.com", 40790639,LocalDate.of(1993,7,2), 30)
		));
	}
	public void saveCurso() {
		cursorepository.saveAll(Arrays.asList(
				new Curso(null, "Matematica",  LocalDate.of(2023,03,15), LocalDate.of(2024,12,10)) ,
				new Curso(null, "Lengua",  LocalDate.of(2023,03,10), LocalDate.of(2023,06,15)),
				new Curso(null, "Quimica", LocalDate.of(2023,03,20), LocalDate.of(2023,12,20))
		));
	}


	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx){
		return args -> {
			saveEstudiante();
			saveCurso();

			inscripcionService.Anotar(LocalDate.of(2020,12,1),
					Estado.Aceptado,
					1L,
					1L
			);
			inscripcionService.Anotar(LocalDate.of(2020,12,1),
					Estado.Pendiente,
					2L,
					2L
			);
			inscripcionService.Anotar(LocalDate.of(2020,12,1),
					Estado.Rechazado,
					3L,
					3L
			);


			estudianteRepository.findAll();
		};
	}
}
