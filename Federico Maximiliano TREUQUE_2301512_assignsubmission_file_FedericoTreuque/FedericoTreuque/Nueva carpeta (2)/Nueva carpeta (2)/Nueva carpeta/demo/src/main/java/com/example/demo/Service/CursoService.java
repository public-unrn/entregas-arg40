package com.example.demo.Service;
import com.example.demo.domain.Curso;
import com.example.demo.domain.Estado;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;
import com.example.demo.dto.CursoDTO;
import com.example.demo.dto.EstudianteDTO;
import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.repository.InscripcionRepository;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Validated


public class CursoService {
    @Autowired
    CursoRepository cursorepository;


    public CursoDTO saveCurso(CursoDTO cursoDTO){
        Curso curso=new Curso(null,
                cursoDTO.getDescripcion(),
                cursoDTO.getFechaini(),
                cursoDTO.getFechafin()
        );
        cursorepository.save(curso);

        return  cursoDTO;

    }
   public List<CursoDTO> findAll(){
      return cursorepository.findAll()
                .stream().map(c -> new CursoDTO(c.getDescripcion(),c.getFechaini(),c.getFechafin()))
                .collect(Collectors.toList());
    }

}
