package com.example.demo.Service;

import com.example.demo.domain.Estudiante;
import com.example.demo.dto.EstudianteDTO;
import com.example.demo.repository.EstudianteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class EstudianteService {
    @Autowired
    private EstudianteRepository estudianteRepository;

    public EstudianteDTO saveEstudiante(EstudianteDTO estudianteDTO){
        Estudiante estudiante=new Estudiante(null,
                estudianteDTO.getFirstName(),
                estudianteDTO.getLastName(),
                estudianteDTO.getEmail(),
                estudianteDTO.getDni(),
                estudianteDTO.getNacimiento(),
                estudianteDTO.getAge()
        );
        estudianteRepository.save(estudiante);

        return  estudianteDTO;

    }
    public List<EstudianteDTO> findAll(){
        return estudianteRepository.findAll()
                .stream().map(c -> new EstudianteDTO(c.getFirstName(),c.getLastName(),c.getEmail(),c.getDni(),c.getBirthDate(), c.getAge()))
                .collect(Collectors.toList());
    }
    public EstudianteDTO update(Long id, EstudianteDTO estudianteDTO){
        Estudiante estudiante=new Estudiante(id,estudianteDTO.getFirstName(),estudianteDTO.getLastName(),estudianteDTO.getEmail(),estudianteDTO.getDni(),estudianteDTO.getNacimiento(),estudianteDTO.getAge());

        estudianteRepository.save(estudiante);
        return estudianteDTO;
    }
    public EstudianteDTO find(Long id){
        Optional<Estudiante> estudianteOptional = estudianteRepository.findById(id);

        if (estudianteOptional.isEmpty()){
            throw new RuntimeException("id invalido");
        }
        Estudiante estudiante=estudianteOptional.get();

        return new EstudianteDTO(estudiante.getFirstName(),estudiante.getLastName(),estudiante.getEmail(),estudiante.getDni(),estudiante.getBirthDate(),estudiante.getAge());
    }
    public void delete(Long id){estudianteRepository.deleteById(id);}
}
