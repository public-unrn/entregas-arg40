package com.example.demo.repository;
import com.example.demo.domain.Curso;
import com.example.demo.domain.Estudiante;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EstudianteRepository extends JpaRepository<Estudiante, Long> {
    @Query("SELECT c From Estudiante c")
    List<Estudiante> findAllEstudiantes();

    @Query("SELECT c From Estudiante c WHERE c.dni > 20000000 OR c.lastName = 'Romero'")
    List<Estudiante> findBydniorlast();

    ///consultasderivadas
   List<Estudiante> findAll();
    List<Estudiante> findByDniGreaterThanAndLastNameIs(int dni,String lastname);





    @Query("SELECT c From Estudiante c ORDER BY c.dni ASC")
    List<Estudiante> findByOrder();
}
