package com.example.demo.repository;
import com.example.demo.domain.Estado;
import com.example.demo.domain.Inscripcion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface InscripcionRepository extends JpaRepository<Inscripcion, Long>{
    @Query("SELECT c From Inscripcion c Where c.estado='Pendiente' OR c.estado='Rechazado'")
    List<Inscripcion> findAllPendienteOrRecahzada();

    @Query("SELECT c From Inscripcion c Where c.estado= :estado")
    List<Inscripcion> findByEstado(@Param("estado") Estado estado);

    @Query(value = "SELECT * From Inscripcion c Where c.estado= :estado",nativeQuery = true)
    List<Inscripcion> findByEstadoNat(@Param("estado") Estado estado);


    //////CONSULTAS DERIVADAS////////////
    List<Inscripcion> findByEstadoIs(Estado estado);
    List<Inscripcion> findByEstadoContaining(Estado estado);

    List<Inscripcion> findByEstadoIn(List<Estado> estados);

    List<Inscripcion> findByEstadoIsOrEstadoIs(Estado pend,Estado recha);






}
