package com.example.demo.service;


import com.example.demo.domain.Car;
import com.example.demo.domain.Customer;
import com.example.demo.domain.Rental;
import com.example.demo.repository.CarRepository;
import com.example.demo.repository.CustomerRepository;
import com.example.demo.repository.RentalRepository;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import java.time.LocalDate;

@Service
@Validated
public class RentalService {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private RentalRepository rentalRepository;

    @Transactional
    public void rent(@NotNull @Positive Long customerId, @NotNull @Positive Long carId, LocalDate rentStartDate, LocalDate endEndDate) {

        if (rentStartDate.isAfter(endEndDate)) {
            throw new RuntimeException("Las fechas ingresadas no son validas");
        }

        Customer customer = customerRepository
            .findById(customerId)
            .orElseThrow(() -> new RuntimeException("El id del customer no es valido"));

        if (!customer.esMayorEdad()) {
            throw new RuntimeException("El customer es menor de edad...");
        }

        Car car = carRepository
            .findById(carId)
            .orElseThrow(() -> new RuntimeException("El id del car no es valido"));

        Rental rental = new Rental(
            null,
            rentStartDate,
            endEndDate,
            customer,
            car
        );

        rentalRepository.save(rental);
    }
}
