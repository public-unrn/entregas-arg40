package com.example.demo.service;

import com.example.demo.domain.Customer;
import com.example.demo.dto.CustomerDTO;
import com.example.demo.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;


    public CustomerDTO saveCustomer(CustomerDTO customerDTO){
        Customer customer = new Customer(
            null,
            customerDTO.getFirstName(),
            customerDTO.getLastName(),
            customerDTO.getEmail(),
            customerDTO.getPhoneNumber(),
            customerDTO.getAge()
        );

        customerRepository.save(customer);

        return customerDTO;
    }



    public List<CustomerDTO> findAll() {
        return customerRepository.findAll()
            .stream().map(c -> new CustomerDTO(c.getFirstName(),c.getLastName() , c.getEmail(), c.getPhoneNumber(), c.getAge()))
            .collect(Collectors.toList());
    }

    public CustomerDTO update(Long id, CustomerDTO customerDTO) {
        Customer customer = new Customer(id,customerDTO.getFirstName(),customerDTO.getLastName() , customerDTO.getEmail(), customerDTO.getPhoneNumber(), customerDTO.getAge());

        customerRepository.save(customer);

        return customerDTO;
    }

    public CustomerDTO find(Long id) {
        Optional<Customer> customerOptional = customerRepository.findById(id);

        if (customerOptional.isEmpty()) {
            throw new RuntimeException("Id invalido");
        }

        Customer customer = customerOptional.get();

        return new CustomerDTO(customer.getFirstName(),customer.getLastName() , customer.getEmail(), customer.getPhoneNumber(), customer.getAge());
    }

    public void delete(Long id){
        customerRepository.deleteById(id);
    }
}
