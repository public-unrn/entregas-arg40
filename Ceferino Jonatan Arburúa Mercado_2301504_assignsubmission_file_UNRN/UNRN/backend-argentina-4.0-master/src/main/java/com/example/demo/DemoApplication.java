package com.example.demo;

import com.example.demo.domain.Car;
import com.example.demo.domain.Customer;
import com.example.demo.repository.CarRepository;
import com.example.demo.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    CarRepository carRepository;

    private void saveCustomers() {
        customerRepository.saveAll(Arrays.asList(
            new Customer(null, "Ale", "Huaiquilican", "ahuaquilican@email.com", "111", 25),
            new Customer(null, "Jose", "Luis", "jl@email.com", "222", 35),
            new Customer(null, "Mauro", "Juan", "mj@email.com", "333", 20),
            new Customer(null, "Horacio", "Juan", "hj@email.com", "444", 15)
        ));
    }

    private void saveCars() {
        carRepository.saveAll(Arrays.asList(
            new Car(null, "ford", "focus", "rojo", 2000),
            new Car(null, "toyota", "hilux", "negro", 2010),
            new Car(null, "chevrolet", "cruze", "blanco", 2015)
        ));
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {
            saveCustomers();
            saveCars();
        };
    }
}
