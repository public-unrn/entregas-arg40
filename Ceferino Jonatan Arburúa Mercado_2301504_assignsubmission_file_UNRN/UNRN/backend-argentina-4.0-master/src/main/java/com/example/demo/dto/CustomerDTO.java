package com.example.demo.dto;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CustomerDTO {

    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private int age;
}
