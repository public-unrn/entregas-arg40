package com.example.demo.repository;

import com.example.demo.domain.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CustomerRepository extends JpaRepository<Customer, Long> {


    List<Customer> findByLastNameAndFirstName(String lastName, String firstName);
    List<Customer> findByLastNameStartingWith(String prefix);
    boolean existsByFirstName(String firstName);
    long countByFirstName(String firstName);

}
