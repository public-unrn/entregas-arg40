package com.example.demo.repository;

import com.example.demo.domain.Rental;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RentalRepository extends JpaRepository<Rental, Long > {


    @Query(value = "SELECT distinct r.* from rental as r " +
        "inner join customer as cu on cu.id=r.customer_id " +
        "inner join car as ca on ca.id=r.car_id " ,
        nativeQuery = true
    )
    List<Rental> findAllRental();
}
