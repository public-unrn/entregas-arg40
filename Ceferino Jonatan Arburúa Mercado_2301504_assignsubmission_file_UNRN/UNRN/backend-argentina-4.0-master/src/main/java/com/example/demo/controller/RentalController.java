package com.example.demo.controller;

import com.example.demo.dto.RentalDTO;
import com.example.demo.service.RentalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rental")
public class RentalController {

    @Autowired
    private RentalService rentalService;


    @PostMapping
    public void save(@RequestBody RentalDTO rentalDTO) {
        rentalService.rent(rentalDTO.getCustomer(), rentalDTO.getCar(), rentalDTO.getRentStartDate(), rentalDTO.getEndEndDate());
    }

}
