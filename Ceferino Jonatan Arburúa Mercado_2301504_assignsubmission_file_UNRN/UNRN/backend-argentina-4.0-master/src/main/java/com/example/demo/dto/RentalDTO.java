package com.example.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class RentalDTO {

    private LocalDate rentStartDate;
    private LocalDate endEndDate;
    private Long customer;
    private Long car;
}
