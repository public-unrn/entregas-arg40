package com.example.demo.Controller;

import com.example.demo.DTO.EstudianteDTO;
import com.example.demo.DTO.InscripcionDTO;
import com.example.demo.domain.Curso;
import com.example.demo.services.InscripcionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/Inscripcion")
public class InscripcionController {

    @Autowired
    private InscripcionService inscripcionService;

    @PostMapping
    public void save (@RequestBody InscripcionDTO inscripcionDTO){
        inscripcionService.save(
                inscripcionDTO.getEstudiante(),
                inscripcionDTO.getCurso(),
                inscripcionDTO.getFechaInscripcion(),
                inscripcionDTO.getEstado()
        );
    }
}
