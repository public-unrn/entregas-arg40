package com.example.demo.repository;

import com.example.demo.domain.Estado;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;
import lombok.Value;
import org.hibernate.query.NativeQuery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.management.ValueExp;
import java.util.List;

public interface InscripcionRepository extends JpaRepository<Inscripcion, Long> {
    /**Listar por ESTADO con consulta derivada**/
     List<Inscripcion> findByEstado(Estado estado);

     /** Listar por Estado **/
     @Query("SELECT i FROM Inscripcion i WHERE i.estado = :estado")
     List<Estudiante> findByEstado(String estado);

     /** Lista todas las inscripciones con ESTADO RECHAZADO O PENDIENTE de manera NATIVA**/
     @Query(value = "SELECT *\n" +
             "FROM inscripcion AS i\n" +
             "WHERE i.estado = 'RECHAZADO' OR i.estado = 'PENDIENTE'\n" +
             "LIMIT 100;", nativeQuery = true)
    List<Inscripcion> findByRechazadoOrPendiente();

     /** Listar inscripciones con parametro de manera NATIVA **/
     @Query(value =
             "SELECT * " +
             "FROM inscripcion as i " +
             "WHERE i.estado = :estado ", nativeQuery = true)
     List<Inscripcion> findByNativeQuery(@Param("estado") Estado estado);

     /** Listar Inscripciones y traer los nombres en vez de ID **/
     @Query(value =
             "SELECT distinct inscripcion.id_inscripcion, inscripcion.fecha_inscripcion, " +
                     "curso.nombre as Materia, inscripcion.estado, curso.fecha_inicio, " +
                     " curso.fecha_fin, estudiante.nombre as Nombre_Estudiante, estudiante.dni, " +
                     " estudiante.fecha_nac, estudiante.apellido, estudiante.email " +
             "FROM   inscripcion " +
                        "inner join curso on curso.id_curso=inscripcion.id_curso " +
                        "inner join estudiante on estudiante.id_estudiante=inscripcion.id_estudiante " +
             "ORDER BY inscripcion.id_inscripcion desc", nativeQuery = true)
    List<Inscripcion> findByMultiTable();


}
