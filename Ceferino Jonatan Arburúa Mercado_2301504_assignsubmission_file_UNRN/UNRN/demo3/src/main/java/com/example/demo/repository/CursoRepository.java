package com.example.demo.repository;

import com.example.demo.domain.Curso;
import org.springframework.cglib.core.Local;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface CursoRepository extends JpaRepository<Curso , Long> {
    @Query ("SELECT c FROM Curso c")
    List<Curso> findAllCurso();

    /** CONSULTA DE TODAS LAS TABLAS DE curso CON LA CONDICIÓN DE QUE SEAN MAYOR A LA  FECHA INDICADA
     usar cursoRepository.findCursoByDate(LocalDate.of(aaaa,mm,dd))
     **/
    @Query ("SELECT c FROM Curso c WHERE c.fechaInicio > :fechaInicio ")
    List<Curso> findCursoByDate(@Param("fechaInicio") LocalDate fechaInicio);

    List<Curso> findAll();

    List<Curso> findByfechaInicio(LocalDate fechaInicio);

    @Query (value = "DELETE c FROM Curso c WHERE id = :id ", nativeQuery = true)
    List<Curso> deleteById(@Param("id")long id);

}
