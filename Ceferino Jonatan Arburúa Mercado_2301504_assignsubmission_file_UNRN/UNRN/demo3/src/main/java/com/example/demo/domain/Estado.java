package com.example.demo.domain;

import jakarta.persistence.Column;

public enum Estado {

    ACEPTADO,
    RECHAZADO,
    PENDIENTE
}
