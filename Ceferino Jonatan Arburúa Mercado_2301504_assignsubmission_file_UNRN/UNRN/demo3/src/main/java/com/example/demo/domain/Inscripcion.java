package com.example.demo.domain;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;

@Entity
@Table (name = "inscripcion")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class Inscripcion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idInscripcion;

    @Column(name = "fecha_inscripcion")
    private LocalDate fechaInscripcion;

    @Column(name = "estado")
    @Enumerated(EnumType.STRING)
    private Estado estado;

    @ManyToOne
    @JoinColumn(name = "Id_curso")
    private Curso curso;

    @ManyToOne
    @JoinColumn(name = "Id_estudiante")
    private Estudiante estudiante;
}
