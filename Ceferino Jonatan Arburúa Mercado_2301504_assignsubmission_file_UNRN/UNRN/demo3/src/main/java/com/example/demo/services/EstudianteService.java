package com.example.demo.services;

import com.example.demo.DTO.CursoDTO;
import com.example.demo.DTO.EstudianteDTO;
import com.example.demo.domain.Curso;
import com.example.demo.domain.Estudiante;
import com.example.demo.repository.EstudianteRepository;
import jakarta.persistence.Column;
import jakarta.persistence.Transient;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Validated
public class EstudianteService {
    @Autowired
    private EstudianteRepository estudianteRepository;

    @Transactional
    public EstudianteDTO saveEstudiante(EstudianteDTO estudianteDTO) {
        Estudiante estudiante = new Estudiante(
                null,
                estudianteDTO.getNombre(),
                estudianteDTO.getApellido(),
                estudianteDTO.getEmail(),
                estudianteDTO.getDNI(),
                estudianteDTO.getFechaNac(),
                estudianteDTO.getEdad()
        );
        estudianteRepository.save(estudiante);
        return estudianteDTO;
    }

    public List<EstudianteDTO> findAll() {
        return estudianteRepository.findAll()
                .stream().map(e -> new EstudianteDTO(e.getNombre(), e.getApellido(), e.getEmail(), e.getDNI(), e.getFechaNac(), e.getEdad()))
                .collect(Collectors.toList());
    }

    public EstudianteDTO find(Long id) {
        Optional<Estudiante> estudianteOptional = estudianteRepository.findById(id);

        if (estudianteOptional.isEmpty()) {
            throw new RuntimeException("ID invalido");
        }

        Estudiante estudiante = estudianteOptional.get();

        return new EstudianteDTO(estudiante.getNombre(), estudiante.getApellido(), estudiante.getEmail(), estudiante.getDNI(), estudiante.getFechaNac(), estudiante.getEdad());

    }

    public EstudianteDTO update(Long id, EstudianteDTO estudianteDTO){
        Estudiante estudiante = new Estudiante(id, estudianteDTO.getNombre(), estudianteDTO.getApellido(), estudianteDTO.getEmail(), estudianteDTO.getDNI(), estudianteDTO.getFechaNac(), estudianteDTO.getEdad());
        estudianteRepository.save(estudiante);
        return estudianteDTO;
    }

    public void deleteById(Long id){
        estudianteRepository.deleteById(id);
    }

}
