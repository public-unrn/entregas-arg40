package com.example.demo.services;

import com.example.demo.domain.Curso;
import com.example.demo.domain.Estado;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;
import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.repository.InscripcionRepository;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.time.LocalDate;
import java.time.Period;
import java.util.Objects;
import java.util.Optional;

@Service
@Validated
public class InscripcionService {
    @Autowired
    private EstudianteRepository estudianteRepository;

    @Autowired
    private CursoRepository cursoRepository;

    @Autowired
    private InscripcionRepository inscripcionRepository;

    @Transactional
    public void save(@NotNull @Positive(message = "Se necesita un ID") Long idEstudiante,
                              @NotNull @Positive (message =  "Se de necesita un ID")Long idCurso,
                              LocalDate fechaInscripcion, Estado estado){


        Curso curso = cursoRepository
                .findById(idCurso)
                .orElseThrow(()-> new RuntimeException("ID no valido"));


        Estudiante estudiante = estudianteRepository
                .findById(idEstudiante)
                .orElseThrow( () -> new RuntimeException("El id del estudiante no es validad"));

        if(!estudiante.esMayorEdad()){
            throw new RuntimeException("El estudiante es menor de edad");
        }

        Inscripcion inscripcion = new Inscripcion(
                null,
                fechaInscripcion,
                estado,
                curso,
                estudiante
        );

        inscripcionRepository.save(inscripcion);
    }

}
