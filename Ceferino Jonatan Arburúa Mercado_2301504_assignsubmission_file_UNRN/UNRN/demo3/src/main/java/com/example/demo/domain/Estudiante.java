package com.example.demo.domain;

import com.example.demo.Controller.InscripcionController;
import com.example.demo.services.InscripcionService;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.time.Period;

@Entity
@Table (name = "estudiante")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Estudiante {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idEstudiante;

    @Column(name="nombre")
   private String nombre;

    @Column(name="apellido")
   private String apellido;

    @Column(name="email")
   private String email;

    @Column(name = "DNI")
   private int DNI;

    @Column(name = "fecha_Nac")
   private LocalDate fechaNac;

    @Transient
   private int edad;

    public boolean esMayorEdad(){
        return true;
    }
   }

