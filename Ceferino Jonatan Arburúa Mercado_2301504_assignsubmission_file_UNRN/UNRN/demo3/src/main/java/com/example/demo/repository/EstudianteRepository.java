package com.example.demo.repository;

import com.example.demo.domain.Estudiante;
import lombok.Value;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EstudianteRepository extends JpaRepository <Estudiante, Long> {
    @Query("SELECT e FROM Estudiante e")
    List<Estudiante> findByAllEstudianteQuery();

    @Query("SELECT e FROM Estudiante e WHERE e.DNI > 20000000 AND e.apellido = 'romero'")
    List<Estudiante> findAllEstudianteForDNITwelveMillon();


    @Query(value = "SELECT date_part('year', age(fecha_nac)) AS edad FROM estudiante WHERE id = :id;", nativeQuery = true)
    List<Estudiante> findEdad(@Param("id") Long id);

    /** CONSULTA DERIVADAS **/

    List<Estudiante> findAll();

    List<Estudiante> findByDNIGreaterThanAndApellido(int DNI, String apellido);



    @Query(value = "SELECT * FROM Estudiante\n" +
            "ORDER BY DNI ASC\n" +
            "LIMIT 5 OFFSET 0;", nativeQuery = true)
    List<Estudiante> findAllPagLim5();

    @Query(value = "SELECT * FROM Estudiante " +
            "ORDER BY DNI ASC " +
            "LIMIT 2 OFFSET 0", nativeQuery = true)
    List<Estudiante> findAllPageLim2();
}
