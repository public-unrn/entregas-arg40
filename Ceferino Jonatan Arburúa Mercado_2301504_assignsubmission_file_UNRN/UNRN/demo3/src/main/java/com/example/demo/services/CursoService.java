package com.example.demo.services;

import com.example.demo.DTO.CursoDTO;
import com.example.demo.domain.Curso;
import com.example.demo.repository.CursoRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Validated
public class CursoService {
    @Autowired
    private CursoRepository cursoRepository;

    @Transactional
    public CursoDTO saveCurso(CursoDTO cursoDTO) {
        Curso curso = new Curso(
        null,
        cursoDTO.getNombre(),
        cursoDTO.getDescripcion(),
        cursoDTO.getFechaInicio(),
        cursoDTO.getFechaFin()
        );
        cursoRepository.save(curso);
        return cursoDTO;
    }
    public List<CursoDTO> findAll() {
        return cursoRepository.findAllCurso()
                .stream().map(c -> new CursoDTO(c.getNombre(), c.getDescripcion(),c.getFechaInicio(),c.getFechaFin()))
                .collect(Collectors.toList());
    }

    public CursoDTO update(long id, CursoDTO cursoDTO){
        Curso curso = new Curso(id, cursoDTO.getNombre(), cursoDTO.getDescripcion(),cursoDTO.getFechaInicio(),cursoDTO.getFechaFin());
        cursoRepository.save(curso);
        return cursoDTO;
    }

    public CursoDTO find(long id){
        Optional<Curso> cursoOptional = cursoRepository.findById(id);

        if (cursoOptional.isEmpty()) {
            throw new RuntimeException("ID invalido");
        }

        Curso curso = cursoOptional.get();

        return new CursoDTO(curso.getNombre(), curso.getDescripcion(), curso.getFechaInicio(), curso.getFechaFin());
    }

    public void deleteById(long id){
        cursoRepository.deleteById(id);
    }
}
