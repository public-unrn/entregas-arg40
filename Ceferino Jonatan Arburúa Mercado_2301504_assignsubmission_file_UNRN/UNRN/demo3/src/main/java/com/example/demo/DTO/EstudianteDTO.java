package com.example.demo.DTO;

import com.example.demo.services.CursoService;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.time.LocalDate;
@Data
@AllArgsConstructor
public class EstudianteDTO {
        private String nombre;
        private String apellido;
        private String email;
        private int DNI;
        private LocalDate fechaNac;
        private int edad;

    }
