package com.example.demo.repository;

import com.example.demo.domain.Estado;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InscripcionRepository extends JpaRepository<Inscripcion, Long> {

    //Listar todas las inscripciones rechazadas o pendiente
    @Query("SELECT i FROM Inscripcion i WHERE i.estado = Estado.PENDIENTE OR i.estado = Estado.RECHAZADO")
    List<Inscripcion> findAllInscripcionesPendientesOrRechazadas();

    //Listar todas las inscripciones en base a un parámetro de estado
    @Query("SELECT i FROM Inscripcion i WHERE  i.estado = :estado")
    List<Inscripcion> findAllInscrpcioneswith(@Param("estado")Estado estado);

    //Listar todas las inscripciones en base a un parámetro de estado utilizando consulta
    //nativa
    @Query(value = "SELECT * FROM inscripcion i WHERE  i.estado = :estado", nativeQuery = true)
    List<Inscripcion> buscarPorEstado(@Param("estado")Integer estado);

    //Consulta derivada
    List<Inscripcion> findByEstadoEquals(Estado estado);


}
