package com.example.demo.controller;

import com.example.demo.dto.EstudianteDTO;
import com.example.demo.service.EstudianteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/estudiante")
public class EstudianteController {

    @Autowired
    private EstudianteService estudianteService;
    @PostMapping("post")
    public EstudianteDTO saveEstudiante(@RequestBody EstudianteDTO estudianteDTO){
        return estudianteService.saveEstudiante(estudianteDTO);
    }

    @GetMapping("get")
    public List<EstudianteDTO> findAllEstudiantes(){
        return estudianteService.findAll();
    }

    @GetMapping("get/{estudianteId}")
    public EstudianteDTO findById(@PathVariable Long estudianteId){
        return estudianteService.findById(estudianteId);
    }

    @PutMapping("put/{estudianteId}")
    public EstudianteDTO updateEstudiante(@PathVariable Long estudianteId, @RequestBody EstudianteDTO estudianteDTO){
        return estudianteService.updateEstudiante(estudianteId,estudianteDTO);
    }

    @DeleteMapping("delete/{estudianteId}")
    public void deleteEstudiante(@PathVariable Long estudianteId){
        estudianteService.deleteEstudiante(estudianteId);
    }

}
