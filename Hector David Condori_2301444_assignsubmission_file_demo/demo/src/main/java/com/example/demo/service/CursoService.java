package com.example.demo.service;

import com.example.demo.domain.Curso;
import com.example.demo.dto.CursoDTO;
import com.example.demo.repository.CursoRespository;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

@Service
@Validated
public class CursoService {
    @Autowired
    CursoRespository cursoRespository;
    @Transactional
    public CursoDTO createCurso(@NotNull CursoDTO cursoDTO){

        Curso nuevoCurso = new Curso(
            null,
            cursoDTO.getDescripcion(),
            cursoDTO.getFechaDeInicio(),
            cursoDTO.getFechaDeFin(),
            cursoDTO.getNombre()
        );

        cursoRespository.save(nuevoCurso);

        return cursoDTO;

    }

}
