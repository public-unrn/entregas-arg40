package com.example.demo.controller;

import com.example.demo.dto.InscripcionDTO;
import com.example.demo.service.InscripcionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/inscripcion")
public class InscripcionController {
    @Autowired
    private InscripcionService inscripcionService;

    @PostMapping("post")
    public InscripcionDTO createInscripcion(@RequestBody InscripcionDTO nuevaInscripcion){
        return inscripcionService.inscribir(nuevaInscripcion);
    }

}
