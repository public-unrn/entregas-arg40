package com.example.demo.service;

import com.example.demo.domain.Curso;
import com.example.demo.domain.Estado;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;
import com.example.demo.dto.CursoDTO;
import com.example.demo.dto.EstudianteDTO;
import com.example.demo.dto.InscripcionDTO;
import com.example.demo.repository.CursoRespository;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.repository.InscripcionRepository;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.time.LocalDate;

@Service
@Validated
public class InscripcionService {

    @Autowired
    CursoRespository cursoRespository;

    @Autowired
    EstudianteRepository estudianteRepository;

    @Autowired
    InscripcionRepository inscripcionRepository;

    @Transactional
    public InscripcionDTO inscribir(@NotNull InscripcionDTO nuevaInscripcion) {

        Curso curso = cursoRespository.findById(nuevaInscripcion.getCurso()).orElseThrow(() -> new RuntimeException("Curso no encontrado"));

        Estudiante estudiante = estudianteRepository.findById(nuevaInscripcion.getEstudiante()).orElseThrow(() -> new RuntimeException("Estudidiante no encontrado"));

        if (!estudiante.esMayorDeEdad()) {
            throw new RuntimeException("No es mayor de edad");
        }

        Inscripcion inscripcion = new Inscripcion(
                null,
                Estado.PENDIENTE,
                nuevaInscripcion.getFechaDeInscripcion(),
                curso,
                estudiante
        );

        inscripcionRepository.save(inscripcion);

        return nuevaInscripcion;

    }
}
