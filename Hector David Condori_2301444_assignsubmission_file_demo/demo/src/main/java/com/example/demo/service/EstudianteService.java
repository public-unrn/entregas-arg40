package com.example.demo.service;

import com.example.demo.domain.Estudiante;
import com.example.demo.dto.EstudianteDTO;
import com.example.demo.repository.EstudianteRepository;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Validated
public class EstudianteService {

    @Autowired
    EstudianteRepository estudianteRepository;
    @Transactional
    public List<EstudianteDTO> findAll(){
        List<EstudianteDTO> estudiantes = estudianteRepository.findAllEstudiantes()
                .stream()
                .map(e -> new EstudianteDTO(e.getDni(),e.getEmail(),e.getFechaDeNacimiento(),e.getApellido(), e.getNombre(), e.getEdad()))
                .collect(Collectors.toList());
        return estudiantes;
    }
    @Transactional
    public EstudianteDTO saveEstudiante(@NotNull EstudianteDTO estudianteDTO){

        Estudiante nuevoEstudiante = new Estudiante(
                null,
                estudianteDTO.getDni(),
                estudianteDTO.getEmail(),
                estudianteDTO.getFechaDeNacimiento(),
                estudianteDTO.getApellido(),
                estudianteDTO.getNombre(),
                estudianteDTO.getEdad()
        );

        estudianteRepository.save(nuevoEstudiante);

        return estudianteDTO;
    }
    @Transactional
    public  EstudianteDTO findById(@NotNull @Positive Long estudianteId){

        Estudiante estudiante = estudianteRepository.findById(estudianteId).orElseThrow(() -> new RuntimeException("Estudidiante no encontrado"));

        EstudianteDTO estudianteDTO = new EstudianteDTO(
                estudiante.getDni(),
                estudiante.getEmail(),
                estudiante.getFechaDeNacimiento(),
                estudiante.getApellido(),
                estudiante.getNombre(),
                estudiante.getEdad()
        );

        return estudianteDTO;

    }
    @Transactional
    public EstudianteDTO updateEstudiante(@NotNull @Positive Long estudianteId,@NotNull EstudianteDTO estudianteDTO){

        Estudiante estudiante = new Estudiante(
                estudianteId,
                estudianteDTO.getDni(),
                estudianteDTO.getEmail(),
                estudianteDTO.getFechaDeNacimiento(),
                estudianteDTO.getApellido(),
                estudianteDTO.getNombre(),
                estudianteDTO.getEdad()
        );

        estudianteRepository.save(estudiante);

        return estudianteDTO;

    }
    @Transactional
    public void deleteEstudiante(@NotNull @Positive Long estudianteId){
        estudianteRepository.deleteById(estudianteId);
    }

}
