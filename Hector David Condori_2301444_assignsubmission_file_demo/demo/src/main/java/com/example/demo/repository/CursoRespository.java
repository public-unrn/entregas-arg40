package com.example.demo.repository;

import com.example.demo.domain.Curso;
import com.example.demo.domain.Estudiante;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface CursoRespository extends JpaRepository<Curso, Long> {
    //Listar todos los cursos
    @Query("SELECT c FROM Curso c")
    List<Curso> findAllCursos();

    //Listar todos los cursos que hayan empezado después de “01/02/2020”
    @Query("SELECT c FROM Curso c WHERE c.fechaDeInicio > :empezadoDesde")
    List<Curso>findAllCursosStartedAfter(@Param("empezadoDesde")LocalDate empezadoDesde);

    //Consulta derivada
    List<Curso>findByFechaDeInicioGreaterThan(LocalDate empezadoDesde);
    List<Curso>findByOrderById();
}
