package com.example.demo;

import com.example.demo.domain.Curso;
import com.example.demo.domain.Estado;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;
import com.example.demo.repository.CursoRespository;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.repository.InscripcionRepository;
import com.example.demo.service.InscripcionService;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import java.util.List;

import java.time.LocalDate;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
	@Autowired
	CursoRespository cursoRespository;

	@Autowired
	EstudianteRepository estudianteRepository;

	@Autowired
	InscripcionRepository inscripcionRepository;
	@Autowired
	InscripcionService inscripcionService;

	private void saveCursos(){

		Curso cursoNuevo1 = new Curso(
			null,
			"Programacion en java",
			LocalDate.of(2023,8,14),
			LocalDate.of(2023,9,14),
			"Java"
		);
		cursoRespository.save(cursoNuevo1);

		Curso cursoNuevo2 = new Curso(
			null,
			"Programacion en C++",
			LocalDate.of(2023,9,14),
			LocalDate.of(2023,10,14),
			"C++"
		);
		cursoRespository.save(cursoNuevo2);

		Curso cursoNuevo3 = new Curso(
			null,
			"Programacion en javaScript",
			LocalDate.of(2023,8,14),
			LocalDate.of(2023,9,14),
			"JavaScript"
		);
		cursoRespository.save(cursoNuevo3);

	}

	private void saveEstudiantes(){

		Estudiante nuevoEstudiante1 = new Estudiante(
				null,
				29454612,
				"nuevorr@e.com",
				LocalDate.of(1991,4,13),
				"Raul",
				"Romero",
				32
		);
		estudianteRepository.save(nuevoEstudiante1);

		Estudiante nuevoEstudiante2 = new Estudiante(
				null,
				22546120,
				"nuevomm@e.com",
				LocalDate.of(2006,4,13),
				"Martineti",
				"Martin",
				17
		);

		estudianteRepository.save(nuevoEstudiante2);

		Estudiante nuevoEstudiante3 = new Estudiante(
				null,
				19454612,
				"nuevorf@e.com",
				LocalDate.of(1999,4,13),
				"Romero",
				"Florencia",
				24
		);

		estudianteRepository.save(nuevoEstudiante3);

	}

	@Bean
	public CommandLineRunner commandLineRunner (ApplicationContext ctx){
		return args -> {

			/*
			Listar todos los estudiantes de forma paginada y ordenada ascendente por DNI

			Probar las siguientes combinaciones:
			○ pagina 1, tamaño 5
			○ pagina 0, tamaño 2
			*/

			Page<Inscripcion> pagina1 =inscripcionRepository.findAll(PageRequest.of(0,2));

			for (Inscripcion inscripcion:pagina1) {
				System.out.println("Incripcion con Id = " + inscripcion.getId());
			}

			Page<Inscripcion> pagina2 =inscripcionRepository.findAll(PageRequest.of(1,5));

			for (Inscripcion inscripcion:pagina2) {
				System.out.println("Incripcion con Id = " + inscripcion.getId());
			}

			System.out.println("Se ejecuto");
		};
	}
}
