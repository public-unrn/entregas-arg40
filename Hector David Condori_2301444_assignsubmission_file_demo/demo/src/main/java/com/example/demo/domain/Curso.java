package com.example.demo.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "curso")
public class Curso {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  @Column(name = "descripcion")
  private String descripcion;

  @Column(name = "fecha_de_inicio")
  private LocalDate fechaDeInicio;

  @Column(name = "fecha_de_fin")
  private LocalDate fechaDeFin;

  @Column(name = "nombre")
  private String nombre;

}
