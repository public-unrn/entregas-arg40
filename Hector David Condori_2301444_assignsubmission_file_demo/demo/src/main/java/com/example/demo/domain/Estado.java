package com.example.demo.domain;

public enum Estado {
    RECHAZADO,
    PENDIENTE,
    ACEPTADO
}
