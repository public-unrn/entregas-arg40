package com.example.demo.repository;

import com.example.demo.domain.Curso;
import com.example.demo.domain.Estudiante;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EstudianteRepository extends JpaRepository<Estudiante, Long> {

    //Listar todos los estudiantes
    @Query("SELECT e FROM Estudiante e")
    List<Estudiante> findAllEstudiantes();

    //Listar todos los estudiantes que tengan un dni mayor a 20M y que su apellido sea “Romero”
    @Query("SELECT e FROM Estudiante e WHERE e.dni > :numeroDni AND e.apellido = :apellido")
    List<Estudiante> findAllEstudiantesWithDniGreatThanAndLastNameLike(@Param("numeroDni") int numeroDni,@Param("apellido") String apellido );

    //Consulta derivada
    List<Estudiante> findByDniGreaterThanAndApellido(int numeroDni,String apellido );
    List<Estudiante> findByOrderById();
}
