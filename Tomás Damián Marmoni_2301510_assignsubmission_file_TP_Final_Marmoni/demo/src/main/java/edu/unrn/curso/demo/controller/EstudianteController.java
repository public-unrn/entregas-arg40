package edu.unrn.curso.demo.controller;

import edu.unrn.curso.demo.domain.Estudiante;
import edu.unrn.curso.demo.dto.EstudianteDTO;
import edu.unrn.curso.demo.repository.EstudianteRepository;
import edu.unrn.curso.demo.service.EstudianteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/estudiante")
public class EstudianteController {
    @Autowired
    private EstudianteService estudianteService;

    @PostMapping
    public EstudianteDTO saveEstudiante(@RequestBody EstudianteDTO estudianteDTO) {
        return estudianteService.saveEstudiante(estudianteDTO);
    }

    @GetMapping
    public List<EstudianteDTO> findAll() {
        return estudianteService.findAll();
    }

    @GetMapping("/{id}")
    public EstudianteDTO findById(@PathVariable Long id) {
        return estudianteService.findById(id);
    }

    @PutMapping
    public EstudianteDTO updateEstudiante(@RequestBody EstudianteDTO estudianteDTO) {
        return estudianteService.updateEstudiante(estudianteDTO);
    }

    @DeleteMapping
    public void deleteById(@RequestParam Long id) {
        estudianteService.deleteById(id);
    }
}