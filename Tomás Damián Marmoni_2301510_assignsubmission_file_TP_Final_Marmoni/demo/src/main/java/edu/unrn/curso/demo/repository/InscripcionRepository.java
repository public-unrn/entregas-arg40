package edu.unrn.curso.demo.repository;

import edu.unrn.curso.demo.domain.EstadoInscripcion;
import edu.unrn.curso.demo.domain.Inscripcion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InscripcionRepository extends JpaRepository<Inscripcion, Long> {
    @Query("SELECT i FROM Inscripcion i WHERE i.estado = 'PENDIENTE' OR i.estado = 'RECHAZADA'")
    List<Inscripcion> findAllPendientesOrRechazadas();
    
    List<Inscripcion> findByEstado(EstadoInscripcion estado);
    
    @Query(value = "SELECT * FROM inscripcion WHERE estado = :estado", nativeQuery = true)
    List<Inscripcion> findAllByEstado(@Param("estado") EstadoInscripcion estado);


}
