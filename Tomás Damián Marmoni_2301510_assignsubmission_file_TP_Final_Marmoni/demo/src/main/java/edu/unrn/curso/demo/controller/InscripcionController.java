package edu.unrn.curso.demo.controller;

import edu.unrn.curso.demo.dto.InscripcionDTO;
import edu.unrn.curso.demo.service.InscripcionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/inscripcion")
public class InscripcionController {
    @Autowired
    private InscripcionService inscripcionService;

    @PostMapping
    public void saveInscripcion(@RequestParam Long estudianteId, @RequestParam Long cursoId) {
        inscripcionService.registrarInscripcion(estudianteId, cursoId);
    }
}
