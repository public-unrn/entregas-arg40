package edu.unrn.curso.demo.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Date;

@Entity @Table(name = "curso")
@Getter @Setter
public class Curso {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private @Id Long id;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "fecha_inicio")
    private LocalDate fechaInicio;
    @Column(name = "fecha_fin")
    private LocalDate fechaFin;

    public Curso(String nombre, String descripcion, LocalDate fechaInicio, LocalDate fechaFin){
      this.nombre = nombre;
      this.descripcion = descripcion;
      this.fechaInicio = fechaInicio;
      this.fechaFin = fechaFin;
    }
}
