package edu.unrn.curso.demo.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.Period;
import java.util.Date;

@Entity @Table(name = "estudiante")
@Getter @Setter
public class Estudiante {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private @Id Long id;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "apellido")
    private String apellido;
    @Column(name = "email")
    private String email;
    @Column(name = "dni")
    private int dni;
    @Column(name = "fecha_nacimiento")
    private LocalDate fechaNacimiento;
    @Transient
    private int edad;

    public Estudiante(String nombre, String apellido, String email, int dni, LocalDate fechaNacimiento) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.email = email;
        this.dni = dni;
        this.fechaNacimiento = fechaNacimiento;
        this.edad = Period.between(fechaNacimiento, LocalDate.now()).getYears();
    }
    public void setEdad(int edad) {
        this.edad = Period.between(fechaNacimiento, LocalDate.now()).getYears();
    }

    public boolean esMayorDeEdad() {
       return this.getEdad() < 18;
    }

}
