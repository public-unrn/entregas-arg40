package edu.unrn.curso.demo.dto;

import edu.unrn.curso.demo.domain.EstadoInscripcion;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data @AllArgsConstructor
public class InscripcionDTO {
    private Long id;
    private LocalDate fechaInscripcion;
    private EstadoInscripcion estado;
    private CursoDTO curso;
    private EstudianteDTO estudiante;
}
