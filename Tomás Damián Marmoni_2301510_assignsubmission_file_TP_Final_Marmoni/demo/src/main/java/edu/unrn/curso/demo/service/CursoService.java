package edu.unrn.curso.demo.service;

import edu.unrn.curso.demo.domain.Curso;
import edu.unrn.curso.demo.dto.CursoDTO;
import edu.unrn.curso.demo.repository.CursoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CursoService {
    @Autowired
    CursoRepository cursoRepository;

    public CursoDTO saveCurso(CursoDTO cursoDTO){
        Curso curso = new Curso (
                cursoDTO.getNombre(),
                cursoDTO.getDescripcion(),
                cursoDTO.getFechaInicio(),
                cursoDTO.getFechaFin()
        );
        cursoRepository.save(curso);
        return cursoDTO;
    }


}
