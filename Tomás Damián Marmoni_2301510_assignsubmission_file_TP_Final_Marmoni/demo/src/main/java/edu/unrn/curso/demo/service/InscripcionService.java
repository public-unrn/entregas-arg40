package edu.unrn.curso.demo.service;

import edu.unrn.curso.demo.domain.Curso;
import edu.unrn.curso.demo.domain.EstadoInscripcion;
import edu.unrn.curso.demo.domain.Estudiante;
import edu.unrn.curso.demo.domain.Inscripcion;
import edu.unrn.curso.demo.dto.CursoDTO;
import edu.unrn.curso.demo.dto.EstudianteDTO;
import edu.unrn.curso.demo.dto.InscripcionDTO;
import edu.unrn.curso.demo.repository.CursoRepository;
import edu.unrn.curso.demo.repository.EstudianteRepository;
import edu.unrn.curso.demo.repository.InscripcionRepository;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

@Service
@Validated
public class InscripcionService {
    @Autowired
    InscripcionRepository inscripcionRepository;

    @Autowired
    EstudianteRepository estudianteRepository;

    @Autowired
    CursoRepository cursoRepository;

    public void registrarInscripcion(@NotNull @Positive Long estudianteId, @NotNull @Positive Long cursoId){


        Estudiante estudiante = estudianteRepository
                .findById(estudianteId)
                .orElseThrow(() -> new RuntimeException("Estudiante no encontrado"));

        Curso curso = cursoRepository
                .findById(cursoId)
                .orElseThrow(() -> new RuntimeException("Curso no encontrado"));

        if(!estudiante.esMayorDeEdad()){
            throw new RuntimeException("El estudiante no es mayor de edad");
        }

        Inscripcion inscripcion = new Inscripcion(EstadoInscripcion.PENDIENTE, curso, estudiante);

        inscripcionRepository.save(inscripcion);
    }
}
