package edu.unrn.curso.demo.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Entity @Table(name = "inscripcion")
@Getter @Setter @NoArgsConstructor
public class Inscripcion {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private @Id Long id;
    private LocalDate fechaInscripcion;
    @Enumerated(EnumType.STRING)
    private EstadoInscripcion estado;
    @ManyToOne @JoinColumn(name = "curso_id")
    private Curso curso;
    @ManyToOne @JoinColumn(name = "estudiante_id")
    private Estudiante estudiante;

    public Inscripcion(EstadoInscripcion pendiente, Curso curso, Estudiante estudiante){
        this.estado = estado;
        this.curso = this.curso;
        this.estudiante = this.estudiante;
    }

}
