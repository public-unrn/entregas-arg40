package edu.unrn.curso.demo.service;

import edu.unrn.curso.demo.domain.Estudiante;
import edu.unrn.curso.demo.dto.EstudianteDTO;
import edu.unrn.curso.demo.repository.EstudianteRepository;
import org.antlr.v4.runtime.misc.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EstudianteService {
    @Autowired
    private EstudianteRepository estudianteRepository;

    public EstudianteDTO saveEstudiante(EstudianteDTO estudianteDTO){
        Estudiante estudiante = new Estudiante(
                estudianteDTO.getNombre(),
                estudianteDTO.getApellido(),
                estudianteDTO.getEmail(),
                estudianteDTO.getDni(),
                estudianteDTO.getFechaNacimiento());

        estudianteRepository.save(estudiante);

        return estudianteDTO;
    }

    public List<EstudianteDTO> findAll(){
        return estudianteRepository.findAll()
                .stream()
                .map(e -> new EstudianteDTO(
                        e.getNombre(),
                        e.getApellido(),
                        e.getEmail(),
                        e.getDni(),
                        e.getFechaNacimiento()
                ))
                .collect(Collectors.toList());
    }

    public EstudianteDTO updateEstudiante(EstudianteDTO estudianteDTO){
        Estudiante estudiante = estudianteRepository
                .findById(estudianteDTO.getId())
                .orElseThrow(() -> new RuntimeException("No se encontro el estudiante"));

        estudiante.setNombre(estudianteDTO.getNombre());
        estudiante.setApellido(estudianteDTO.getApellido());
        estudiante.setEmail(estudianteDTO.getEmail());
        estudiante.setDni(estudianteDTO.getDni());
        estudiante.setFechaNacimiento(estudianteDTO.getFechaNacimiento());

        estudianteRepository.save(estudiante);

        return estudianteDTO;
    }

    public EstudianteDTO findById(Long id){
        Estudiante estudiante = estudianteRepository
                .findById(id)
                .orElseThrow(() -> new RuntimeException("No se encontro el estudiante"));
        return new EstudianteDTO(
                estudiante.getNombre(),
                estudiante.getApellido(),
                estudiante.getEmail(),
                estudiante.getDni(),
                estudiante.getFechaNacimiento()
        );
    }

    public void deleteById(Long id){
        estudianteRepository.deleteById(id);
    }
}
