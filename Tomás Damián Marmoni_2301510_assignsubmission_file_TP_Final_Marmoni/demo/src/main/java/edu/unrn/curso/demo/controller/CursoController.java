package edu.unrn.curso.demo.controller;

import edu.unrn.curso.demo.dto.CursoDTO;
import edu.unrn.curso.demo.service.CursoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/curso")
public class CursoController {
   @Autowired
    private CursoService cursoService;

   @PostMapping
    public CursoDTO saveCurso(@RequestBody CursoDTO cursoDTO){
       cursoService.saveCurso(cursoDTO);
       return cursoDTO;
   }
}
