package edu.unrn.curso.demo.domain;

public enum EstadoInscripcion {
    ACEPTADO, RECHAZADO, PENDIENTE
}
