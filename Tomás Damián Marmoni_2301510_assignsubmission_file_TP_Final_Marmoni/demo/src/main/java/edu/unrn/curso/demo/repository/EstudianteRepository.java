package edu.unrn.curso.demo.repository;

import edu.unrn.curso.demo.domain.Estudiante;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EstudianteRepository extends JpaRepository<Estudiante, Long> {
    @Query("SELECT e FROM Estudiante e")
    List<Estudiante> findAllEstudiantes();
    @Query("SELECT e FROM Estudiante e WHERE e.apellido = :apellido AND e.dni > :dni")
    List<Estudiante> findAllByApellidoAndDniGreaterThan(@Param("apellido") String apellido, @Param("dni") int dni);


    //Listar todos los estudiantes de forma paginada con parámetros page 0 size 5 y ordenada ascendentemente según dni.
    List<Estudiante> findAllByOrderByDniAsc(Pageable pageable);

    //Page<Estudiante> findAllByOrderByDniAsc();



}
