package edu.unrn.curso.demo.repository;

import edu.unrn.curso.demo.domain.Curso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;

public interface CursoRepository extends JpaRepository<Curso, Long> {
    @Query("SELECT c FROM Curso c")
    List<Curso> findAllCursos();

    //Listar todos los cursos que inicien después una LocalDate pasada como @Param
    @Query("SELECT c FROM Curso c WHERE c.fechaInicio > :fechaInicio")
    List<Curso> findAllByFechaInicioGreaterThan(LocalDate fechaInicio);

}
