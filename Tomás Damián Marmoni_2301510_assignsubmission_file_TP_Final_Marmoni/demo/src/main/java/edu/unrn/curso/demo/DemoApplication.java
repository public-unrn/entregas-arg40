package edu.unrn.curso.demo;

import edu.unrn.curso.demo.domain.*;
import edu.unrn.curso.demo.repository.CursoRepository;
import edu.unrn.curso.demo.repository.EstudianteRepository;
import edu.unrn.curso.demo.repository.InscripcionRepository;
import edu.unrn.curso.demo.service.InscripcionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.PageRequest;

import java.time.LocalDate;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Autowired
	CursoRepository cursoRepository;
	@Autowired
	EstudianteRepository estudianteRepository;
	@Autowired
	InscripcionRepository inscripcionRepository;
	@Autowired
	InscripcionService inscripcionService;

	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		return args -> {

		};
	}
}
