package com.example.demo.repository;
import com.example.demo.domain.Estado;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;
import jakarta.persistence.criteria.CriteriaBuilder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.time.LocalDate;
import java.util.List;

public interface InscripcionRepository extends JpaRepository<Inscripcion,Long> {
    @Query("SELECT i FROM Inscripcion i WHERE i.estado='Pendiente' OR i.estado='Rechazada'")
    List<Inscripcion> finAllInscripcionPendienteRechazada();
    @Query("SELECT i FROM Inscripcion i WHERE i.fechaDeInscripcion > :fecha")
    List<Inscripcion> findAllInscripcionFechaInscripcion(@Param("fecha")LocalDate fecha);
    @Query("SELECT i FROM Inscripcion i WHERE i.estado= :parametroEstado")
    List<Inscripcion> findallInscripciones(@Param("parametroEstado") Estado parametroEstado);
    @Query(value = "SELECT * FROM inscripcion WHERE estado= :parametroEstado",nativeQuery = true)
    List<Inscripcion> findAllInscripcionNat(@Param("parametroEstado") String parametroEstado);
    //Consultas derivadas
    /** Listar todas las inscripciones rechazadas o pendiente **/
    List<Inscripcion> findIscripcionesByEstadoIn(List<Estado> estados);
    /**Listar todos los cursos que hayan empezado después de “01/02/2020 **/
    List<Inscripcion> findCursosByFechaDeInscripcionLessThan(LocalDate fecha);
    /** Listar todas las inscripciones en base a un parámetro de estado **/
    List<Inscripcion> findInscripcionesByEstadoIs(Estado estado);


}
