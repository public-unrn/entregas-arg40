package com.example.demo.service;
import com.example.demo.dto.CursoDTO;

import com.example.demo.domain.Curso;
import com.example.demo.repository.CursoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CursoService {
    @Autowired
    private CursoRepository cursoRepository;
    public CursoDTO saveCursoDTO(CursoDTO cursoDTO){
        Curso curso= new Curso(
                null,cursoDTO.getDescripcion(),cursoDTO.getFechaDeInicio(),
                cursoDTO.getFechaDeFin(),cursoDTO.getNombre()
        );
        cursoRepository.save(curso);
        return cursoDTO;
    }
}
