package com.example.demo.repository;

import com.example.demo.domain.Curso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CursoRepository extends JpaRepository<Curso,Long> {

    @Query("SELECT c FROM Curso c")
    List<Curso> findAllCurso();

    List<Curso> findAllBy();
}
