package com.example.demo;
import com.example.demo.domain.Curso;
import com.example.demo.domain.Estado;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;
import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.repository.InscripcionRepository;
import com.example.demo.service.InscripcionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.util.Arrays;

@SpringBootApplication
public class DemoApplication {
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
	@Autowired
	EstudianteRepository estudianteRepository;
	@Autowired
	CursoRepository cursoRepository;
	@Autowired
	InscripcionRepository inscripcionRepository;
	@Autowired
	InscripcionService inscripcionService;
	private void saveCursos(){
		cursoRepository.saveAll(Arrays.asList(
				new Curso(null,"Curso dictado por la UNRN",LocalDate.of(2023, 7, 13),LocalDate.of(2023, 8, 1),"Spring Boot"),
				new Curso(null,"Curso dictado por la UTN",LocalDate.of(2023, 7, 11),LocalDate.of(2023, 8, 12),"Testing Avanzado ")
				));

	}
	private void saveEstudiantes(){
		estudianteRepository.saveAll(Arrays.asList(
				new Estudiante(null, 33245324, "santy@gmail.com",LocalDate.of(1980, 2, 1),"Gonzales", "Santiago"),
				new Estudiante(null,30929888,"juan@hotmail.com",LocalDate.of(1990, 2, 1),"Garcia","Juan"),
				new Estudiante(null, 252225512, "santy@gmail.com",LocalDate.of(1986, 2, 1),"Ortega", "Santiago"),
				new Estudiante(null, 32323252, "tito@gmail.com",LocalDate.of(1987, 2, 1),"Gonzales", "Diego"),
				new Estudiante(null, 33245324, "osvaldo@gmail.com",LocalDate.of(1999, 2, 1),"Cappa", "Osvaldo"),
				new Estudiante(null, 34235323, "andres@gmail.com",LocalDate.of(1995, 2, 1),"Sosa", "Andres"),
				new Estudiante(null, 31542423, "javier@gmail.com",LocalDate.of(2000, 2, 1),"Serna", "Santiago"),
				new Estudiante(null, 223411553, "daiana@gmail.com",LocalDate.of(1995, 2, 1),"Perez", "Daiana")
				));
	}
	private void saveInscripciones(){
		inscripcionRepository.saveAll(Arrays.asList(
				new Inscripcion(null, cursoRepository.findById(1L).orElse(null),LocalDate.of(2023, 7, 14),estudianteRepository.findById(1L).orElse(null),Estado.PENDIENTE)
		));
	}
	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx){

		return args->{
			saveCursos();
			saveEstudiantes();
			saveInscripciones();

		/**Listar todos los cursos **/
			cursoRepository.findAllCurso();
		/**Listar todos los estudiantes **/
			estudianteRepository.findAllEstudiante();
		/**Listar todos los estudiantes que tengan un dni mayor a 20M y que su apellido sea “Romero”**/
			estudianteRepository.findAllEstudianteMayorVeinteRomero();
		/** Listar todas las inscripciones rechazadas o pendiente **/
			inscripcionRepository.finAllInscripcionPendienteRechazada();
		/**Listar todos los cursos que hayan empezado después de “01/02/2020 **/
			inscripcionRepository.findAllInscripcionFechaInscripcion(LocalDate.now());
		/** Listar todas las inscripciones en base a un parámetro de estado **/
			inscripcionRepository.findallInscripciones(Estado.PENDIENTE);
		/**Listar todas las inscripciones en base a un parámetro de estado utilizando consulta
		 nativa **/
			inscripcionRepository.findAllInscripcionNat("PENDIENTE");
		/**Implementar las primeras 6 consultas mediante consulta derivada **/
			//1
			cursoRepository.findAllBy();
			//2
			estudianteRepository.findAllBy();
			//3
			estudianteRepository.findEstudiantesByDniGreaterThanAndApellidoEquals(2000000,"Romero");
			//4
			inscripcionRepository.findIscripcionesByEstadoIn(Arrays.asList(Estado.RECHAZADO, Estado.PENDIENTE));
			//5
			inscripcionRepository.findCursosByFechaDeInscripcionLessThan(LocalDate.of(2020, 2, 1));
			//6
			inscripcionRepository.findInscripcionesByEstadoIs(Estado.PENDIENTE);
		/**Listar todos los estudiantes de forma paginada y ordenada ascendente por DNI Probar las siguientes combinaciones **/
		/**pagina 1, tamaño 5 **/
		estudianteRepository.findAll(PageRequest.of(1,5, Sort.by(Sort.Direction.ASC,"dni")))	;
		/**pagina 0, tamaño 2 **/
		estudianteRepository.findAll(PageRequest.of(0,2, Sort.by(Sort.Direction.ASC,"dni")))	;
		//Servicio y Transacción
		/**Queremos registrar una inscripción donde se tiene que indicar el estudiante y curso. Reglas de negocio:
		 ● El estudiante debe ser mayor de edad **/
		inscripcionService.createInscripcion(1L,1L);
		//Nota: validar todos los parametros del metodo del servicio


		};
	}
}
