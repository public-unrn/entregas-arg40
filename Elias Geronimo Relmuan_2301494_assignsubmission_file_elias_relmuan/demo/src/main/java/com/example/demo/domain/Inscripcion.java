package com.example.demo.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
@Entity
@Table(name="inscripcion")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Inscripcion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Long id;
    @ManyToOne
    @JoinColumn(name="curso_id")
    private Curso curso;
    private LocalDate fechaDeInscripcion;
    @ManyToOne
    @JoinColumn(name="estudiante_id")
    private Estudiante estudiante;
    @Enumerated(EnumType.STRING)
    private Estado estado;
}
