package com.example.demo.service;
import com.example.demo.domain.Curso;
import com.example.demo.domain.Estado;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;
import com.example.demo.dto.InscripcionDTO;
import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.repository.InscripcionRepository;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDate;

@Service
public class InscripcionService {
    @Autowired
    private EstudianteRepository estudianteRepository;
    @Autowired
    private CursoRepository cursoRepository;
    @Autowired
    private InscripcionRepository inscripcionRepository;
    public InscripcionDTO saveInscripcion(InscripcionDTO inscripcionDTO){
        Inscripcion inscripcion= new Inscripcion(
                null,
                inscripcionDTO.getCurso(),
                inscripcionDTO.getFechaDeInscripcion(),
                inscripcionDTO.getEstudiante(),
                inscripcionDTO.getEstado()
        );
        inscripcionRepository.save(inscripcion);
        return inscripcionDTO;
    }


    @Transactional
    public void createInscripcion (@NotNull @Positive(message = "No puede ser negativo") Long id_curso, @NotNull @Positive(message = "No puede ser negativo") Long id_estudiante){
        Estudiante optionalestudiante = estudianteRepository
                .findById(id_estudiante)
                .orElseThrow(()->new RuntimeException("El id del estudiante no es valido"));

        if (!optionalestudiante.esMayorEdad()){
            throw new RuntimeException("El estudiante no es mayor de edad");
        }
        Curso optionalcurso = cursoRepository
                .findById(id_curso)
                .orElseThrow(()->new RuntimeException("El id del curso no es valido"));

        Inscripcion inscripcion= new Inscripcion(null, optionalcurso,LocalDate.now(),optionalestudiante, Estado.PENDIENTE);
        inscripcionRepository.save(inscripcion);
    }


}
