package com.example.demo.repository;

import com.example.demo.domain.Estudiante;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.w3c.dom.stylesheets.LinkStyle;

import java.util.List;

public interface EstudianteRepository  extends JpaRepository<Estudiante,Long> {

    @Query("SELECT e  FROM Estudiante e")
    List<Estudiante> findAllEstudiante();
    @Query("SELECT e FROM Estudiante e WHERE e.dni >20000000 AND e.apellido='Romero'")
    List<Estudiante> findAllEstudianteMayorVeinteRomero();
    // Consultas derivadas
    List<Estudiante> findAllBy();
    /**Listar todos los estudiantes que tengan un dni mayor a 20M y que su apellido sea “Romero”**/
    List<Estudiante> findEstudiantesByDniGreaterThanAndApellidoEquals(int dni, String apellido);




}
