package com.example.demo.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.Period;

@Entity
@Table(name="estudiante")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Estudiante {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name="dni")
    private int dni;
    @Column(name="email")
    private String email;
    @Column (name="fecha_de_nacimiento")
    private LocalDate fechaDeNacimiento;
    @Column (name="apellido")
    private  String apellido;
    @Transient
    private  int edad;
    @Column(name="nombre")
    private  String nombre;
    public Estudiante(Long id, int dni, String email, LocalDate fechaDeNacimiento, String apellido, String nombre) {
        this.id = id;
        this.dni = dni;
        this.email = email;
        this.fechaDeNacimiento = fechaDeNacimiento;
        this.apellido = apellido;
        this.nombre = nombre;
    }

    public int getEdad(){
        LocalDate fechaActual = LocalDate.now();
        Period periodo = Period.between(fechaDeNacimiento, fechaActual);
        return periodo.getYears();
    }
    public boolean esMayorEdad(){
        return getEdad() > 18;
    }

}
