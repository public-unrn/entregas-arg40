package com.example.demo.repository;

import com.example.demo.domain.Estudiante;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EstudianteRepository extends JpaRepository<Estudiante, Long> {

    ////////////JPQL

    //Listar todos los estudiantes
    @Query("SELECT c FROM Estudiante c")
    List<Estudiante> findAllEstudiantes();

    //Listar todos los estudiantes que tengan un dni mayor a 20M y que su apellido sea “Romero”
    @Query("SELECT c FROM Estudiante c WHERE c.dni > 20000000 AND c.apellido = 'Romero'")
    List<Estudiante> findEstudiantesByMinDniAndLastname();


    ////////////DERIVADAS

    //Listar todos los estudiantes
    List<Estudiante> findAll();

    //Listar todos los estudiantes que tengan un dni mayor a 20M y que su apellido sea “Romero”
    List<Estudiante> findByDniGreaterThanAndApellidoIs(int nd, String ap);

    //Listar estudiantes de forma paginada y ordenada ascendente por DNI
    Page<Estudiante> findByOrderByDniAsc(Pageable pageable);

}
