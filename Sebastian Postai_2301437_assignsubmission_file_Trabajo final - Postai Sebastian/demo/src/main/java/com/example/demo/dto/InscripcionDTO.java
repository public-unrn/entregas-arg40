package com.example.demo.dto;

import com.example.demo.domain.Curso;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;
import lombok.AllArgsConstructor;
import lombok.Data;
import java.time.LocalDate;

@Data
@AllArgsConstructor
public class InscripcionDTO {

    private LocalDate FechaDeInscripcion;
    private Curso curso;
    private Estudiante estudiante;
    private Inscripcion.Estado estado;

}
