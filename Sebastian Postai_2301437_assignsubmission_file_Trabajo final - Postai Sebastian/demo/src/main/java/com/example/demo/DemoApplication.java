package com.example.demo;

import com.example.demo.domain.Curso;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;
import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.repository.InscripcionRepository;
import com.example.demo.service.InscripcionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Autowired
	CursoRepository cursoRepository;
	@Autowired
	EstudianteRepository estudianteRepository;
	@Autowired
	InscripcionRepository inscripcionRepository;

	@Autowired
	InscripcionService inscripcionService;


	public void saveCursosEstudiantesInscripciones(){
		Curso curso1 = new Curso(null, "Fotografía", "Hyperlapse", LocalDate.of(2023, 01, 10), LocalDate.of(2023, 6, 15));
		Curso curso2 = new Curso(null, "Fotografía 2", "Hyperlapse Avanzado", LocalDate.of(2023, 7, 10), LocalDate.of(2024, 1, 8));
		Curso curso3 = new Curso(null, "Video", "Video 101", LocalDate.of(2024, 1, 10), LocalDate.of(2024, 12, 30));
		Curso curso4 = new Curso(null, "Animación 3D", "Modelado 3D", LocalDate.of(2019, 5, 1), LocalDate.of(2020, 5, 1));
		cursoRepository.save(curso1);
		cursoRepository.save(curso2);
		cursoRepository.save(curso3);
		cursoRepository.save(curso4);

		Estudiante estudiante1 = new Estudiante(null, "Damian", "Vega", 38088918, "sebapostai92@gmail.com", (LocalDate.of(1995, 3, 20)), 28);
		Estudiante estudiante2 = new Estudiante(null, "Valeria", "Jones", 32234918, "valejones@gmail.com", (LocalDate.of(1990, 7, 10)), 18);
		Estudiante estudiante3 = new Estudiante(null, "Nicolas", "Perez", 40123123, "nico@gmail.com", (LocalDate.of(2000, 12, 3)), 16);
		Estudiante estudiante4 = new Estudiante(null, "Jose", "Romero", 18234589, "joromero@gmail.com", (LocalDate.of(1970, 5, 17)), 50);
		Estudiante estudiante5 = new Estudiante(null, "Belen", "Romero", 21234589, "Belromero@gmail.com", (LocalDate.of(1990, 8, 21)), 40);
		estudianteRepository.save(estudiante1);
		estudianteRepository.save(estudiante2);
		estudianteRepository.save(estudiante3);
		estudianteRepository.save(estudiante4);
		estudianteRepository.save(estudiante5);

		Inscripcion inscripcion1 = new Inscripcion(null, LocalDate.of(2019, 4, 10), curso1, estudiante3, Inscripcion.Estado.ACEPTADO);
		Inscripcion inscripcion2 = new Inscripcion(null, LocalDate.of(2021, 2, 1), curso1, estudiante2, Inscripcion.Estado.PENDIENTE);
		Inscripcion inscripcion3 = new Inscripcion(null, LocalDate.of(2024, 1, 1), curso2, estudiante1, Inscripcion.Estado.ACEPTADO);
		Inscripcion inscripcion4 = new Inscripcion(null, LocalDate.of(2023, 6, 22), curso3, estudiante1, Inscripcion.Estado.RECHAZADO);
		inscripcionRepository.save(inscripcion1);
		inscripcionRepository.save(inscripcion2);
		inscripcionRepository.save(inscripcion3);
		inscripcionRepository.save(inscripcion4);
	}

	@Bean
	public CommandLineRunner commandLineRunner (ApplicationContext ctx) {
		return args -> {

			System.out.println("La aplicación se ha iniciado");

			saveCursosEstudiantesInscripciones();

			//Listar estudiantes de forma paginada y ordenada ascendente por DNI - pagina 1, tamaño 5
			System.out.println("Listar estudiantes de forma paginada y ordenada ascendente por DNI - pagina 1, tamaño 5");
			estudianteRepository.findAll(PageRequest.of(1, 5, Sort.by(Sort.Direction.ASC,"dni")));

			//Listar estudiantes de forma paginada y ordenada ascendente por DNI - pagina 0, tamaño 2
			System.out.println("Listar estudiantes de forma paginada y ordenada ascendente por DNI - pagina 0, tamaño 2");
			estudianteRepository.findAll(PageRequest.of(0, 2, Sort.by(Sort.Direction.ASC,"dni")));

			// Servicio y Transacción
			System.out.println("Servicio inscripciones");
			inscripcionService.inscripcion(4L, 5L, LocalDate.of(2025, 10, 10), Inscripcion.Estado.PENDIENTE);

			System.out.println("Ultima linea");

		};
	}

}