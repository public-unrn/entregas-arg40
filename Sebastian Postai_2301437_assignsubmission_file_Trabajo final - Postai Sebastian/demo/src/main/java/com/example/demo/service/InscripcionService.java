package com.example.demo.service;

import com.example.demo.domain.Curso;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;
import com.example.demo.dto.InscripcionDTO;
import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.repository.InscripcionRepository;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Validated
@Service
public class InscripcionService {

    @Autowired
    private CursoRepository cursoRepository;

    @Autowired
    private EstudianteRepository estudianteRepository;

    @Autowired
    private InscripcionRepository inscripcionRepository;


    // Servicio y Transacción
    @Transactional
    public void inscripcion(@NotNull @Positive Long cursoId, @NotNull @Positive Long estudianteId, LocalDate fechaDeInicio, Inscripcion.Estado estado){

        Estudiante estudiante = estudianteRepository
                .findById(estudianteId)
                .orElseThrow( () -> new RuntimeException("El id del estudiante no es valido"));

        if(!estudiante.esMayorEdad()){
            throw new RuntimeException("El estudiante debe ser mayor de edad");
        }

        Curso curso = cursoRepository
                .findById(cursoId)
                .orElseThrow( () -> new RuntimeException("El id del curso no es valido"));


        Inscripcion inscripcion = new Inscripcion(null, fechaDeInicio, curso, estudiante, estado);

        inscripcionRepository.save(inscripcion);

    }

    public InscripcionDTO saveInscripcion(InscripcionDTO inscripcionDTO){
        Inscripcion inscripcion = new Inscripcion(
                null,
                inscripcionDTO.getFechaDeInscripcion(),
                inscripcionDTO.getCurso(),
                inscripcionDTO.getEstudiante(),
                inscripcionDTO.getEstado()
        );

        inscripcionRepository.save(inscripcion);
        return inscripcionDTO;
    }

    public List<InscripcionDTO> findAll(){
        return inscripcionRepository.findAll()
                .stream().map(i -> new InscripcionDTO(i.getFechaDeInscripcion(), i.getCurso(), i.getEstudiante(), i.getEstado()))
                .collect(Collectors.toList());
    }

    public InscripcionDTO update(Long id, InscripcionDTO inscripcionDTO){
        Inscripcion inscripcion = new Inscripcion(id, inscripcionDTO.getFechaDeInscripcion(), inscripcionDTO.getCurso(), inscripcionDTO.getEstudiante(), inscripcionDTO.getEstado());
        inscripcionRepository.save(inscripcion);
        return inscripcionDTO;
    }

    public InscripcionDTO find(Long id){
        Optional<Inscripcion> inscripcionOptional = inscripcionRepository.findById(id);

        if (inscripcionOptional.isEmpty()){
            throw new RuntimeException("Id invalido");
        }

        Inscripcion inscripcion = inscripcionOptional.get();

        return new InscripcionDTO(inscripcion.getFechaDeInscripcion(), inscripcion.getCurso(), inscripcion.getEstudiante(), inscripcion.getEstado());
    }

    public void delete(Long id){
        inscripcionRepository.deleteById(id);
    }

}
