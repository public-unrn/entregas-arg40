package com.example.demo.domain;

import jakarta.persistence.*;
import lombok.*;
import java.time.LocalDate;

@Entity
@Data
@Table(name = "inscripcion")
@AllArgsConstructor
@NoArgsConstructor
public class Inscripcion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "fecha_de_inscripcion")
    private LocalDate fechaDeInscripcion;

    @ManyToOne
    @JoinColumn(name = "curso_id")
    private Curso curso;

    @ManyToOne
    @JoinColumn(name = "estudiante_id")
    private Estudiante estudiante;

    @Column(name = "estado")
    @Enumerated(EnumType.STRING)
    private Estado estado;

    public enum Estado {
        ACEPTADO("A"), RECHAZADO("R"), PENDIENTE("P");
        private String code;
        private Estado(String code) {
            this.code = code;
        }
        public String getCode() {
            return code;
        }
    }

}
