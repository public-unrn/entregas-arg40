package com.example.demo.repository;

import com.example.demo.domain.Curso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface CursoRepository extends JpaRepository<Curso, Long> {

    ////////////JPQL

    //Listar todos los cursos
    @Query("select c from Curso c")
    List<Curso> findAllCursos();

    //Listar todos los cursos que hayan empezado después de “01/02/2020”
    @Query("select c from Curso c where c.fechaDeInicio >= :fechaDeInicio")
    List<Curso> findByStartDateAfter(@Param("fechaDeInicio") LocalDate fechaDeInicio);


    ////////////DERIVADAS

    //Listar todos los cursos
    List<Curso> findAll();

    //Listar todos los cursos que hayan empezado después de “01/02/2020”
    List<Curso> findByFechaDeInicioAfter(LocalDate date);
}
