package com.example.demo.repository;

import com.example.demo.domain.Inscripcion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InscripcionRepository extends JpaRepository<Inscripcion, Long> {

    ////////////JPQL

    //Listar todas las inscripciones rechazadas o pendiente
    @Query("SELECT c FROM Inscripcion c WHERE NOT c.estado ='ACEPTADO' ")
    List<Inscripcion> findNoAceptadas();

    //Listar todas las inscripciones en base a un parámetro de estado
    @Query("SELECT c FROM Inscripcion c WHERE c.estado = :estado")
    List<Inscripcion> findByParamEstado(@Param("estado") Enum estado);


    ////////////NATIVAS

    //Listar todas las inscripciones
    @Query(value = "select distinct i.* from inscripcion as i inner join estudiante as e on e.id=i.estudiante_id inner join curso as c on c.id=i.curso_id", nativeQuery = true)
    List<Inscripcion> findAllInscripcionesNativa();

    //Listar todas las inscripciones en base a un parámetro de estado
    @Query(value = "SELECT c FROM inscripcion c WHERE c.estado = :estado", nativeQuery = true)
    List<Inscripcion> findByParamEstadoNativa(@Param("estado") Enum estado);


    ////////////DERIVADAS

    //Listar todas las inscripciones rechazadas o pendiente
    List<Inscripcion> findAllByEstadoIsOrEstadoIs(Inscripcion.Estado estado, Inscripcion.Estado estado2); //List<Inscripcion> findAllByEstadoIsNot(Inscripcion.Estado estado);

    //Listar todas las inscripciones en base a un parámetro de estado
    List<Inscripcion> findAllByEstadoIs(Inscripcion.Estado estado);

}
