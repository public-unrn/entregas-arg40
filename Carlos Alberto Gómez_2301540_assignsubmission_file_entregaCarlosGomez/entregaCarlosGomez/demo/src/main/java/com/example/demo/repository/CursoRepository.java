package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.entity.Curso;

public interface CursoRepository extends JpaRepository<Curso, Long>{
	
	@Query("Select c FROM Curso c")
	List<Curso> findAllCurso();//Listar todos los cursos

	@Query("Select c FROM Curso c WHERE c.fecheDeInicio > '2020-02-01'")
	List<Curso> findByFechaDeInicio();//que hayan empezado después de “01/02/2020”
	
}
