package com.example.demo.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.entity.Estudiante;

public interface EstudianteRepository extends JpaRepository<Estudiante, Long>{
	@Query("Select e FROM Estudiante e")
	List<Estudiante> findAllEstudiante();//Listar todos los estudiantes
	
	@Query("Select e FROM Estudiante e WHERE e.dni > 20000000 AND e.apellido = 'Romero'")
	List<Estudiante> findByDniAndApellidoQ();//estudiantes con dni mayor a 20M y que su apellido sea“Romero”
	
	List<Estudiante> findAll(Long id, String nombre, String apellido, int dni, String email, LocalDate fechaDeNacimiento);//Listar todos los estudiantes. Cons Derivada
	List<Estudiante> findByDniAndApellido(int dni, String apellido );//estudiantes con dni mayor a 20M y que su apellido sea“Romero” cons Derivada
}
