package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.entity.Inscripcion;

public interface InscripcionRepository extends JpaRepository<Inscripcion, Long>{
	@Query("Select i FROM Inscripcion i WHERE i.estado = 'RECHAZADO' OR i.estado = 'PENDIENTE'")
	List<Inscripcion> findByEstado();//inscripciones rechazadas o pendiente
	
	@Query("Select i FROM Inscripcion i WHERE i.estado = 'RECHAZADO' OR i.estado = :unEst")
	List<Inscripcion> findByEstadoParm(@Param("unEstado")String unEst);//inscripciones en base a un parámetro de estado

	List<Inscripcion> findByEstadoParmNat(@Param("unEstado")String estado);//un parámetro de estado con consulta

	@Query(value="Select i.estado FROM inscripcion i WHERE i.estado = 'RECHAZADO' OR i.estado = :unEst", nativeQuery=true)
	List<Inscripcion> findByEstadoParmNativo(@Param("unEstado")String unEst);//inscripciones en base a un parámetro de estado. Nativa
	
}
