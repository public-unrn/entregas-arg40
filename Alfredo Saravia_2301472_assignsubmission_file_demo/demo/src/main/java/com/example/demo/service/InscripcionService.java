package com.example.demo.service;

import com.example.demo.domain.Curso;
import com.example.demo.domain.Estado;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;
import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.repository.InscripcionRepository;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.time.LocalDate;
@Service
@Validated
public class InscripcionService {


    @Autowired
    private CursoRepository cursoRepository;
    @Autowired
    private EstudianteRepository estudianteRepository;
    @Autowired
    private InscripcionRepository inscripcionRepository;

    public void rent(@NotNull @Positive Long estudianteId, @NotNull @Positive Long CursoId, LocalDate rentStartDate, LocalDate endEndDate) {

        if (rentStartDate.isAfter(endEndDate)) {
            throw new RuntimeException("Las fechas ingresadas no son validas");
        }

        Curso curso = cursoRepository
                .findById(cursoId)
                .orElseThrow(() -> new RuntimeException("El id del curso no es valido"));

        if (!curso.esMayorEdad()) {
            throw new RuntimeException("El curso es Mayor de edad...");
        }

        Estudiante estudiante = estudianteRepository
                .findById(estudianteId)
                .orElseThrow(() -> new RuntimeException("El id del estudiante no es valido"));

        if (!estudiante.esMayorEdad()) {
            throw new RuntimeException("El estudiante es menor de edad...");
        }


        Inscripcion inscripcion = new Inscripcion(
                null,
                rentStartDate,
                endEndDate,
                curso,
                estudiante
        );

        inscripcionRepository.save(inscripcion);
    }
}