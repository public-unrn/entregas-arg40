package com.example.demo.repository;

import com.example.demo.domain.Estudiante;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EstudianteRepository extends JpaRepository<Estudiante, Long> {
    //* Listar todos los estudiantes

    @Query( "SELECT c FROM Estudiante c")
    List<Estudiante> findAll();

    //* Listar todos los estudiantes que tengan un dni mayor a 20M
    // y que su apellido sea "Fernandez"

    @Query("SELECT c FROM Estudiante c WHERE dni > 20000000 AND apellido c Fernandez")
    List<Estudiante> findEstudianteByDni20MAndApellidoFernandez();

    //DERIVADA
    //* Listar todos los estudiantes
    List<Estudiante> findAll();
    //* Listar todos los estudiantes que tengan un dni mayor a 20M
    //y que su apellido sea "Fernandez"
    List<Estudiante> findBydniGreaterThan(int nd);
    List<Estudiante> findByApellidoIs(String apellido);
    List<Estudiante> findByContaining(String apellido);
    List<Estudiante> findByApellidoLike(String apellido);
    List<Estudiante> findBydniGreaterThanApellidoIs(int nd, String ap);
    List<Estudiante> findBydniGreaterThanApellidoLike(int nd, String ap);
    List<Estudiante> findBydniGreaterThanApellidoContaining(int nd, String ap);
    List<Estudiante> findByApellidoIsAndDniGreaterThan(String ap, int nd);
    List<Estudiante> findByApellidoLikeAndDniGreaterThan(String ap, int nd);
    List<Estudiante> findByApellidoContainingAndDniGreaterThan(String ap, int nd);

    //
}
