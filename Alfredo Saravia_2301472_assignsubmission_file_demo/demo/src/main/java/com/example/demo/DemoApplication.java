package com.example.demo;

import com.example.demo.domain.Curso;
import com.example.demo.domain.Estado;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;
import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.repository.InscripcionRepository;
import com.example.demo.service.InscripcionService;
import jakarta.persistence.Id;
import jakarta.validation.constraints.Null;
import org.hibernate.type.TrueFalseConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.yaml.snakeyaml.events.Event;

import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.Optional;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Autowired
	CursoRepository cursoRepository;
	@Autowired
	EstudianteRepository estudianteRepository;

	@Autowired
	InscripcionService inscripcionService;



			private void saveEstudiantes() {
				estudianteRepository.saveAll(Arrays.asList(
						new Estudiante(null, "Alfredo", "Saravia", "Alfre_95fer@hotmail.com", 12345678, LocalDate.of(1995, Month.AUGUST, 24), 27),
				new Estudiante(null, "Alberto", "Gomez", "Alberto_95gomez@hotmail.com", 25365278, LocalDate.of(1991, Month.OCTOBER, 10), 31),
				new Estudiante(null, "Ana", "Fernandez", "Ana_12Fernan@hotmail.com", 21345687, LocalDate.of(2002, Month.JULY, 15), 21),
				new Estudiante(null, "Lucas", "Martinez", "Lucas_02@hotmail.com", 32456874, LocalDate.of(2006, Month.APRIL, 22), 17)
				));
			}

			private void saveCursos() {
				cursoRepository.saveAll(Arrays.asList(
						new Curso(null,"Springboot" ,"Programación", LocalDate.of(2023, Month.JUNE, 15), LocalDate.of(2023, Month.DECEMBER, 14),
				new Curso(null, "Desarrollador", "Programación", LocalDate.of(2023, Month.JUNE, 15), LocalDate.of(2023, Month.DECEMBER, 14),
				new Curso(null,"RealidadVirtual", "Programación", LocalDate.of(2023, Month.JUNE, 15), LocalDate.of(2023, Month.DECEMBER, 14),
				new Curso(null,"Embedded", "Programación", LocalDate.of(2023, Month.JUNE, 15), LocalDate.of(2023, Month.DECEMBER, 14)
				);

			}
			private void saveInscripcion() {
				cursoRepository.saveAll(Arrays.asList(
						new Inscripcion(null, LocalDate.of(2023, Month.JUNE, 04), 1, "Ana", Estado.ACEPTADA),
						new Inscripcion(null, LocalDate.of(2023, Month.JUNE, 04), 2, "Alberto", Estado.RECHAZADA),
						new Inscripcion(null, LocalDate.of(2023, Month.JUNE, 04), 3, "Lucas", Estado.PENDIENTE),
						new Inscripcion(null, LocalDate.of(2023, Month.JUNE, 04), 4, "Alfredo", Estado.ACEPTADA)
				));
			}
			@Bean
			public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
				return args -> {   args: []
					saveCursos();
					saveEstudiantes();

					InscripcionService.rent(
							1L,
							1L,
							LocalDate.of(2020, Month.AUGUST, 1),
							LocalDate.of(2022, Month.AUGUST, 1)
					);
					cursoRepository.findAll();
				};
			}
}