package com.example.demo.repository;

import com.example.demo.domain.Curso;
import jakarta.validation.OverridesAttribute;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface CursoRepository extends JpaRepository<Curso, Long> {

    @Query ("SELECT c FROM Curso c")
    List<Curso> findAllCursos();

    @Query ("SELECT c FROM Curso c WHERE c.fechaDeInicio > :fechaDeInicio")
    List<Curso> findCursosByDate(@Param("fechaDeInicio")LocalDate fechaDeInicio);

    List<Curso> findByDescription(String descripcion);

    boolean existsByName(String nombre);

    long countByName(String nombre);



    //PARAMETROS

    @Query("SELECT c FROM Curso c WHERE fechaDeInicio > :fecha")
    List<Curso> findCursoComienzoPost01_02_2020(@Param("fecha") LocalDate fecha);

    //DERIVADA
    //* Listar todos los cursos derivada
    List<Curso> findAll();

    //* Listar todos los cursos que hayan empezado despues de "01/02/2020" derivada
    List<Curso> findByFechaDeInicioAfter(LocalDate fechaInicio);

}
