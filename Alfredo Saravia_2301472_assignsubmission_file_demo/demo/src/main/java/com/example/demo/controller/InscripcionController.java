package com.example.demo.controller;

import com.example.demo.domain.Inscripcion;
import com.example.demo.dto.InscripcionDTO;
import com.example.demo.service.InscripcionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/inscripcion")
public class InscripcionController {
    @Autowired
    private InscripcionService inscripcionService;


    public void save(@RequestBody InscripcionDTO inscripcionDTO) {
        inscripcionService.rent(inscripcionDTO.getCurso(), inscripcionDTO.getEstudiante(), inscripcionDTO.getRentStartDate(), inscripcionDTO.getEndEndDate());
    }
}
