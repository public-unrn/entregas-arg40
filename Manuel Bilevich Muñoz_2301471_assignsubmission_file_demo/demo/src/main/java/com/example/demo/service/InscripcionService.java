package com.example.demo.service;
import com.example.demo.domain.Curso;
import com.example.demo.domain.Estado;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;
import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.repository.InscripcionRepository;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PastOrPresent;
import jakarta.validation.constraints.Positive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import java.time.LocalDate;

@Service
@Validated
public class InscripcionService {
    @Autowired
    private CursoRepository cursoRepository;

    @Autowired
    private EstudianteRepository estudianteRepository;

    @Autowired
    private InscripcionRepository inscripcionRepository;

    @Transactional
    public void inscript(@NotNull @Positive Long cursoId, @NotNull @Positive Long estudianteId, @NotNull @PastOrPresent LocalDate fechaDeInscripcion, @NotNull Estado estado){

        Curso curso = cursoRepository
                .findById(cursoId)
                .orElseThrow(()->new RuntimeException("El cursoId no es valido"));
        Estudiante estudiante = estudianteRepository
                .findById(estudianteId)
                .orElseThrow(()->new RuntimeException("El EstudianteId no es valido"));

        if (!estudiante.esMayorEdad()){
            throw new RuntimeException("El Estudiante es menor de edad");
        }

        Inscripcion inscripcion = new Inscripcion(
                null,
                estado,
                curso,
                fechaDeInscripcion,
                estudiante
        );

        inscripcionRepository.save(inscripcion);
    }
}
