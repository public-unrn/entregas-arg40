package com.example.demo.repository;

import com.example.demo.domain.Estudiante;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
public interface EstudianteRepository extends JpaRepository<Estudiante, Long> {

    List<Estudiante> findAll();
    List<Estudiante> findByNombreAndDniGreaterThan(String nombre, Integer dni);
}
