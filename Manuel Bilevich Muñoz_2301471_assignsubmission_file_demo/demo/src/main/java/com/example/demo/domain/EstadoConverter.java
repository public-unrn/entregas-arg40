package com.example.demo.domain;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

import java.util.stream.Stream;

@Converter(autoApply = true)
public class EstadoConverter implements AttributeConverter<Estado, String> {
    @Override
    public String convertToDatabaseColumn(Estado estado){
        if (estado == null){
            return null;
        }
        return estado.getCode();
    }

    @Override
    public Estado convertToEntityAttribute(String estado){
        if (estado == null){
            return null;
        }

        return Stream.of(Estado.values())
                .filter(c -> c.getCode().equals(estado))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);

    }
}
