package com.example.demo.domain;

public enum Estado {
    Aceptada("A"), Rechazada("R"), Pendiente("P");

    private String status;

    private Estado(String status){
        this.status = status;
    }

    public String getCode(){
        return status;
    }
}
