package com.example.demo.repository;

import com.example.demo.domain.Estado;
import com.example.demo.domain.Inscripcion;
import org.hibernate.annotations.NamedNativeQuery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InscripcionRepository extends JpaRepository<Inscripcion, Long> {
    List<Inscripcion> findAllByEstadoIsNot(Estado estado);
    List<Inscripcion> findAllByEstado(Estado estado);

    @Query("SELECT c FROM Inscripcion c Where c.estado = :estado")
    List<Inscripcion> findAllByEstadoParam(@Param("estado") Estado estado);

    @Query(value = "SELECT * FROM inscripcion Where estado = ?1", nativeQuery = true)
    List<Inscripcion> findAllByEstadoParamNative(String estado);
}
