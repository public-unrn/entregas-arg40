package com.example.demo.service;

import com.example.demo.domain.Estudiante;
import com.example.demo.dto.EstudianteDTO;
import com.example.demo.repository.EstudianteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EstudianteService {
    @Autowired
    private EstudianteRepository estudianteRepository;

    public EstudianteDTO saveEstudiante(EstudianteDTO estudianteDTO){
        Estudiante estudiante = new Estudiante(
                null,
                estudianteDTO.getDni(),
                estudianteDTO.getEmail(),
                estudianteDTO.getFechaDeNacimiento(),
                estudianteDTO.getApellido(),
                estudianteDTO.getNombre(),
                null
        );

        estudianteRepository.save(estudiante);

        return estudianteDTO;
    }

    public List<EstudianteDTO> findAll(){
        return estudianteRepository.findAll()
                .stream().map(c -> new EstudianteDTO(c.getNombre(), c.getApellido(),c.getDni(),c.getFechaDeNacimiento(), c.getEmail()))
                .collect(Collectors.toList());
    }

    public EstudianteDTO update(Long id, EstudianteDTO estudianteDTO){
        Estudiante estudiante = new Estudiante(
                id,
                estudianteDTO.getDni(),
                estudianteDTO.getEmail(),
                estudianteDTO.getFechaDeNacimiento(),
                estudianteDTO.getApellido(),
                estudianteDTO.getNombre(),
                null
        );

        estudianteRepository.save(estudiante);

        return estudianteDTO;
    }

    public EstudianteDTO find(Long id){
        Estudiante estudiante = estudianteRepository
                .findById(id)
                .orElseThrow(()->new RuntimeException("El cursoId no es valido"));

        return new EstudianteDTO(estudiante.getNombre(), estudiante.getApellido(),estudiante.getDni(),estudiante.getFechaDeNacimiento(), estudiante.getEmail());

    }

    public void delete(Long id){
        estudianteRepository.deleteById(id);
    }
}
