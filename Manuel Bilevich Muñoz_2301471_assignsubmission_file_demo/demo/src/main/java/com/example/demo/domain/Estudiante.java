package com.example.demo.domain;

import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Entity
@Table(name = "estudiante")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Estudiante {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "dni")
    private int dni;

    @Email(message = "Debe ser un mail")
    @Column(name = "email")
    private String email;

    @Column(name = "fecha_de_nacimiento")
    private LocalDate fechaDeNacimiento;

    @Column(name = "apellido")
    private String apellido;

    @Column(name = "nombre")
    private String nombre;

    @Transient
    private Integer edad;

    public int edadActual(LocalDate fechaDeNacimiento){
        return edad=(LocalDate.now().getYear())-(fechaDeNacimiento.getYear());
    }
    public boolean esMayorEdad(){
        return edadActual(fechaDeNacimiento) >=18;
    }
}