package com.example.demo;

import com.example.demo.service.InscripcionService;
import com.example.demo.domain.Curso;
import com.example.demo.domain.Estado;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;
import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.repository.InscripcionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.time.Month;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Autowired
	EstudianteRepository estudianteRepository;

	@Autowired
	CursoRepository cursoRepository;

	@Autowired
	InscripcionRepository inscripcionRepository;

	@Autowired
	InscripcionService inscripcionService;

	private void fillDatabase(){
		estudianteRepository.saveAll(Arrays.asList(
				new Estudiante(null, 39388378, "manuelbilevich@legislatura.gob.ar",
						LocalDate.of(1995, Month.NOVEMBER, 1), "Bilevich", "Manuel", null),
				new Estudiante(null,19123654,"jp@gmail.com",LocalDate.of(1990,Month.JANUARY,1),
						"Perez", "Juan",null),
				new Estudiante(null, 38613172, "jg@gmail.com", LocalDate.of(1990,Month.FEBRUARY,1),
						"jose", "Romero", null)
		));

		cursoRepository.saveAll(Arrays.asList(
				new Curso(null, "Java", "JPA", LocalDate.of(2023,Month.JUNE,13),
						LocalDate.of(2023,Month.JULY,6)),
				new Curso(null, "Python", "MathLibrary", LocalDate.of(2020,Month.JUNE,13),
						LocalDate.of(2021,Month.JULY,6)),
				new Curso(null, "CSharp", "NetFramework Core", LocalDate.of(2013,Month.JUNE,13),
						LocalDate.of(2015,Month.JULY,6))
		));

		List estudiantes = estudianteRepository.findAll();
		List cursos = cursoRepository.findAll();

		long minDay = LocalDate.of(1970, 1, 1).toEpochDay();
		long maxDay = LocalDate.of(2015, 12, 31).toEpochDay();
		long randomDay;
		LocalDate randomDate;
		Estado randomEst = null;

		Iterator<Estudiante> iterador = estudiantes.iterator();
		Iterator<Curso> iteradorCurso = cursos.iterator();
		while(iterador.hasNext()){
			randomEst= randomEst.values()[new Random().nextInt(randomEst.values().length)];
			Estudiante e = iterador.next();
			Curso c = iteradorCurso.next();
			randomDay = ThreadLocalRandom.current().nextLong(minDay, maxDay);
			randomDate = LocalDate.ofEpochDay(randomDay);
			Inscripcion inscripcion = new Inscripcion(null, randomEst, c, randomDate, e);
			inscripcionRepository.save(inscripcion);
		}
	}

	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx){
		return args ->{
			fillDatabase();

			inscripcionService.inscript(
					1L,
					1L,
					LocalDate.of(2020,Month.AUGUST,1),
					Estado.Aceptada
			);

			cursoRepository.findAll();
			estudianteRepository.findAll();
			estudianteRepository.findByNombreAndDniGreaterThan("Romero", 20000000);
			inscripcionRepository.findAllByEstadoIsNot(Estado.Aceptada);
			cursoRepository.findByFechaDeInicioGreaterThan(LocalDate.of(2020,2,1));
			inscripcionRepository.findAllByEstado(Estado.Aceptada);
			inscripcionRepository.findAllByEstadoParam(Estado.Aceptada);
			inscripcionRepository.findAllByEstadoParamNative("Aceptada");
			estudianteRepository.findAll(PageRequest.of(1,5, Sort.by(Sort.Direction.ASC, "dni")));
			estudianteRepository.findAll(PageRequest.of(0,2, Sort.by(Sort.Direction.ASC, "dni")));


		};
	}

}